//
//  CommentResponse.swift
//  gag
//
//  Created by buisinam on 12/15/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import SwiftyJSON


class CommentResponse: Serializable {
    var comment_id = ""
    var comment_date = ""
    var comment_date_unix = 0
    var comment_content = ""
    var comment_point = 0
    var comment_author : AuthorDTO!
    var comment_voted = 0
    var comment_children = [CommentResponse]()
    var is_subcomment = false
    var is_last = false
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        
        comment_id = json["comment_id"].stringValue
        comment_date = json["comment_date"].stringValue
        comment_date_unix = json["comment_date_unix"].intValue
        comment_content = json["comment_content"].stringValue
        comment_point = json["comment_point"].intValue
        comment_voted = json["comment_voted"].intValue
        
        
        comment_voted = json["comment_voted"].intValue
        if let dataObject: Array = json["comment_children"].arrayObject
        {
            var listSub: [CommentResponse] = []
            for item in dataObject
            {
                let temp:CommentResponse = CommentResponse.newInstance() as! CommentResponse
                temp.fromJsonAnyObject(jsonAnyObject: item as AnyObject)
                temp.is_subcomment = true
                listSub.append(temp)
            }
            listSub.sort(by: { (item1, item2) -> Bool in
                return item1.comment_date_unix > item2.comment_date_unix
            })
            comment_children = listSub
        }
        
        if let dataObject: AnyObject = json["comment_author"].object as AnyObject?
        {
            let temp = AuthorDTO.newInstance()
            temp.fromJsonAnyObject(jsonAnyObject: dataObject as AnyObject)
            self.comment_author =  temp as! AuthorDTO
        }
    }
    
    override class func newInstance() -> Serializable {
        return CommentResponse()
    }

}

