//
//  Category.swift
//  gag
//
//  Created by buisinam on 12/15/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryResponse: Serializable {
    var category_id = 0
    var category_name = ""
    var category_description = ""
    var category_image = ""

    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        
        category_id = json["category_id"].intValue
        category_name = json["category_name"].stringValue
        category_description = json["category_description"].stringValue
        category_image = json["category_image"].stringValue
    }
    
    override class func newInstance() -> Serializable {
        return CategoryResponse()
    }

}
