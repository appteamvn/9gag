//
//  Post.swift
//  gag
//
//  Created by buisinam on 12/1/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import SwiftyJSON

class PostResponse: Serializable {
    var post_id = ""
    var post_title = ""
    var post_link = ""
    var youtube_thumbnail = ""
    var post_view = 0
    var post_comment = "0"
    var is_vote = 0
    var safe = 0
    var post_type = 0
    var is_long = 0
    var url = ""
    var post_date = ""
    var post_point:Int = 0
    var api_width = 0.0
    var api_height = 0.0
    var height_cell: Double?
    var isAds: Bool = false
    
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        
        post_id = json["post_id"].stringValue
        post_title = json["post_title"].stringValue
        post_link = json["post_link"].stringValue
        youtube_thumbnail = json["youtube_thumbnail"].stringValue
        post_view = json["post_view"].intValue
        post_comment = json["post_comment"].stringValue
        safe = json["safe"].intValue
        is_vote = json["is_vote"].intValue
        post_type = json["post_type"].intValue
        is_long = json["is_long"].intValue
        url = json["url"].stringValue
        post_date = json["post_date"].stringValue
        post_point = json["post_point"].intValue
        api_width = json["api_width"].doubleValue
        api_height = json["api_height"].doubleValue
    }
    
    override class func newInstance() -> Serializable {
        return PostResponse()
    }

}
