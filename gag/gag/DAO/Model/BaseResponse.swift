//
//  BaseResponse.swift
//  FastCard
//
//  Created by Le Anh Tai on 3/11/16.
//  Copyright © 2016 Le Anh Tai. All rights reserved.
//

import UIKit
import SwiftyJSON

class BaseResponse<T: Serializable>: Serializable {

    var code: Bool = false
    var message: String?
    var status: Bool = false
    var data = T.newInstance()
    let successCode: Bool = true
    
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        var json = JSON(jsonAnyObject)
        
        code = json["success"].boolValue
        status = code == successCode
        if let dataObject: AnyObject = json["posts"].object as AnyObject?
        {
            let temp = T.newInstance()
            temp.fromJsonAnyObject(jsonAnyObject: dataObject as AnyObject)
            self.data =  temp

        }
    }
    
}
