//
//  BaseArrayResponse.swift
//  FastCard
//
//  Created by Le Anh Tai on 3/11/16.
//  Copyright © 2016 Le Anh Tai. All rights reserved.
//

import UIKit
import SwiftyJSON

class BaseArrayResponse<T: Serializable>: Serializable {

    var code: Bool = false
    var message: String?
    var status: Bool = false
    let successCode: Bool = true
    var arrayData = T.newArrayInstance()
    
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        var json = JSON(jsonAnyObject)
        
        code = json["success"].boolValue
        status = code == successCode

        
        if let dataObject: AnyObject = json["posts"].object as AnyObject? {
            var jsonData: JSON = JSON(dataObject)
            if let arr = jsonData.arrayObject {
                for item in arr {
                    let temp = T.newInstance()
                    temp.fromJsonAnyObject(jsonAnyObject: item as AnyObject)
                    self.arrayData.append(temp)
                }
            }
        }
        
        
    }
}
