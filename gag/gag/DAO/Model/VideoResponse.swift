//
//  VideoResponse.swift
//  gag
//
//  Created by buisinam on 12/18/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import SwiftyJSON

class VideoResponse: Serializable {
    
    var is_vote = 0
    var post_author = 1
    var post_comment = 0
    var post_date = 0
    var post_id = 0
    var post_link = "http://demo.mona-media.com/fack/tv/kong-skull-island/";
    var post_point = 0
    var post_title = "KONG SKULL ISLAND"
    var post_view = 7
    var youtube_duration = "05:06"
    var youtube_id = "2onxgmKT1fw"
    var youtube_thumbnail = "https://img.youtube.com/vi/2onxgmKT1fw/hqdefault.jpg";
    var google_drive = ""
    
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        
        is_vote = json["is_vote"].intValue
        post_author = json["post_author"].intValue
        post_comment = json["post_comment"].intValue
        post_date = json["post_date"].intValue
        post_id = json["post_id"].intValue
        post_link = json["post_link"].stringValue
        post_point = json["post_point"].intValue
        post_title = json["post_title"].stringValue
        post_view = json["post_view"].intValue
        youtube_duration = json["youtube_duration"].stringValue
        youtube_id = json["youtube_id"].stringValue
        youtube_thumbnail = json["youtube_thumbnail"].stringValue
        google_drive = json["google_drive"].stringValue
    }
    
    override class func newInstance() -> Serializable {
        return VideoResponse()
    }
}
