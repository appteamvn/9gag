
//
//  UserResponse.swift
//  gag
//
//  Created by ToaNT1 on Dec14.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserResponse: Serializable {
    var user_type = 0
    var user_id = ""
    var user_name = ""
    var user_email = ""
    var user_password = ""
    var user_avatar = ""
    var secret_key = ""
    var device_token_ios = ""

    
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        
        user_type = json["user_type"].intValue
        user_id = json["user_id"].stringValue
        user_name = json["user_name"].stringValue
        user_email = json["user_email"].stringValue
        user_password = json["user_password"].stringValue
        user_avatar = json["user_avatar"].stringValue
        secret_key = json["secret_key"].stringValue
        device_token_ios = json["device_token_ios"].stringValue

    }
    
    override class func newInstance() -> Serializable {
        return UserResponse()
    }
}
