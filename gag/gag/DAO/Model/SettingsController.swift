//
//  SettingsController.swift
//  gag
//
//  Created by ThanhToa on 12/21/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class SettingControllerSingleton {
    var isShowAds: Bool!
    var isAutoNightMode: Bool!
    var isShowNewPost: Bool!
    var isAutoPlayGIFPost: Bool!
    var isAutoExpandLongPost: Bool!
    var isShowSensitiveContent: Bool!
    var isNightMode: Bool!
    var isPrivateUpvotes: Bool!
    
    //MARK: Shared Instance
    
    static let sharedInstance : SettingControllerSingleton = {
        let instance = SettingControllerSingleton()
        return instance
    }()
    
    //MARK: Local Variable
    
    
}

