//
//  User.swift
//  gag
//
//  Created by ThanhToa on 12/7/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class User: NSObject {
    var user_type: Int!
    var user_id:  NSString!
    var user_name: NSString!
    var user_email: NSString!
    var user_password: NSString!
    var user_avatar: NSString!
    let userDefaults = UserDefaults.standard
    
    override init() {
        
    }
    
    init(user_type:Int, user_id: NSString!, user_name:NSString, user_email:NSString, user_password:NSString, user_avatar:NSString) {
        self.user_type = user_type
        self.user_id = user_id
        self.user_name = user_name
        self.user_email = user_email
        self.user_password = user_password
        self.user_avatar = user_avatar
    }
    
    func fromUserDefault() {
        
        self.user_type = userDefaults.value(forKey: "user_type") as! Int
        self.user_id = userDefaults.value(forKey: "user_id") as! NSString
        self.user_name = userDefaults.value(forKey: "user_name") as! NSString
        self.user_email = userDefaults.value(forKey: "user_email") as! NSString
        self.user_avatar = userDefaults.value(forKey: "user_avatar") as! NSString
        
    }

}
