//
//  NewResponse.swift
//  gag
//
//  Created by HieuNT50 on 12/21/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import Foundation
import SwiftyJSON

class NewResponse: Serializable {
    var is_read = 0
    var date = ""
    var date_past = ""
    var post_id = ""
    var post_title = ""
    var first_str = ""
    var second_str = ""
    var type = ""
    var that_avatar = ""
    var url = ""

    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        is_read = json["is_read"].intValue
        date = json["date"].stringValue
        date_past = json["date_past"].stringValue
        post_title = json["post_title"].stringValue
        post_id = json["post_id"].stringValue
        first_str = json["first_str"].stringValue
        second_str = json["second_str"].stringValue
        type = json["type"].stringValue
        that_avatar = json["that_avatar"].stringValue
        url = json["url"].stringValue
    }

    override class func newInstance() -> Serializable {
        return NewResponse()
    }
    
}
