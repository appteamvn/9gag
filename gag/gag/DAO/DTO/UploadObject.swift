//
//  UploadObject.swift
//  gag
//
//  Created by buisinam on 1/8/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit

class UploadObject: NSObject {
    var title:String!
    var progress: Float! = 0.0
    var image: UIImage!
}
