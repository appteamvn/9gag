//
//  AuthorDTO.swift
//  gag
//
//  Created by buisinam on 12/16/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthorDTO: Serializable {
    var author_id = 0
    var author_name = ""
    var author_avatar = ""
    
    override func fromJsonAnyObject(jsonAnyObject: AnyObject) {
        let json = JSON(jsonAnyObject)
        
        author_id = json["author_id"].intValue
        author_name = json["author_name"].stringValue
        author_avatar = json["author_avatar"].stringValue
    }
    
    override class func newInstance() -> Serializable {
        return AuthorDTO()
    }

}
