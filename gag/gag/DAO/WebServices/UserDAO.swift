//
//  UserDAO.swift
//  gag
//
//  Created by ThanhToa on 12/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class UserDAO: BaseDAO {
    //! Define for header
    //    static var headerContents: [String: String] = ["Content-Type":"application/json"]\\
    
    
    
    // MARK: - USER GOOGLE
    class func userLogin(user_type: String, user_id: String,
                         user_name: String, user_email: String,
                         user_password: String
        ,completeHandle:@escaping (_ succesc: Bool, UserResponse?)-> Void)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let params = ["type" : user_type, "google_id" : user_id,
                      "login_email" : user_email, "login_name" : user_name,"device_token_ios" : appDelegate.strDeviceToken,
                      "method": "action_user_login"]
        
//        let window :UIWindow = UIApplication.shared.keyWindow!
//        let alertController = UIAlertController(title: "Title", message: params.description, preferredStyle: UIAlertControllerStyle.alert)
//        
//        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//        {
//            (result : UIAlertAction) -> Void in
//            print("You pressed OK")
//        }
//        alertController.addAction(okAction)
//        window.rootViewController?.present(alertController, animated: true, completion: nil)

        
        callServerPOSTWithJSONData(url: "", parameters : params) { (data:DataResponse<Any>) in
                                                            
                                                            if data.result.error == nil {
                                                                
                                                                let resultObject = BaseResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                                                                if resultObject.status == true {
                                                                    completeHandle(true, resultObject.data as? UserResponse)
                                                                    
                                                                } else {
                                                                    
                                                                    // Show alert of wrong message
                                                                    if let msg: String = resultObject.message {
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            // Callback when faild
                                                                            completeHandle(false, nil)
                                                                            
                                                                            // Show alert faild
                                                                            showErrorMessage(message: msg, errorCode: 0)
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            } else {
                                                                // Show error when registration
                                                                DispatchQueue.main.async {
                                                                    // Callback when faild
                                                                    completeHandle(false, nil)
                                                                    
                                                                    // Show message
                                                                   // showErrorMessage(message: "", errorCode: 0)
                                                                }
                                                            }
        }
    }
    // MARK: - USER FACEBOOK
    class func userFacebookLogin(user_type: String, user_id: String,
                                 user_name: String, user_email: String,
                                 user_password: String
        ,completeHandle:@escaping (_ succesc: Bool, UserResponse?)-> Void)
    {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let params = ["type" : user_type, "facebook_id" : user_id,
                      "login_email" : user_email, "login_name" : user_name,"device_token_ios" : appDelegate.strDeviceToken,
                      "method": "action_user_login"]
        
        callServerPOSTWithJSONData(url: "", parameters : params) { (data:DataResponse<Any>) in
                                                            
                                                            if data.result.error == nil {
                                                                
                                                                let resultObject = BaseResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                                                                if resultObject.status == true {
                                                                    completeHandle(true, resultObject.data as? UserResponse)
                                                                    
                                                                } else {
                                                                    
                                                                    // Show alert of wrong message
                                                                    if let msg: String = resultObject.message {
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            // Callback when faild
                                                                            completeHandle(false, nil)
                                                                            
                                                                            // Show alert faild
                                                                            showErrorMessage(message: msg, errorCode: 0)
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            } else {
                                                                // Show error when registration
                                                                DispatchQueue.main.async {
                                                                    // Callback when faild
                                                                    completeHandle(false, nil)
                                                                    
                                                                    // Show message
                                                                    // showErrorMessage(message: "", errorCode: 0)
                                                                }
                                                            }
        }
    }
    // MARK: - USER EMAIL
    class func userEmailLogin(user_type: String,user_email: String, user_password: String
        ,completeHandle:@escaping (_ succesc: Bool, UserResponse?)-> Void)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        callServerGETWithJSONData(url: "", parameters : ["method": "action_user_login",
                                                         "type": user_type,
                                                         "login_email" : user_email,
                                                         "login_password" : user_password,
                                                         "device_token_ios" : appDelegate.strDeviceToken
        ]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                if resultObject.status == true {
                    completeHandle(true, resultObject.data as? UserResponse)
                    
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    } else {
                        Common.showHubWithMessage(string: NSLocalizedString("incorrect_password", comment: ""))
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    Common.showHubWithMessage(string: NSLocalizedString("incorrect_password", comment: ""))
                }
            }
        }
    }
    
    // MARK: - USER REGISTER
    class func registerUser(login_name: String,
                            login_email: String,
                            login_password: String
        ,completeHandle:@escaping (_ succesc: Bool, UserResponse?)-> Void)
    {
        callServerPOSTWithJSONData(url: "", parameters : ["method": "action_user_register",
                                                          "login_name" : login_name,"login_email" : login_email,
                                                          "login_password": login_password]) { (data:DataResponse<Any>) in
                                                            let desiredString = NSString(data: data.data!, encoding: String.Encoding.utf8.rawValue)
                                                            print(desiredString)
                                                            if data.result.error == nil {
                                                                
                                                                let resultObject = BaseResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                                                                if resultObject.status == true {
                                                                    completeHandle(true, resultObject.data as? UserResponse)
                                                                    
                                                                } else {
                                                                    
                                                                    // Show alert of wrong message
                                                                    if let msg: String = resultObject.message {
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            // Callback when faild
                                                                            completeHandle(false, nil)
                                                                            
                                                                            // Show alert faild
                                                                            showErrorMessage(message: msg, errorCode: 0)
                                                                        }
                                                                    } else {
                                                                    Common.showHubWithMessage(string: NSLocalizedString("sign_up_fail", comment: ""))
                                                                    }
                                                                }
                                                                
                                                            } else {
                                                                // Show error when registration
                                                                DispatchQueue.main.async {
                                                                    // Callback when faild
                                                                    completeHandle(false, nil)
                                                                    
                                                                    // Show message
                                                                   // showErrorMessage(message: "", errorCode: 0)
                                                                }
                                                            }
        }
    }
    // MARK: - USER LOGOUT
    class func logout(user_id: String, secret_key: String
        ,completeHandle:@escaping (_ succesc: Bool, UserResponse?)-> Void)
    {
        callServerPOSTWithJSONData(url: "", parameters : ["method": "action_user_logout",
                                                          "user_id," : user_id,
                                                          "secret_key": secret_key]) { (data:DataResponse<Any>) in
                                                            
                                                            if data.result.error == nil {
                                                                
                                                                let resultObject = BaseResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                                                                if resultObject.status == true {
                                                                    completeHandle(true, resultObject.data as? UserResponse)
                                                                    
                                                                } else {
                                                                    
                                                                    // Show alert of wrong message
                                                                    if let msg: String = resultObject.message {
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            // Callback when faild
                                                                            completeHandle(false, nil)
                                                                            
                                                                            // Show alert faild
                                                                            showErrorMessage(message: msg, errorCode: 0)
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            } else {
                                                                // Show error when registration
                                                                DispatchQueue.main.async {
                                                                    // Callback when faild
                                                                    completeHandle(false, nil)
                                                                }
                                                            }
        }
    }
    
    // MARK: - USER CHANGE PASSWORD
    class func changePassword(user_id: String,
                              new_password: String,
                              secret_key: String,
                              completeHandle: @escaping(_ success: Bool, _ data: [UserResponse]?) -> Void )
    {
        callServerPOSTWithJSONData(url: "", parameters: ["method": "action_user_change_password",
                                                         "user_id" : user_id,
                                                         "new_password" : new_password,
                                                         "secret_key": secret_key]) { (data: DataResponse<Any>) in
                                                            if data.result.error == nil {
                                                                print("changePassword success")
                                                                
                                                                let resultObject = BaseArrayResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                                                                print(resultObject.arrayData)
                                                                if resultObject.status == true {
                                                                    completeHandle(true, resultObject.arrayData as? [UserResponse])
                                                                } else {
                                                                    
                                                                    // Show alert of wrong message
                                                                    if let msg: String = resultObject.message {
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            // Callback when faild
                                                                            completeHandle(false, nil)
                                                                            
                                                                            // Show alert faild
                                                                            showErrorMessage(message: msg, errorCode: 0)
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            } else {
                                                                // Show error when registration
                                                                DispatchQueue.main.async {
                                                                    // Callback when faild
                                                                    completeHandle(false, nil)
                                                                    
                                                                    // Show message
                                                                    showErrorMessage(message: "", errorCode: 0)
                                                                }
                                                            }
        }
    }
    
    // MARK: - USER CHANGE AVATAR
    class func changeAvatar(user_id: String,
                            secret_key: String,
                            file: UIImage,
                            navigationBar: UINavigationController?,
                            completeHandle:@escaping (_ success: Bool, _ img: String)-> Void)    {
        callServerByUploadPostJSONData(url: "", parameters: ["method": "action_user_change_avatar" as AnyObject,
                                                             "user_id" : user_id  as AnyObject,
                                                             "secret_key": secret_key as AnyObject,
                                                             ], images: file, photoKey: "file", completionHandler: { (data:DataResponse<Any>) in
                                                                let dataJson  = JSON(data: data.data!)
                                                                let success = dataJson["success"].boolValue
                                                                let data_img = dataJson["image"].string
                                                                if success == true{
                                                                    completeHandle(true, data_img!)
                                                                    navigationBar?.progress = 0.0
                                                                }
                                                                else{
                                                                    completeHandle(false, "")
                                                                    navigationBar?.progress = 0.0
                                                                    self.showErrorMessage(message: dataJson["message"].stringValue, errorCode: 1)
                                                                }
        }) { (progress, error) in
            if navigationBar != nil && error == nil {
                navigationBar?.progress = Float(progress!)
            }
            else{
                Common.showHubWithMessage(string: NSLocalizedString("sign_up_fail", comment: ""))
            }
        }
    }
    
    // MARK: - USER CHANGE DATA (Edit profile)
    class func updateProfile(user_id: String, secret_key: String,
                             gender: Int, birhday: String,
                             country: String, user_display_name: String,
                             nsfw: Int, user_email: String,
                             completeHandle: @escaping(_ success: Bool,  _ data: [UserResponse]?) -> Void )
    {
        // http://demo.mona-media.com/fack/mona-api/?method=action_user_change_data&user_id=37&secret_key=3R28K42FaIpbphXR&data_change=[1,2016-12-27,"HCM","ThanhToa", 0, "12130120@st.hcmuaf.edu.vn"]
        
        callServerPOSTWithJSONData(url: "", parameters: ["method": "action_user_change_data",
                                                         "user_id" : user_id,
                                                         "secret_key" : secret_key,
                                                         "data_change" : [gender,birhday,country,user_display_name,nsfw,user_email]
        ]) { (data: DataResponse<Any>) in
            if data.result.error == nil {
                print("Update profile success")
                
                let resultObject = BaseArrayResponse<UserResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [UserResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                }
            }
            
        }
    }
}
