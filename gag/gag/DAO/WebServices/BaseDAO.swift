//
//  BaseDAO.swift
//  FastCard
//
//  Created by Le Anh Tai on 3/11/16.
//  Copyright © 2016 Le Anh Tai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


// MARK: - Define typealias of callback
typealias callServerCompleteCallback = (DataResponse<Any>) -> Void
typealias progressBlockDownload = (_ progress: Double?, _ error: NSError?,_ index : Int, _ completed: Bool) -> Void
typealias progressBlock = (_ progress: Double?, _ error: NSError?) -> Void
class BaseDAO: NSObject {
    
    
    //! Define for header
    static var headerContents: [String: String] = ["Content-Type":"application/json"]
    
    // MARK: - THE REFACTORY API SERVICE FUNCTIONS
    
    //! Function use to POST Model data
    class func callServerPOSTWithModelData(url: String, request: Serializable?, completionHandler: @escaping callServerCompleteCallback) {
        
        var parameters: [String:Any]?
        if request != nil {
            parameters = JSON(data: request!.toJsonNSData() as Data).dictionaryObject
        }
        
        Alamofire.request(ServiceURL.serverDomain + url, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON (completionHandler: completionHandler)
    }
    
    //! Function use to GET Model data
    class func callServerGETWithModelData(url: String, request: Serializable?, completionHandler: @escaping callServerCompleteCallback) {
        
         
        
        var parameters: [String:Any]?
        if request != nil {
            parameters = JSON(data: request!.toJsonNSData() as Data).dictionaryObject
        }
        
        Alamofire.request(ServiceURL.serverDomain + url, method: HTTPMethod.get, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON (completionHandler: completionHandler)
    }
    
    //! Function use to POST JSON data
    class func callServerPOSTWithJSONData(url: String, parameters: [String : Any]?, completionHandler: @escaping callServerCompleteCallback) {
        
         
        print("request: \(ServiceURL.serverDomain + url)")
        
        Alamofire.request(ServiceURL.serverDomain + url, method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON (completionHandler: completionHandler)
        
    }
    
    //! Function use to GET JSON data
    class func callServerGETWithJSONData(url: String, parameters: [String : Any]?, completionHandler: @escaping callServerCompleteCallback) {
        
        print("request: \(ServiceURL.serverDomain + url)")
        
        Alamofire.request(ServiceURL.serverDomain + url, method: HTTPMethod.get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON (completionHandler: completionHandler)
    }

    class func callServerByUploadPostJSONData(url: String, parameters: [String : AnyObject]?, images: UIImage?, photoKey: String?, completionHandler:  callServerCompleteCallback?, progress: @escaping progressBlock) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImageJPEGRepresentation(images!, 1)!, withName: "file", fileName: "file.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters! {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:ServiceURL.serverDomain + url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progressCurrent) in
                    print("upload: " + "\(progress)")
                    progress(progressCurrent.fractionCompleted,nil)
                })
                
                upload.responseJSON (completionHandler: completionHandler!)
                
            case .failure(let encodingError):
                progress(nil,encodingError as NSError?)
                print(encodingError.localizedDescription)
                break
            }
        }
    }

    class func callServerByUploadManagerPostJSONData(url: String, parameters: [String : AnyObject]?, images: UIImage?, photoKey: String?,index: Int, completionHandler:  callServerCompleteCallback?, progress: @escaping progressBlockDownload) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImageJPEGRepresentation(images!, 1.0)!, withName: "file", fileName: "file_001.jpg", mimeType: "image/jpg")
            for (key, value) in parameters! {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:ServiceURL.serverDomain + url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progressCurrent) in
                    print("upload: " + "\(progressCurrent)")
                    progress(progressCurrent.fractionCompleted,nil,index, false)
                })
                
                upload.responseJSON(completionHandler: { (data) in
                    progress(1.0,nil,index, true)
                })
                
            case .failure(let encodingError):
                progress(nil,encodingError as NSError?,index, false)
                print(encodingError.localizedDescription)
                break
            }
        }
    }
        
//            Alamofire.upload(multipartFormData: { (multipartFormData) in
//                multipartFormData.append(NSKeyedArchiver.archivedData(withRootObject: images!), withName: photoKey!)
//                // Handle for parameters
//                // Check to append parameters into body data
//                var parameters: [String : AnyObject]?
//                if request != nil {
//                    
//                    // Convert to JSON data
//                    parameters = JSON(data: request!.toJsonNSData()).dictionaryObject
//                    
//                    // Append to body data
//                    for (key, value) in parameters! {
//                        
//                        if value is Array<NSDictionary> {
//                            do {
//                                let jsonData = try NSJSONSerialization.dataWithJSONObject(value, options: NSJSONWritingOptions.PrettyPrinted)
//                                multipartFormData.appendBodyPart(data: jsonData, name: key)
//                            } catch {
//                                print(error)
//                            }
//                        } else if value is NSNumber {
//                            multipartFormData.appendBodyPart(data: "\(value)".dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                        } else {
//                            multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                        }
//
//                    }
//                }
//
//            }, with: "", encodingCompletion: { (result) in
//                
//            })
//        
//    }
//    
//        Alamofire.upload(.POST, ServiceURL.serverDomain + url, multipartFormData: { multipartFormData in
//                
//                // Convert list image to NSData
//                multipartFormData.appendBodyPart(data: NSKeyedArchiver.archivedDataWithRootObject(images!), name: photoKey!)
//                
//                // Handle for parameters
//                for (key, value) in parameters! {
//                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                }
//
//            },
//                             encodingCompletion: { encodingResult in
//                                switch encodingResult {
//                                case .Success(let upload, _, _):
//                                    upload.responseJSON(completionHandler: completionHandler)
//                                case .Failure(let encodingError):
//                                    completionHandler(nil, nil, Result.Failure(nil, encodingError))
//                                }
//            }
//            )
//            
//        } else {
//            Alamofire.request(.POST, ServiceURL.serverDomain + url, headers: headerContents,
//                              parameters: parameters, encoding: .JSON).responseJSON(completionHandler: completionHandler)
//        }
//    }

    
//    //! Function use to POST form-data include image
//    class func callServerPOSTFormDataWithModel(url: String, request: Serializable?, images: [String: UIImage]?, completionHandler: @escaping callServerCompleteCallback) {
//        
//         
//        
//        // Check have any image data
//        Alamofire.upload(.POST, ServiceURL.serverDomain + url, multipartFormData: { multipartFormData in
//            
//            // Append list image into data body
//            if let imageInfos = images {
//                for (key,value) in imageInfos {
//                    if let imageData = UIImagePNGRepresentation(value) {
//                        multipartFormData.appendBodyPart(data: imageData, name: key, fileName: key, mimeType: "image/png")
//                    }
//                }
//            }
//            // Check to append parameters into body data
//            var parameters: [String : AnyObject]?
//            if request != nil {
//                
//                // Convert to JSON data
//                parameters = JSON(data: request!.toJsonNSData()).dictionaryObject
//                
//                // Append to body data
//                for (key, value) in parameters! {
//                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                }
//            }
//            
//            }, encodingCompletion: { encodingResult in
//                switch encodingResult {
//                case .Success(let upload, _, _):
//                    upload.responseJSON(completionHandler: completionHandler)
//                case .Failure(let encodingError):
//                    completionHandler(nil, nil, Result.Failure(nil, encodingError))
//                }
//        })
//    }
//    
//    //! Function use to POST Data with single image & Image key
//    class func callServerPOSTProfileWithFormData(url: String, request: Serializable?, images: [UIImage]?, imagesKey: String?, image: UIImage?, imageKey: String?, completionHandler: @escaping callServerCompleteCallback) {
//        
//        
//        Alamofire.upload(.POST, ServiceURL.serverDomain + url, multipartFormData: { multipartFormData in
//            
//            // Convert list image to NSData
//            if images != nil {
//                multipartFormData.appendBodyPart(data: NSKeyedArchiver.archivedDataWithRootObject(images!), name: imagesKey!)
//            }
//            
//            // Append image data
//            if image != nil {
//                if let imageData = UIImagePNGRepresentation(image!) {
//                    multipartFormData.appendBodyPart(data: imageData, name: imageKey!, fileName: imageKey!, mimeType: "image/png")
//                }
//            }
//            
//            // Check to append parameters into body data
//            var parameters: [String : AnyObject]?
//            if request != nil {
//                
//                // Convert to JSON data
//                parameters = JSON(data: request!.toJsonNSData()).dictionaryObject
//                
//                // Append to body data
//                for (key, value) in parameters! {
//                    
//                    if value is Array<NSDictionary> {
//                        do {
//                            let jsonData = try NSJSONSerialization.dataWithJSONObject(value, options: NSJSONWritingOptions.PrettyPrinted)
//                            multipartFormData.appendBodyPart(data: jsonData, name: key)
//                        } catch {
//                            print(error)
//                        }
//                    } else if value is NSNumber {
//                        multipartFormData.appendBodyPart(data: "\(value)".dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                    } else {
//                        multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                    }
//                    
//                }
//            }
//
//        }, encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .Success(let upload, _, _):
//                upload.responseJSON(completionHandler: completionHandler)
//            case .Failure(let encodingError):
//                completionHandler(nil, nil, Result.Failure(nil, encodingError))
//            }
//        })
//    }
//    
//    
//    // MARK: - THE STILL NOT REFACTORY API SERVICE
//    
//
//    /**
//     * Function use to request API by JSON data
//     * @param url: String
//     * @param completionHandler: CompleteBlock
//     * @param parameters: [String : AnyObject]?
//     */
//    class func callServerByJSONData(url: String, parameters: [String : AnyObject]?, completionHandler: (NSURLRequest?, HTTPURLResponse?, Result<AnyObject>) -> Void) {
//        
//         
//        
//        Alamofire.request(.POST, ServiceURL.serverDomain + url, headers: headerContents,
//            parameters: parameters, encoding: .JSON).responseJSON(completionHandler: completionHandler)
//    }
//    
//    class func callServerByRequestModel(url: String, request: Serializable?, completionHandler: (NSURLRequest?, HTTPURLResponse?, Result<AnyObject>) -> Void) {
//        
//         
//        
//        var parameters: [String : AnyObject]?
//        if request != nil {
//            parameters = JSON(data: request!.toJsonNSData() as Data).dictionaryObject
//            print("\(parameters!)")
//        }
//        
//        Alamofire.request(.POST, ServiceURL.serverDomain + url, headers: headerContents,
//            parameters: parameters, encoding: .JSON).responseJSON(completionHandler: completionHandler)
//    }
//    
//    /**
//     * Function use to request API by JSON data
//     * @param url: String
//     * @param completionHandler: CompleteBlock
//     * @param parameters: [String : AnyObject]?
//     */
//    class func callServerByGETJSONData(url: String, parameters: [String : AnyObject]?, completionHandler: (NSURLRequest?, HTTPURLResponse?, Result<AnyObject>) -> Void) {
//        
//         
//        
//        // Create URL Parameters
//        var parameterString: String = ""
//        if parameters != nil {
//            let parameterURL: NSMutableArray = NSMutableArray()
//            for (key, value) in parameters! {
//                parameterURL.add("\(key)=\(value)")
//            }
//            parameterString = parameterURL.componentsJoined(by: "&")
//        }
//        Alamofire.request(.GET, ServiceURL.serverDomain + url + parameterString, headers: headerContents,
//            parameters: parameters, encoding: .JSON).responseJSON(completionHandler: completionHandler)
//    }
//    
//    /*
//     * @Function Upload POST with parameters
//     * @param url: String
//     * @param completionHandler: CompleteBlock
//     * @param parameters: [String : AnyObject]?
//     * @param images
//     */
//
//    class func callServerByUploadListImagePostJSONData(url: String, parameters: [String : AnyObject]?, images: [String: UIImage]?, completionHandler: @escaping (NSURLRequest?, HTTPURLResponse?, Result<AnyObject>) -> Void) {
//        
//        if let imageInfos:[String: UIImage] = images {
//            
//            Alamofire.upload(.POST, ServiceURL.serverDomain + url, multipartFormData: { multipartFormData in
//                
//                // Hanlde upload images
//                for (key,value) in imageInfos {
//                    if let imageData = UIImagePNGRepresentation(value) {
//                        multipartFormData.appendBodyPart(data: imageData, name: key, fileName: key, mimeType: "image/png")
//                    }
//                }
//                
//                // Handle for parameters
//                for (key, value) in parameters! {
//                    if let stringValue: String = value as? String {
//                        multipartFormData.appendBodyPart(data: stringValue.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                    }
//                    else {
//                        multipartFormData.appendBodyPart(data: "\(value)".dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                    }
//                }
//                
//            }, encodingCompletion: { encodingResult in
//                switch encodingResult {
//                case .Success(let upload, _, _):
//                    upload.responseJSON(completionHandler: completionHandler)
//                case .Failure(let encodingError):
//                    completionHandler(nil, nil, Result.Failure(nil, encodingError))
//                }
//            })
//            
//        } else {
//            Alamofire.request(.POST, ServiceURL.serverDomain + url, headers: headerContents,
//                parameters: parameters, encoding: .JSON).responseJSON(completionHandler: completionHandler)
//        }
//    }
//    
    /**
     * Function use to show error
     * @param message
     * @return void
     */
    class func showErrorMessage(message: String, errorCode: Int) {
        
        // Show error message
//        AppDelegate.shareObject().window!.makeToast(message)
//        
//        // Check to force logout
//        if errorCode == ServiceURL.InvalidTokenCode {
//            DataCenter.shareInstance.logout()
//        }
//        let alert = UIAlertView()
//        alert.title = NSLocalizedString("app_name", comment: "")
//        alert.message = message
//        alert.addButton(withTitle: "OK")
//        alert.show()
    }
    
    
    class func getUserId () -> Int{
        let user = UserDefaults.standard
        if user.object(forKey: Common.USER_ID) == nil {
            return 0
        }
        let user_id  = user.integer(forKey: Common.USER_ID)
        return user_id
    }
    
    class func getSecretKey () -> String{
        let user = UserDefaults.standard
        if user.string(forKey: Common.SECRET_KEY) == nil || user.string(forKey: Common.SECRET_KEY) == nil {
        return ""
        }
        let secretKey = user.string(forKey: Common.SECRET_KEY)
        return secretKey!
    }
    
    class func checkLogin () -> Bool{
        let user = UserDefaults.standard
        if user.object(forKey: Common.USER_ID) == nil {
            return false
        } else {
            return true
        }
    }

}
