//
//  PostDAO.swift
//  gag
//
//  Created by buisinam on 12/2/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KYNavigationProgress

protocol DownloadDelegate {
    func downloadComplete(complete: Bool, progress: Double,index : Int)
}

class PostDAO: BaseDAO {
    /**
     * Function use to register user's account
     * @param fullName
     * @param email
     * @param phoneNumber
     * @param password not need encrypt
     * @param completionHandler
     */
    
    class PostDaoSingleton{
        
        //MARK: Shared Instance
        var delegate:DownloadDelegate!
        static let sharedInstance : PostDaoSingleton = {
            let instance = PostDaoSingleton()
            return instance
        }()
        
        //MARK: Local Variable
        var categoryPost : [CategoryResponse] = []
        var uploadObjs: [UploadObject?] = []
        
    }
    
    class func getPost(display: String,type: String,since: String,last: String,completeHandle:@escaping (_ succesc: Bool, [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["display" : display,"user_id": self.getUserId() ,"type" : type,"since" : since,"last": last, "method": "get_post_list"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                        completeHandle(true, resultObject.arrayData as? [PostResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
    class func getPostDetail(post_id: String,completeHandle:@escaping (_ succesc: Bool, PostResponse?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["post_id" : post_id,"user_id": self.getUserId(),"method": "get_post_detail"]) { (data:DataResponse<Any>) in
            if data.result.error == nil {
                let resultObject = BaseResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.data)
                if resultObject.status == true {
                    completeHandle(true, resultObject.data as? PostResponse)
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }

    
    class func userVote(post_id: Int,value: Int,completeHandle:@escaping (_ succesc: Bool, _ data: Int?)-> Void)
    {
        callServerPOSTWithJSONData(url: "", parameters: ["user_id" : self.getUserId(), "post_id": post_id, "value": value, "method": "action_user_vote_post"]) { (data:DataResponse<Any>) in
            if data.result.error == nil {
                let jsonResult = JSON(data:data.data!)
                let sucess = jsonResult["success"].boolValue
                if sucess {
                    completeHandle(true, jsonResult["points"].intValue)
                }
                else{
                    completeHandle(false, nil)
                }
            }
            else{
                completeHandle(false, nil)
            }
        }
    }
    
    class func getCategoryPost(completeHandle:@escaping (_ success: Bool, _ data: [CategoryResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters: ["method": "get_category"]) { (data:DataResponse<Any>) in
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<CategoryResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    PostDaoSingleton.sharedInstance.categoryPost = (resultObject.arrayData as? [CategoryResponse])!

                    completeHandle(true, resultObject.arrayData as? [CategoryResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
    class func getFeaturePost(completeHandle:@escaping (_ success: Bool, _ data: [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters: ["method": "get_post_featured"]) { (data:DataResponse<Any>) in
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [PostResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    // MARK: - action_user_post
    class func userPost(user_id: String, title: String
                        ,category_id: String, safe: String
                        ,source_url: String, data: UIImage
                        ,navigationBar: UINavigationController?
                        ,completeHandle:@escaping (_ success: Bool)-> Void)
    {
        let uploadObject = UploadObject()
        uploadObject.title = title
        uploadObject.image = data
        PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.append(uploadObject)

        callServerByUploadManagerPostJSONData(url: "", parameters: ["method": "action_user_post" as AnyObject,
                                                                    "user_id" : user_id as AnyObject,
                                                                    "title" : title as AnyObject,
                                                                    "category_id" : category_id as AnyObject,
                                                                    "safe" : safe as AnyObject,
                                                                    "source_url" : source_url as AnyObject,
                                                                    
                                                                    ], images: data, photoKey: "file",index: (PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count - 1), completionHandler: { (data:DataResponse<Any>) in
                                                                 let dataJson  = JSON(data: data.data!)
                                                                
                                                                 let success = dataJson["success"].boolValue
                                                                if success == true{
                                                                    completeHandle(true)
                                                                }
                                                                else{
                                                                    completeHandle(false)
                                                                    navigationBar?.progress = 0.0
                                                                    self.showErrorMessage(message: dataJson["message"].stringValue, errorCode: 1)
                                                                }
            
        }) { (progress, error, index, completed) in
//            if navigationBar != nil && error == nil {
//                navigationBar?.progress = Float(progress!)
//            }
//            else{
//                showErrorMessage(message: NSLocalizedString("post_fail", comment: ""), errorCode: 1)
//            }
            if completed && index < PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count{
                PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.remove(at: index)
            }

            if self.PostDaoSingleton.sharedInstance.delegate != nil {
                PostDaoSingleton.sharedInstance.delegate.downloadComplete(complete: completed, progress: progress!,index: index)
            }
        }
    }
    //get_post_profile
    class func getPostProfile(author_id: String,user_id: String,type: String,since: String,last: String,completeHandle:@escaping (_ succesc: Bool, [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["author_id" : author_id,"user_id": user_id,"type" : type,"since" : since,"last": last, "method": "get_post_profile"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [PostResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    //get_post_search
    class func getPostSearch(user_id: String,since: String,last: String,search_value: String,completeHandle:@escaping (_ succesc: Bool, [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["user_id" : user_id,"since" : since,"last": last,"search_value": search_value, "method": "get_post_search"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [PostResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    //get_post_comment
    class func getPostComment(post_id: Int,type: String,user_id: String,completeHandle:@escaping (_ succesc: Bool, [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["post_id" : post_id, "type" : type, "user_id" : user_id, "method" : "get_post_search"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [PostResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    class func getPostExplore(display: String,type: String,category_id: Int,since: String,last: String,completeHandle:@escaping (_ succesc: Bool, [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["display" : display,"type" : type,"category_id": category_id,"since" : since,"last": last, "method": "get_post_list"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [PostResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
}


