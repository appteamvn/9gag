//
//  CommentDAO.swift
//  gag
//
//  Created by buisinam on 12/15/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CommentDAO: BaseDAO {
    
    class func getListPostComment(post_id: String,type: String,completeHandle:@escaping (_ succesc: Bool, _ data: [CommentResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["post_id" : post_id,"user_id" : self.getUserId(),
                                                         "type": type, "method": "get_post_comment"]) { (data:DataResponse<Any>) in
                                                            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<CommentResponse>(jsonAnyObject: data.result.value! as AnyObject)
                
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [CommentResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
    class func commentPost(post_id: String, comment_parent: String,comment_content: String, completeHandle:@escaping (_ success: Bool)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["post_id" : post_id,"user_id" : self.getUserId(),
                                                         "comment_parent": comment_parent,"comment_content" : comment_content, "method": "action_user_comment"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseResponse<CommentResponse>(jsonAnyObject: data.result.value! as AnyObject)
                
                if resultObject.status == true {
                    completeHandle(true)
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
    class func userVoteComment(user_id: String, comment_id: String, value: Int, completeHandle: @escaping(_ success: Bool, _ data: Int?) -> Void )
    {
        callServerPOSTWithJSONData(url: "", parameters: ["user_id": user_id, "comment_id": comment_id, "value": value, "method": "action_user_vote_comment"]) { (data: DataResponse<Any>) in
            if data.result.error == nil {
                let jsonResponse = JSON(data: data.data!)
                let success = jsonResponse["success"].boolValue
                if success {
                    completeHandle(true, jsonResponse["points"].intValue)
                } else {
                    completeHandle(false, nil)
                }
            } else {
                completeHandle(false, nil)
            }
        }
    }
    // MARK: - DELETE COMMENT
    class func userDeleteComment(user_id: String, comment_id: String, secret_key: String, completeHandle: @escaping(_ success: Bool) -> Void )
    {
        callServerPOSTWithJSONData(url: "", parameters: ["method": "action_user_delete_comment", "user_id": user_id, "comment_id": comment_id, "secret_key": secret_key]) { (data: DataResponse<Any>) in
            if data.result.error == nil {
                let jsonResponse = JSON(data: data.data!)
                let success = jsonResponse["success"].boolValue
                if success {
                    completeHandle(true)
                } else {
                    completeHandle(false)
                }
            } else {
                completeHandle(false)
            }
        }
    }
}
