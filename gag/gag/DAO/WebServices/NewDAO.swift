//
//  NewDAO.swift
//  gag
//
//  Created by HieuNT50 on 12/20/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class NewDAO: BaseDAO{
    
    class NewDaoSingleton {
        
        //MARK: Shared Instance
        
        static let sharedInstance : NewDaoSingleton = {
            let instance = NewDaoSingleton()
            return instance
        }()
        
        //MARK: Local Variable
        
        var newNotify : [NewResponse] = []
        
    }
    
    class func getNewNotify(completeHandle:@escaping (_ succesc: Bool, [NewResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["method": "get_notification"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<NewResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    NewDaoSingleton.sharedInstance.newNotify = (resultObject.arrayData as? [NewResponse])!
                    completeHandle(true, resultObject.arrayData as? [NewResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
    class func getNewList(since: String, last: String, completeHandle:@escaping (_ succesc: Bool, [NewResponse]?)-> Void)
    {
        let secretKey = self.getSecretKey()
        let user_id =  self.getUserId()
        callServerGETWithJSONData(url: "", parameters : ["user_id": user_id, "secret_key" : secretKey, "since": since, "last": last,"method": "get_notification"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<NewResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [NewResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
}
