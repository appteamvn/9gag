//
//  VideoDAO.swift
//  gag
//
//  Created by TamPham on 12/16/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VideoDao: BaseDAO{
    
    class VideoDaoSingleton {
        
        //MARK: Shared Instance
        
        static let sharedInstance : VideoDaoSingleton = {
            let instance = VideoDaoSingleton()
            return instance
        }()
        
        //MARK: Local Variable
        
        var categoryVideo : [CategoryResponse] = []

    }
    
    class func getVideoCategory(completeHandle:@escaping (_ succesc: Bool, [CategoryResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["method": "get_video_category"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<CategoryResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    VideoDaoSingleton.sharedInstance.categoryVideo = (resultObject.arrayData as? [CategoryResponse])!
                    completeHandle(true, resultObject.arrayData as? [CategoryResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }
    
    class func getListVideo(display: String, type: String,category_id: Int, since: String, last: String, completeHandle:@escaping (_ succesc: Bool, [VideoResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["display": display, "type": type,"category_id" : category_id, "since": since, "last": last,"method": "get_video_list"]) { (data:DataResponse<Any>) in
            
            if data.result.error == nil {
                
                let resultObject = BaseArrayResponse<VideoResponse>(jsonAnyObject: data.result.value! as AnyObject)
                print(resultObject.arrayData)
                if resultObject.status == true {
                    completeHandle(true, resultObject.arrayData as? [VideoResponse])
                } else {
                    
                    // Show alert of wrong message
                    if let msg: String = resultObject.message {
                        DispatchQueue.main.async {
                            
                            // Callback when faild
                            completeHandle(false, nil)
                            
                            // Show alert faild
                            showErrorMessage(message: msg, errorCode: 0)
                        }
                    }
                }
                
            } else {
                // Show error when registration
                DispatchQueue.main.async {
                    // Callback when faild
                    completeHandle(false, nil)
                    
                    // Show message
                    showErrorMessage(message: "", errorCode: 0)
                }
            }
        }
    }

}
