//
//  ExploreDAO.swift
//  gag
//
//  Created by TamPham on 12/28/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ExploreDAO: BaseDAO {
    class func getListSearchExplore(since: String, last: String, search_value: String ,completeHandle:@escaping (_ succesc: Bool, _ data: [PostResponse]?)-> Void)
    {
        callServerGETWithJSONData(url: "", parameters : ["user_id:": self.getUserId(),"since": since,"last": last ,"search_value": search_value, "method": "get_post_search"]) { (data:DataResponse<Any>) in
                                                            
                                                            if data.result.error == nil {
                                                                
                                                                let resultObject = BaseArrayResponse<PostResponse>(jsonAnyObject: data.result.value! as AnyObject)
                                                                
                                                                print(resultObject.arrayData)
                                                                if resultObject.status == true {
                                                                    completeHandle(true, resultObject.arrayData as? [PostResponse])
                                                                } else {
                                                                    
                                                                    // Show alert of wrong message
                                                                    if let msg: String = resultObject.message {
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            // Callback when faild
                                                                            completeHandle(false, nil)
                                                                            
                                                                            // Show alert faild
                                                                            showErrorMessage(message: msg, errorCode: 0)
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            } else {
                                                                // Show error when registration
                                                                DispatchQueue.main.async {
                                                                    // Callback when faild
                                                                    completeHandle(false, nil)
                                                                    
                                                                    // Show message
                                                                    showErrorMessage(message: "", errorCode: 0)
                                                                }
                                                            }
        }
    }
}
