//
//  BorderView.swift
//  Bar
//
//  Created by buisinam on 10/8/16.
//  Copyright © 2016 Kienbk1910. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import QuartzCore

@IBDesignable
class BorderView: UIView {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    @IBInspectable var color: UIColor = UIColor.clear
    @IBInspectable var borderThin: CGFloat = 0
    @IBInspectable var corner: CGFloat = 0
    let gradientLayer = CAGradientLayer()
    
    override func draw(_ rect: CGRect) {
        // Drawing code
//        self.clipsToBounds = false
//        self.layer.masksToBounds = true
//        self.layer.borderColor = color.cgColor
//        self.layer.borderWidth = borderThin
//        self.layer.cornerRadius = corner
//        
//        self.layer.shadowColor = UIColor.darkGray.cgColor
//        self.layer.shadowOpacity = 0.8
//        self.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
//        self.layer.shadowRadius = 16
//        
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        
//        self.layer.shouldRasterize = true
    }
}

@IBDesignable
class BorderTextField: UITextField {
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    @IBInspectable var placeholderColor: UIColor = UIColor.clear
    @IBInspectable var color: UIColor = UIColor.clear
    @IBInspectable var borderThin: CGFloat = 0
    @IBInspectable var corner: CGFloat = 0
    @IBInspectable var paddingLeft: CGFloat = 0
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.masksToBounds = true
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderThin
        self.layer.cornerRadius = corner
        
        if placeholder != nil{
            
            let placeholderString = NSAttributedString(string: placeholder!, attributes: [NSForegroundColorAttributeName: placeholderColor])
            self.attributedPlaceholder = placeholderString
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
}


@IBDesignable
class ShadowImage: UIImageView {
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        
    }
    override func awakeFromNib() {
        self.clipsToBounds = false
//        self.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize(width: 0, height: -8)
        
        //        self.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        self.layer.shadowRadius = 5.0
        
        
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
    
}




