//
//  Downloader.swift
//  gag
//
//  Created by buisinam on 11/23/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class Downloader{

    
    class func load(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
//        FileManager.default.delegate = self
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = try! URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion()
                } catch (let writeError) {
                    print("error writing file \(localUrl) : \(writeError)")
                    if (writeError as NSError).code == NSFileWriteFileExistsError {
                        do {
                            try FileManager.default.removeItem(at: localUrl)
                            print("Existing file deleted.")
                        } catch {
                            print("Failed to delete existing file:\n\((error as NSError).description)")
                        }
                        do {
                            try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                            print("File saved.")
                            completion()
                        } catch {
                            print("File not saved:\n\((error as NSError).description)")
                        }
                        
                    } else {
                        
                    }

                }
                
            } else {
                print("Failure: %@", error?.localizedDescription);
            }
        }
        task.resume()
    }
    
    

    func fileManager(_ fileManager: FileManager, shouldProceedAfterError error: Error, copyingItemAtPath srcPath: String, toPath dstPath: String) -> Bool{
        if (error as NSError).code == NSFileWriteFileExistsError {
            do {
                try fileManager.removeItem(atPath: dstPath)
                print("Existing file deleted.")
            } catch {
                print("Failed to delete existing file:\n\((error as NSError).description)")
            }
            do {
                try fileManager.copyItem(atPath: srcPath, toPath: dstPath)
                print("File saved.")
            } catch {
                print("File not saved:\n\((error as NSError).description)")
            }
            return true
        } else {
            return false
        }
    }
}

