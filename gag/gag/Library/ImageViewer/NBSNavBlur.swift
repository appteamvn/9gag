//
//  NBSNavBar.swift
//  gag
//
//  Created by NamBS on 12/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class NBSNavBlur: UIVisualEffectView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblVote: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    let border = CALayer()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
//        load()
        
        border.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - 0.3, width: self.frame.size.width, height: 0.3)
        self.layer.addSublayer(border)
        
    }
    
    required override init(effect: UIVisualEffect?){
        super.init(effect: effect)
//        load()
    }
    
    func load(){
        let view: UIView? = Bundle(for: type(of: self)).loadNibNamed("NBSNavBlur", owner: self, options: nil)?.first as? UIView
        addSubview(view!)
        view?.frame = bounds
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        border.frame = CGRect(x: 0, y: self.frame.size.height - 0.3, width: self.frame.size.width, height: 0.3)
    }
    
}
