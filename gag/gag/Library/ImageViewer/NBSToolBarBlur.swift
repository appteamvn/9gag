//
//  NBSToolBar.swift
//  ImageViewer
//
//  Created by buisinam on 11/1/16.
//  Copyright © 2016 MailOnline. All rights reserved.
//

import UIKit

class NBSToolBarBlur: UIVisualEffectView {
    
    @IBOutlet weak var btnVote: UIButton!
    @IBOutlet weak var btnUnVote: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnShare: UIButton!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let border = CALayer()
        border.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 0.3)
        self.layer.addSublayer(border)
    }
    
}

