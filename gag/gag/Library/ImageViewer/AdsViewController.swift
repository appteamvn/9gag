//
//  AdsViewController.swift
//  gag
//
//  Created by buisinam on 1/15/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit
import FBAudienceNetwork

extension AdsView: ItemView {}

class AdsViewController: ItemBaseController<AdsView>,FBNativeAdDelegate {
    
    var nativeAd: FBNativeAd!
    
    var nativeAdVIew: FBNativeAdView!
    
    override init(index: Int, itemCount: Int, fetchImageBlock: @escaping FetchImageBlock, configuration: GalleryConfiguration, isInitialController: Bool = false) {
        
        super.init(index: index, itemCount: itemCount, fetchImageBlock: fetchImageBlock, configuration: configuration, isInitialController: isInitialController)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.itemView.contentMode = .scaleAspectFill
        nativeAd = FBNativeAd(placementID: "132023207205751_237959956612075")
        nativeAd.delegate = self
        nativeAd.load()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        itemView.bounds.size = aspectFitSize(forContentOfSize: itemView.bounds.size, inBounds: self.scrollView.bounds.size)
        itemView.center = scrollView.boundsCenter
    }
    
    // MARK: FBNativeAdDelegate Methods
    
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        // handleLoadedNativeAdUsingCustomViews()
        
        nativeAdVIew  = FBNativeAdView(nativeAd: nativeAd, with: FBNativeAdViewType.genericHeight300)
        
        self.scrollView.addSubview(nativeAdVIew)
        self.scrollView.removeFromSuperview()
        self.nativeAdVIew.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 300)
        nativeAdVIew.center = self.view.boundsCenter
        self.view.addSubview(nativeAdVIew)
    }
    
    
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error){
        print(error)
    }
    
    
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        print("Did tap on the ad")
    }
    
}
