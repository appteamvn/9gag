//
//  UtilityObjc.m
//  gag
//
//  Created by buisinam on 4/11/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

#import "UtilityObjc.h"

@implementation UtilityObjc
+(NSString *)encodeURL:(NSString *)urlString
{
    CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)urlString, NULL, CFSTR("'"), kCFStringEncodingUTF8);
    return (NSString *)CFBridgingRelease(newString);
}
@end
