//
//  UtilityObjc.h
//  gag
//
//  Created by buisinam on 4/11/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilityObjc : NSObject
+(NSString *)encodeURL:(NSString *)urlString;
@end
