//
//  videoPlayer.swift
//  streamAndSave
//
//  Created by buisinam on 11/26/16.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit
import AVFoundation
import CoreGraphics

class VideoPlayer: UIView {
    
    //MARK: - Accessors
    let settingControllers = SettingControllerSingleton.sharedInstance
    
    private var context = 0
    private var post:PostResponse!
    private lazy var spinnerView: SpinnerView = {
        
        var spinnerView = SpinnerView()
        
        spinnerView.frame = self.bounds
        spinnerView.spinner.center = self.center
        
        return spinnerView
    }()
    
    private lazy var thumbView: UIImageView = {
        var thumbView = UIImageView(frame: self.bounds)
        return thumbView
    }()
    
    private var playerLayer: AVPlayerLayer?
    private  var player: AVPlayer?
//    private lazy var player: AVPlayer? = {
//        
//        var player: AVPlayer = AVPlayer(playerItem: self.playerItem)
//        
//        player.actionAtItemEnd = AVPlayerActionAtItemEnd.none
//
//        return player
//    }()
    
//    private lazy var playerItem: AVPlayerItem? = {
//        
//        var playerItem: AVPlayerItem = AVPlayerItem(asset: self.asset)
//        
//        return playerItem
//    }()
    
    private lazy var asset: AVURLAsset = {
        
        var asset: AVURLAsset = AVURLAsset(url: self.url as URL)
        
        asset.resourceLoader.setDelegate(self, queue: DispatchQueue.main)
        
        return asset
    }()
    var stringURL: String! = ""
    
    var url: NSURL! {
        didSet{
            DispatchQueue.main.async {
                self.loadVideo()
            }
        }
    }
    
    //MARK: - KVO
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &self.context {
            
            if let object = object {
                
                if (object as AnyObject).isKind(of: AVPlayer.self) {
                    
                    let item = object as! AVPlayer
                    
                    switch item.status {
                        
                    case .failed:
                        
                        ()
                        
                    case .readyToPlay:
                        
                        spinnerView.removeFromSuperview()
                        if !(self.player?.isPlaying())! && settingControllers.isAutoPlayGIFPost == true{
                            self.PlayVideo()
                        }
                        
                        //screen shoot here
//                        let asset:AVURLAsset = item.asset as! AVURLAsset
//                        let imageGenerator: AVAssetImageGenerator = AVAssetImageGenerator.init(asset: asset)
//                        do{
//                            let thumb:CGImage = try imageGenerator.copyCGImage(at: CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(1.0)), actualTime: nil)
//                            thumbView.contentMode = .scaleAspectFill
//                            let image = UIImage(cgImage: thumb)
//                            thumbView.image = image
//                            self.addSubview(thumbView)
//                        }catch {
//                            
//                        }
                    case .unknown:
                        
                        ()
                    }
                }
            }
        }
    }
    override func draw(_ rect: CGRect) {
        
    }
    
    
    //MARK: - Notifications
    
    func playerItemDidReachEnd(notification: NSNotification) {
        
        if notification.object as? AVPlayerItem  == player?.currentItem {
            
            player?.pause()
            player?.seek(to: kCMTimeZero)
            player?.play()
            
            /*--------------------*/
            
            let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
            
            let filename = "filename.mp4"
            
            let documentsDirectory = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last!
            
            let outputURL = documentsDirectory.appendingPathComponent(filename)
            
            exporter?.outputURL = outputURL
            exporter?.outputFileType = AVFileTypeMPEG4
            
            exporter?.exportAsynchronously(completionHandler: {
                
                //                print(exporter?.status.rawValue)
                //                print(exporter?.error)
            })
        }
    }
    
    func playerItemPlaybackStalled(notification: NSNotification) {
        
        
            player?.pause()
        
        
        //        resumeButton.hidden = false
    }
    
    //MARK: - Deinit
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
        player?.removeObserver(self, forKeyPath: "status", context: &context)
    }
    
    //MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func loadVideo()
    {
        let screenSize = UIScreen.main.bounds
        spinnerView.frame = self.bounds
        if player?.observationInfo != nil{
            
            NotificationCenter.default.removeObserver(self)
            player?.removeObserver(self, forKeyPath: "status", context: &context)
            thumbView.removeFromSuperview()
            playerLayer?.removeFromSuperlayer()
            player?.pause()
            player = nil
        }
        
        player = AVPlayer(url : self.url as URL)
        player?.addObserver(self, forKeyPath: ("status"), options: .new, context: &context)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemPlaybackStalled(notification:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
        
        playerLayer = AVPlayerLayer(player: self.player)
        
        playerLayer?.frame = self.bounds
        playerLayer?.backgroundColor = UIColor.white.cgColor
        playerLayer?.videoGravity = AVLayerVideoGravityResizeAspect
        self.layer.addSublayer(playerLayer!)
        self.addSubview(spinnerView)
    }
    
    open func destroy(){
        NotificationCenter.default.removeObserver(self)
        player?.removeObserver(self, forKeyPath: "status", context: &context)
        thumbView.removeFromSuperview()
        playerLayer?.removeFromSuperlayer()
        player?.pause()
        player = nil
    }
    
    
    open func PlayVideo()
    {
        thumbView.isHidden = true
        player?.play()
    }
    
    open func PauseVideo()
    {
        player?.pause()
        thumbView.isHidden = false
    }
    
    open func IsPlaying() -> Bool
    {
        if url == nil{
            return false
        }
        else{
            if player != nil {
                return player!.isPlaying()
            }
                return false
        }
    }
    
    override func layoutSubviews() {
        spinnerView.frame = self.bounds
        spinnerView.center = self.center
        playerLayer?.frame = self.bounds
    }
    
}

extension VideoPlayer : AVAssetResourceLoaderDelegate {
    
}
