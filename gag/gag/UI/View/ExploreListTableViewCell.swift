//
//  ExploreListTableViewCell.swift
//  gag
//
//  Created by TamPham on 1/2/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit

protocol ExploreListCellClickDelegate {
    func clickItemAtIndex(index: Int, cell: UITableViewCell)
}

class ExploreListTableViewCell: UITableViewCell {
    var delegate: ExploreListCellClickDelegate!
    var checkCell: Bool = false
    
    var arrayString:[CategoryResponse] = []
    let sectionInsets = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 0)
}

extension ExploreListTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreListCollectionViewCell", for: indexPath) as! ExploreListCollectionViewCell
        
        
            cell.titleListLabel.text = arrayString[indexPath.item].category_name
        
            return cell
        }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                return arrayString.count
    }
    
}

extension ExploreListTableViewCell: UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil{
            delegate.clickItemAtIndex(index: indexPath.item, cell: self)
        }
    }
}

extension ExploreListTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width/2) - 0.5, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
