//
//  UploadViewCell.swift
//  gag
//
//  Created by buisinam on 1/8/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit

class UploadViewCell: UICollectionViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var progressBar: UIProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
