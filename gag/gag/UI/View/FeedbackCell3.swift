//
//  FeedbackCell3.swift
//  gag
//
//  Created by ThanhToa on 11/26/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Photos
class FeedbackCell3: UITableViewCell {
    @IBOutlet weak var FB3CollectionView: UICollectionView!

}

extension FeedbackCell3 {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        FB3CollectionView.delegate = dataSourceDelegate
        FB3CollectionView.dataSource = dataSourceDelegate
        FB3CollectionView.tag = row
        FB3CollectionView.setContentOffset(FB3CollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        FB3CollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { FB3CollectionView.contentOffset.x = newValue }
        get { return FB3CollectionView.contentOffset.x }
    }
}
