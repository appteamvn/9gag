//
//  AdsCollectionViewCell.swift
//  gag
//
//  Created by buisinam on 1/14/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit
import FBAudienceNetwork
class AdsCollectionViewCell: UICollectionViewCell,FBNativeAdDelegate {
    
    var nativeAd: FBNativeAd!
    
    var nativeAdVIew: FBNativeAdView!

    override func layoutSubviews() {
        if nativeAd  == nil{
            nativeAd = FBNativeAd(placementID: "132023207205751_237959956612075")
            nativeAd.delegate = self
            nativeAd.mediaCachePolicy = .all
            nativeAd.load()
        }
    }
    
    override func awakeFromNib() {
        
        

    }
    
    // MARK: FBNativeAdDelegate Methods
    
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        // handleLoadedNativeAdUsingCustomViews()
        
        nativeAdVIew  = FBNativeAdView(nativeAd: nativeAd, with: FBNativeAdViewType.genericHeight300)
        nativeAdVIew.frame = self.bounds
        self.addSubview(nativeAdVIew)
    }
    
    
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error){
        print(error)
    }
    
    
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        print("Did tap on the ad")
    }

}
