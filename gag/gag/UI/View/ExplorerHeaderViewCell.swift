//
//  ExplorerHeaderViewCell.swift
//  gag
//
//  Created by buisinam on 11/19/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ExplorerHeaderViewCell: UICollectionViewCell {
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    
    let gradientLayer = CAGradientLayer()
    
    override func draw(_ rect: CGRect) {
        // 2
        gradientLayer.frame = self.imgAvatar.bounds
    
        // 3
        let color1 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.05).cgColor
        let color2 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65).cgColor
        let color3 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.75).cgColor
        let color4 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.86).cgColor
        
        gradientLayer.colors = [color1, color2, color3, color4]
        
        // 4
        gradientLayer.locations = [0.0, 0.7, 0.9, 1.0]
        
        // 5
        self.imgAvatar.layer.addSublayer(gradientLayer)
    }
    
}
