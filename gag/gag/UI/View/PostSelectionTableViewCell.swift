//
//  PostSelectionTableViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/26/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class PostSelectionTableViewCell: UITableViewCell {
    @IBOutlet weak var postTypeImage: UIImageView!
    @IBOutlet weak var postTypeTitle: UILabel!
    @IBOutlet weak var postTypeSubtitle: UILabel!
    @IBOutlet weak var postCheckBoxView: VKCheckbox!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
