//
//  ProfileChangeEmailCell.swift
//  gag
//
//  Created by ToaNT1 on Nov30.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileChangeEmailCell: UITableViewCell {
    @IBOutlet weak var changeEmailIcon: UIImageView!
    @IBOutlet weak var changeEmailTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
