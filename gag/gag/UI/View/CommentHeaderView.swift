//
//  CommentHeaderView.swift
//  gag
//
//  Created by buisinam on 12/17/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import TTSegmentedControl

class CommentHeaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var segment: TTSegmentedControl!
    @IBOutlet weak var segmentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        
        addSubview(view)
//        segment.itemTitles = ["Hot","New"]
//        segment.allowChangeThumbWidth = false
////        segment.selectedTextFont = UIFont.systemFont(ofSize: 16, weight: 0.3)
////        segment.defaultTextFont = UIFont.systemFont(ofSize: 16, weight: 0.01)
//        segment.useGradient = true
//        segment.useShadow = false
//        segment.thumbShadowColor = UIColorFromRGB(0x22C6E7)
//        segment.thumbGradientColors = [ UIColorFromRGB(0x25D0EC), UIColorFromRGB(0x1EA3D8)]
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
