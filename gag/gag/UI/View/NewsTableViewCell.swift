//
//  NewsTableViewCell.swift
//  gag
//
//  Created by HieuNT50 on 11/25/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var userNewsStatus: UILabel!
    @IBOutlet weak var userNewPostImg: UIImageView!
    @IBOutlet weak var lblDatePast: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.userAvatar.layer.cornerRadius = 3
        self.userNewPostImg.layer.cornerRadius = 3
        self.userAvatar.layer.masksToBounds = true
        self.userNewPostImg.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
