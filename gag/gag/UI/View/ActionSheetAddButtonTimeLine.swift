//
//  ActionSheetAddButtonTimeLine.swift
//  gag
//
//  Created by ThanhToa on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Photos
import AssetsLibrary

protocol ActionSheetAddButtonTimeLineDelegate {
    func btnCameraAction()
    func btnGalleryAction()
    func btnCancelAction()
    func didSelectedImageAction(imageSelected: UIImage)
}

class ActionSheetAddButtonTimeLine: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    var delegateButton:ActionSheetAddButtonTimeLineDelegate!
    
    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var tapGestureRecognizerCamera = UITapGestureRecognizer()
    var tapGestureRecognizerGallery = UITapGestureRecognizer()
    // Photo framwork
    var contentMode = PHImageContentMode.aspectFill
    var imagePicked = UIImage()
    var fetchResult: PHFetchResult<PHAsset>!
        fileprivate let imageManager = PHCachingImageManager()
    fileprivate var thumbnailSize: CGSize!
    fileprivate var previousPreheatRect = CGRect.zero
//    let requestOptions = PHImageRequestOptions()
    var targetSize : CGSize!
    var selectedIdentifier: String!
    let isAuthorizePhoto = PHPhotoLibrary.authorizationStatus()

    open func reloadData(){
        PHImageRequestOptions().deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        PHImageRequestOptions().resizeMode = PHImageRequestOptionsResizeMode.exact
        PHImageRequestOptions().isNetworkAccessAllowed = true

        btnCamera.setTitle(NSLocalizedString("camera", comment: ""), for: .normal)
        btnGallery.setTitle(NSLocalizedString("gallery", comment: ""), for: .normal)

//        let width  =  collectionViewImage.frame.size.width / 4
//        let heigh  =  collectionViewImage.frame.size.height

        

        resetCachedAssets()
        PHPhotoLibrary.shared().register(self)
        self.fetchPhotosFromDevice()
        
    }
    ////     This application is not allowed to access Photo data => CRASH
    //    deinit {
    //        let status = PHPhotoLibrary.authorizationStatus()
    //        if (status == PHAuthorizationStatus.authorized) {
    //            PHPhotoLibrary.shared().unregisterChangeObserver(self as PHPhotoLibraryChangeObserver)
    //        }
    //    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if fetchResult != nil {
            return fetchResult.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Dequeue a GridViewCell.
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridViewCell", for: indexPath) as? GridViewCell
            else { fatalError("unexpected cell in collection view") }
        
        //        #if os(iOS)
        //            // Add a badge to the cell if the PHAsset represents a Live Photo.
        //            if asset.mediaSubtypes.contains(.photoLive) {
        //                cell.livePhotoBadgeImage = PHLivePhotoView.livePhotoBadgeImage(options: .overContent)
        //            }
        //        #endif
        //        if (isAuthorizePhoto == PHAuthorizationStatus.authorized) {
        let asset = fetchResult.object(at: indexPath.item)
        // Request an image for the asset from the PHCachingImageManager.
        cell.representedAssetIdentifier = asset.localIdentifier
        imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: { image, _ in
            // The cell may have been recycled by the time this handler gets called;
            // set the cell's thumbnail image only if it's still showing the same asset.
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.thumbnailImage = image
            }
        })
        //        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let asset = self.fetchResult.object(at: indexPath.item)
        updateStaticImage(asset: asset)
    }
    
    func updateStaticImage(asset: PHAsset) {
        // Prepare the options to pass when fetching the (photo, or video preview) image.
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.isNetworkAccessAllowed = true
//        options.progressHandler = { progress, _, _, _ in
//            // Handler might not be called on the main queue, so re-dispatch for UI work.
//            DispatchQueue.main.sync {
//                self.progressView.progress = Float(progress)
//            }
        //        } 
        let ratio =  CGFloat(asset.pixelWidth) / 500
        let finalSize = CGSize(width: 500,height: CGFloat(CGFloat(asset.pixelHeight) / ratio))

        
        PHImageManager.default().requestImage(for: asset, targetSize: finalSize, contentMode: .aspectFit, options: options, resultHandler: { image, _ in
            // Hide the progress view now the request has completed.
            
            // If successful, show the image view and display the image.
            guard let image = image else { return }
            DispatchQueue.main.async {
                //                let newImage = image.resized(to: finalSize)
                self.delegateButton.didSelectedImageAction(imageSelected: image)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCancel.setTitle(NSLocalizedString("cancel", comment: ""), for: .normal)
        let nib  = UINib(nibName: "GridViewCell", bundle: nil)
        collectionViewImage.register(nib, forCellWithReuseIdentifier: "GridViewCell")
        
        if (isAuthorizePhoto == PHAuthorizationStatus.authorized) {
            self.reloadData()
        } else {
            PHPhotoLibrary.requestAuthorization() {statusPhoto in
                switch statusPhoto {
                case .authorized:
                    self.reloadData()
                    break
                case .denied, .restricted:
                    Common.showAlert(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("accept_access_photo_message", comment: ""), titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                        print("not accept photo")
                    })
                    break
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
        tapGestureRecognizerCamera = UITapGestureRecognizer(target:self, action:#selector(tappedOnCameraButton(sender:)))
        tapGestureRecognizerCamera.numberOfTapsRequired  = 1
        imgCamera.isUserInteractionEnabled = true
        imgCamera.addGestureRecognizer(tapGestureRecognizerCamera)
        
        tapGestureRecognizerGallery = UITapGestureRecognizer(target:self, action:#selector(tappedOnGalerryButton(sender:)))
        tapGestureRecognizerGallery.numberOfTapsRequired  = 1
        imgGallery.isUserInteractionEnabled = true
        imgGallery.addGestureRecognizer(tapGestureRecognizerGallery)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (isAuthorizePhoto == PHAuthorizationStatus.authorized) {
            updateCachedAssets()
        } else {
            PHPhotoLibrary.requestAuthorization() {statusPhoto in
                switch statusPhoto {
                case .authorized:
                    self.updateCachedAssets()
                    break
                case .denied, .restricted:
                    Common.showAlert(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("accept_access_photo_message", comment: ""), titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                        print("not accept photo")
                    })
                    break
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        thumbnailSize = CGSize(width: 100, height: 100)
    }
    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        updateCachedAssets()
//    }


    @IBAction func btnCameraTapped(_ sender: Any) {
        delegateButton.btnCameraAction()
    }
    @IBAction func btnGalleryTapped(_ sender: Any) {
        delegateButton.btnGalleryAction()
    }
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        delegateButton.btnCancelAction()
    }
    
    func tappedOnCameraButton(sender: UITapGestureRecognizer) {
        delegateButton.btnCameraAction()
    }
    
    func tappedOnGalerryButton(sender: UITapGestureRecognizer) {
        delegateButton.btnGalleryAction()
    }
    
    // Photo framework
//    func fetchPhotos() {
//        self.fetchPhotosFromDevice()
//    }
   
    func fetchPhotosFromDevice() {
        targetSize = CGSize(width: self.collectionViewImage.frame.width / 4, height: self.collectionViewImage.frame.size.height)
        if fetchResult == nil {
            let allPhotosOptions = PHFetchOptions()
            allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            fetchResult = PHAsset.fetchAssets(with: allPhotosOptions)
            collectionViewImage.dataSource = self
            collectionViewImage.delegate = self
        }
    }
    
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width  = collectionViewImage.frame.size.width / 4
        let height = collectionViewImage.frame.size.height
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
    
    fileprivate func updateCachedAssets() {
        // Update only if the view is visible.
        guard isViewLoaded && view.window != nil else { return }
        
        // The preheat window is twice the height of the visible rect.
        let preheatRect = view!.bounds.insetBy(dx: 0, dy: -0.5 * view!.bounds.height)
        
        // Update only if the visible area is significantly different from the last preheated area.
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else { return }
        
        // Compute the assets to start caching and to stop caching.
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
            .flatMap { rect in self.collectionViewImage!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        let removedAssets = removedRects
            .flatMap { rect in self.collectionViewImage!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        
        // Update the assets the PHCachingImageManager is caching.
        imageManager.startCachingImages(for: addedAssets,
                                                   targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        imageManager.stopCachingImages(for: removedAssets,
                                                  targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        
        // Store the preheat rect to compare against in the future.
        previousPreheatRect = preheatRect
    }
    
}

// MARK: PHPhotoLibraryChangeObserver
extension ActionSheetAddButtonTimeLine: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        if (isAuthorizePhoto == PHAuthorizationStatus.authorized) {
        guard let changes = changeInstance.changeDetails(for: fetchResult)
            else { return }
        
        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            fetchResult = changes.fetchResultAfterChanges
            if changes.hasIncrementalChanges {
                // If we have incremental diffs, animate them in the collection view.
                guard let collectionView = self.collectionViewImage else { fatalError() }
                collectionView.performBatchUpdates({
                    // For indexes to make sense, updates must be in this order:
                    // delete, insert, reload, move
                    if let removed = changes.removedIndexes, removed.count > 0 {
                        self.collectionViewImage.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, inserted.count > 0 {
                        self.collectionViewImage.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let changed = changes.changedIndexes, changed.count > 0 {
                        self.collectionViewImage.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        self.collectionViewImage.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                })
            } else {
                // Reload the collection view if incremental diffs are not available.
                self.collectionViewImage!.reloadData()
            }
            resetCachedAssets()
        }
    }
    }
}

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}

