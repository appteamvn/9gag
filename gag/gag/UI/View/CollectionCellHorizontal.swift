//
//  CollectionCellHorizontal.swift
//  gag
//
//  Created by HieuNT50 on 11/11/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class CollectionCellHorizontal: UICollectionViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
