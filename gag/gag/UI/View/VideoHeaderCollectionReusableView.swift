//
//  VideoHeaderCollectionReusableView.swift
//  gag
//
//  Created by ThanhToa on 11/9/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class VideoHeaderCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var videoHeaderCollectionView: UICollectionView!
    
    let collectionHorizental = CollectionViewHorizontal()
    let cellHeight = 0, cellWidth = 0
    open func reloadData(){
        let width  =  videoHeaderCollectionView.frame.size.width
        let heigh  =  videoHeaderCollectionView.frame.size.height
        self.videoHeaderCollectionView.contentInset = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0)
        collectionHorizental.collectionView = videoHeaderCollectionView
        collectionHorizental.cellWidth = Double(width)
        collectionHorizental.cellHeight = Double(heigh)
        collectionHorizental.reloadCollectionView()
        videoHeaderCollectionView.delegate = collectionHorizental
        videoHeaderCollectionView.dataSource = collectionHorizental
    }
}
