//
//  NBSNavigationBar.swift
//  gag
//
//  Created by NamBS on 11/10/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class NBSNavigationBar: UINavigationBar {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    ///The height you want your navigation bar to be of
    static let navigationBarHeight: CGFloat = 74.0
    
    ///The difference between new height and default height
    static let heightIncrease:CGFloat = navigationBarHeight - 44
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        let shift = NBSNavigationBar.heightIncrease/2
        
//        ///Transform all view to shift upward for [shift] point
//        self.transform =
//            CGAffineTransform(translationX: 0, y: -shift)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let shift = NBSNavigationBar.heightIncrease/2
        let classNamesToReposition : [String]!
        ///Move the background down for [shift] point
//         if #available(iOS 10.0, *) {
//            // 👍
//            classNamesToReposition  =  ["_UIBarBackground"]
//        } else {
//            // 👎
//            classNamesToReposition  =   ["_UINavigationBarBackground"]
//        }
        classNamesToReposition  =   ["_UINavigationBarBackground"]
        
        for view: UIView in self.subviews {
            if classNamesToReposition.contains(NSStringFromClass(type(of: view))) {
                let bounds: CGRect = self.bounds
                var frame: CGRect = view.frame
                if #available(iOS 10.0, *) {
                    frame.origin.y = bounds.origin.y + shift - 20.0
                    frame.size.height = bounds.size.height + 20.0
                    view.frame = frame
                }
                else{
                    frame.origin.y = bounds.origin.y - 20.0
                    frame.size.height = bounds.size.height + 20.0
                    view.frame = frame
                }
            }
        }
        
        self.setTitleVerticalPositionAdjustment(-40, for: .default)
    }
    
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        let shift = NBSNavigationBar.heightIncrease/2
//        
//        ///Move the background down for [shift] point
//        let classNamesToReposition = ["_UINavigationBarBackground"]
//        
//        for view: UIView in self.subviews {
//            
//            if classNamesToReposition.contains(NSStringFromClass(view.classForCoder)) {
//                let bounds: CGRect = self.bounds
//                var frame: CGRect = view.frame
//                frame.origin.y = bounds.origin.y + shift - 20.0
//                frame.size.height = bounds.size.height + 20.0
//                view.frame = frame
//            }
//        }
//    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let amendedSize:CGSize = super.sizeThatFits(size)
        let newSize:CGSize = CGSize(width: amendedSize.width, height: NBSNavigationBar.navigationBarHeight);
        return newSize;
    }
}
