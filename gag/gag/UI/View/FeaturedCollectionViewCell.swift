//
//  FeaturedCollectionViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class FeaturedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var featuredImage: UIImageView!
    @IBOutlet weak var featuredTitle: UILabel!
    @IBOutlet weak var featuredPoint: UILabel!
    
    
    let gradientLayer = CAGradientLayer()
    
    override func draw(_ rect: CGRect) {
        // 2
        gradientLayer.frame = self.featuredImage.bounds
        
        // 3
        let color1 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.05).cgColor
        let color2 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.85).cgColor
        let color3 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.95).cgColor
        let color4 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).cgColor
        
        gradientLayer.colors = [color1, color2, color3, color4]
        
        // 4
        gradientLayer.locations = [0.0, 0.7, 0.9, 1.0]
        
        // 5
        self.featuredImage.layer.addSublayer(gradientLayer)
    }
}
