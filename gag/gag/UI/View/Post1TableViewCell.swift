//
//  Post1TableViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/16/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class Post1TableViewCell: UITableViewCell {
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var postMessageCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
