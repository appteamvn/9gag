//
//  Post3TableViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/16/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class Post3TableViewCell: UITableViewCell {
    @IBOutlet weak var post3Message: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
