//
//  ProfileSettingTableViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/21/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var profileSettingSwt: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
