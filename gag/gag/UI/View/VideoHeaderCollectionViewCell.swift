//
//  VideoHeaderCollectionViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/9/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class VideoHeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var videoHeaderImage: UIImageView!
    @IBOutlet weak var videoHeaderTitle: UILabel!
}
