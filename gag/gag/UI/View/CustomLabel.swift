//
//  CustomLabel.swift
//  gag
//
//  Created by ThanhToa on 11/2/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 10.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 10.0
    
//    override var text: String? {
//        didSet {
//            guard let text = text else { return }
//            let textRange = NSMakeRange(0, text.characters.count)
//            let attributedText = NSMutableAttributedString(string: text)
//            attributedText.addAttribute(NSUnderlineStyleAttributeName, value:NSUnderlineStyle.styleSingle.rawValue, range: textRange)
//            self.attributedText = attributedText
//        }
//    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
