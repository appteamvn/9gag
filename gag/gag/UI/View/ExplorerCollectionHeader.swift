//
//  ExplorerCollectionHeader.swift
//  gag
//
//  Created by buisinam on 11/19/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

protocol ExplorerCollectionHeaderDelegate {
     func cellForCollectionView(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int)
}

class ExplorerCollectionHeader: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var delegate: ExplorerCollectionHeaderDelegate!
    
    //collectionview
    var collectionView:UICollectionView!
    var currentArray:[AnyObject]!
    var isShowLabel = true
    // properties cell
    open var cellWidth = 0.0
    open var cellHeight = 0.0
    
    override init() {
        super.init()
    }
    
    func reloadData()
    {
        
        collectionView.dataSource = self
        collectionView.delegate = self
//        collectionView.reloadData()
    }
    
    var videoTitleArrayStub = ["This has got to be one of most awesome pictures of WW2 Engineers can relate So Accurate", "This has got to be one of most awesome pictures of WW2. ","Engineers can relate So Accurate", "This has got to be one of most awesome pictures of WW2 Engineers can relate So Accurate", "This has got to be one of most awesome pictures of WW2. ","Engineers can relate So Accurate"]
    var videoImageArrayStub = ["postImage1.jpg", "postImage2.jpg","postImage3.jpg", "postImage1.jpg", "postImage2.jpg","postImage3.jpg"]
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return currentArray.count
        return videoTitleArrayStub.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cellIdentifier = "FadeCell"
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! ExplorerHeaderViewCell
        cell.lblTitle.text = videoTitleArrayStub[indexPath.row]
        if delegate != nil{
            delegate.cellForCollectionView(collectionViewCell: cell, cellForItemAt: indexPath.item)
        }
        
        return cell

    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
}
