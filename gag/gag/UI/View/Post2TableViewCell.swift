//
//  Post2TableViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/16/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
protocol Post2TableViewCellDelegate {
    func switchAction(state:Bool)
    }
class Post2TableViewCell: UITableViewCell {
    @IBOutlet weak var post2Message: UILabel!
    @IBOutlet weak var post2Switch: UISwitch!
    @IBAction func switchAction(_ sender: Any) {
        let state = self.post2Switch.isOn ? true : false
//        delegateButton.switchAction(state: state)
    }
    var delegateButton:Post2TableViewCellDelegate!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
