//
//  ProfileCollectionReusableView.swift
//  gag
//
//  Created by ThanhToa on 11/20/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var myFunnyCollection: UILabel!
    
    open func reloadData(){
        userAvatar.layer.cornerRadius = userAvatar.bounds.width/2
        userAvatar.layer.masksToBounds = true
    }
}
