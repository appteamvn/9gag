//
//  PostViewCell.swift
//  gag
//
//  Created by buisinam on 12/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class PostViewCell: UITableViewCell {

    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postPoints: UILabel!
    @IBOutlet weak var postLikeButton: UIButton!
    @IBOutlet weak var postUnlikeButton: UIButton!
    @IBOutlet weak var postCommentButton: UIButton!
    @IBOutlet weak var postMoreButton: UIButton!
    @IBOutlet weak var postShareButton: UIButton!
    @IBOutlet weak var videoView: VideoPlayer!
    @IBOutlet weak var btnGift: UIButton!
    var postLink: String!

    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
