//
//  VideoCollectionViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/9/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class VideoContentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var videoContentImage: UIImageView!
    @IBOutlet weak var videoContentTitle: UILabel!
}
