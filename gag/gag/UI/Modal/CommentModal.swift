//
//  CommentModal.swift
//  gag
//
//  Created by buisinam on 12/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import TUSafariActivity
import Kingfisher

class CommentModal: NSObject,GalleryItemsDataSource, GalleryItemsDelegate {
    
    open var controller : UITableViewController!
    open var postCell: PostViewCell!
    open var dataPost: PostResponse!
    
    var galleryViewController:GalleryViewController!
    
    let nbsToolbar  = UINib(nibName: "NBSToolBarBlur", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NBSToolBarBlur
    
    let nbsNav = UINib(nibName: "NBSNavBlur", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NBSNavBlur
    
    var placeImage : UIImageView!
    
    override init() {
        // add button
        super.init()
        
    }
    
    func tappedOnPostImage(sender: UITapGestureRecognizer) {
        
        setUpToolBar()
        setUpButtonBar()
        reloadNavBarData(index: 0)
        
        placeImage = sender.view as! UIImageView
        
        guard (sender.view as? UIImageView) != nil else { return }
        let displacedViewIndex = placeImage.tag
        
        galleryViewController = GalleryViewController(startIndex: displacedViewIndex, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: galleryConfiguration())
        
        galleryViewController.headerView = nbsNav
        galleryViewController.footerView = nbsToolbar
        galleryViewController.launchedCompletion = { self.reloadNavBarData(index: displacedViewIndex) }
        galleryViewController.closedCompletion = { UIApplication.shared.statusBarStyle = .default}
        galleryViewController.swipedToDismissCompletion = { UIApplication.shared.statusBarStyle = .default
        self.galleryViewController = nil
        }
        galleryViewController.landedPageAtIndexCompletion = { index in
            
        }
        
        controller.presentImageGallery(galleryViewController)
        
    }
    
    func shareActionNormal(sender: Any) {
        shareAction(sender: (sender as AnyObject))
    }
    
    func commentNormalAction(sender: Any) {
        commentAction(sender: (sender as AnyObject))
    }
    
    func voteNormalAction(sender: Any)
    {
        if BaseDAO.checkLogin() {
            voteLikeAction(index: (sender as AnyObject).tag)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            
            let nav = UINavigationController(rootViewController: vc)
            controller.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func voteUnNormalAction(sender: Any)
    {
        if BaseDAO.checkLogin() {
            voteUnLikeAction(index: (sender as AnyObject).tag)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            controller.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    //voteAction
    func voteLikeAction(index: Int) {
        weak var data = dataPost
        if data?.is_vote == 0 {
            let cell  = postCell as PostViewCell
            data?.post_point += 1
            cell.postLikeButton.tintColor = Common.blueColor()
            cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 1
            
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        } else if data?.is_vote == 1 {
            let cell  = postCell as PostViewCell
            data?.post_point -= 1
            cell.postLikeButton.tintColor = Common.lighGrayColor()
            cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 0
            
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        } else {
            let cell  = postCell as PostViewCell
            data?.post_point += 2
            cell.postLikeButton.tintColor = Common.blueColor()
            cell.postUnlikeButton.tintColor = Common.lighGrayColor()
            cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 1
            
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        }
        
    }
    
    func voteUnLikeAction(index: Int) {
        weak var data = dataPost as PostResponse
        if data?.is_vote == 0  {
            let cell  = postCell as PostViewCell
            data?.post_point -= 1
            cell.postUnlikeButton.tintColor = Common.blueColor()
            cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = -1
            
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        }else if data?.is_vote == -1 {
            let cell  = postCell as PostViewCell
            data?.post_point += 1
            cell.postUnlikeButton.tintColor = Common.lighGrayColor()
            cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 0
            
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        }else {
            let cell  = postCell as PostViewCell
            data?.post_point -= 2
            cell.postUnlikeButton.tintColor = Common.blueColor()
            cell.postLikeButton.tintColor = Common.lighGrayColor()
            cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = -1
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
            
        }
    }
    
    //MARK: - Imageviewer Delegate
    
    func itemCount() -> Int {
        return 1
    }
    
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        
        return  nil
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let postResponse = dataPost as PostResponse
        
        if postResponse.post_type == PostType.Video.rawValue {
            return GalleryItem.video(fetchPreviewImageBlock: { $0(UIImage())} , videoURL: URL(string: postResponse.url)!)
        }
        else {
            let url = URL(string: postResponse.url)
            return GalleryItem.image { callback in
                KingfisherManager.shared.retrieveImage(with: ImageResource(downloadURL: url!), options: [], progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
                    callback(image)
                })
            }
            
        }

    }
    
    func removeGalleryItem(at index: Int){
        
    }

    
    
    func galleryConfiguration() -> GalleryConfiguration {
        
        return [
            
            GalleryConfigurationItem.pagingMode(.standard),
            GalleryConfigurationItem.presentationStyle(.displacement),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            GalleryConfigurationItem.maximumZoomScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.linear),
            GalleryConfigurationItem.statusBarHidden(false),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(false),
            GalleryConfigurationItem.displacementInsetMargin(50),
            GalleryConfigurationItem.closeButtonMode(.none)
        ]
    }
    
    //MARK: - FullScreen TOOLBAR
    func voteAction(sender: Any)
    {
        let index: UIButton = sender as! UIButton
        let resultData = dataPost as PostResponse
        switch resultData.is_vote {
        case 0:
            
            resultData.post_point += 1
            self.nbsToolbar.btnVote.tintColor = Common.blueColor()
            nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
            resultData.is_vote = 1
            
            PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    resultData.post_point = Int(result!)
                    self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                    self.loadlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!, rowIndex: index.tag)
                }
            }
            break
        case 1:
            resultData.post_point -= 1
            self.nbsToolbar.btnVote.tintColor = UIColor.white
            nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
            resultData.is_vote = 0
            
            PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    resultData.post_point = Int(result!)
                    self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                    self.loadlikeHome(color: Common.lighGrayColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                }
            }
            break
        case -1:
            resultData.post_point += 2
            self.nbsToolbar.btnVote.tintColor = Common.blueColor()
            self.nbsToolbar.btnUnVote.tintColor = UIColor.white
            nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
            resultData.is_vote = 1
            
            PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    resultData.post_point = Int(result!)
                    self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                    self.loadlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                }
            }
            break
        default:
            break
        }
    }
    
    func loadlikeHome (color: UIColor, numberLike: String, rowIndex: Int)
    {
        let cell = postCell as PostViewCell
        cell.postLikeButton.tintColor = color
        cell.postUnlikeButton.tintColor = Common.lighGrayColor()
        cell.postPoints.text = numberLike
        
    }
    
    func loadUnlikeHome (color: UIColor, numberLike: String, rowIndex: Int)
    {
        let cell = postCell as PostViewCell
        cell.postUnlikeButton.tintColor = color
        cell.postLikeButton.tintColor = Common.lighGrayColor()
        cell.postPoints.text = numberLike
    }
    
    func unVoteAction(sender: Any)
    {
        let index: UIButton = sender as! UIButton
        let resultData = dataPost as PostResponse
        switch resultData.is_vote {
        case 0:
            resultData.post_point -= 1
            self.nbsToolbar.btnUnVote.tintColor = Common.blueColor()
            nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
            resultData.is_vote = -1
            
            PostDAO.userVote(post_id: Int(resultData.post_id)!, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    resultData.post_point = Int(result!)
                    self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                    self.loadUnlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                }
            }
            break
        case 1:
            resultData.post_point -= 2
            self.nbsToolbar.btnUnVote.tintColor = Common.blueColor()
            self.nbsToolbar.btnVote.tintColor = UIColor.white
            nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
            resultData.is_vote = -1
            
            PostDAO.userVote(post_id: Int(resultData.post_id)!, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    resultData.post_point = Int(result!)
                    self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                    self.loadUnlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                }
            }
            break
        case -1:
            resultData.post_point += 1
            self.nbsToolbar.btnUnVote.tintColor = UIColor.white
            nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
            resultData.is_vote = 0
            
            PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    resultData.post_point = Int(result!)
                    self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                    self.loadUnlikeHome(color: Common.lighGrayColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                }
            }
            break
        default:
            break
        }
        
    }
    
    func commentAction(sender: Any)
    {
        
        if galleryViewController != nil{
            
            let vc = CommentTableViewController()
            vc.post_id = dataPost.post_id
            vc.isPresented = true
            galleryViewController.presentDetail(UINavigationController(rootViewController: vc))
        }

    }
    
    func moreAction(sender: Any)
    {
        
    }
    
    func shareAction(sender:Any)
    {
        // share action
        if (self.postCell.postTitle.text?.isEmpty)! ||  self.postCell.postImage.image == nil{
            return
        }
        let linkToShare = URL(string: self.postCell.postLink)
        let imgToShare = self.postCell.postImage.image
        var activityItems: [AnyObject]?
        let activity = TUSafariActivity()
        activityItems = [linkToShare as AnyObject , imgToShare!]
        let actionSheetShare = UIActivityViewController(activityItems: activityItems!, applicationActivities: [activity])
        if galleryViewController != nil {
            galleryViewController.present(actionSheetShare, animated: true, completion: nil)
        } else {
            self.controller.present(actionSheetShare, animated: true, completion: nil)
        }
    }
    
    func setUpButtonBar(){
        nbsNav.btnClose.addTarget(self, action: #selector(tapCloseFullScreen), for: .touchUpInside)
        
        var frame = nbsToolbar.frame
        frame.size.width = controller.view.frame.width
        nbsToolbar.frame = frame
        
        frame = nbsNav.frame
        frame.size.width = controller.view.frame.width
        nbsNav.frame = frame
        
    }
    
    func setUpToolBar()
    {
        nbsToolbar.btnVote.addTarget(self, action: #selector(voteAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnUnVote.addTarget(self, action: #selector(unVoteAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnComment.addTarget(self, action: #selector(commentAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnMore.addTarget(self, action: #selector(moreAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnShare.addTarget(self, action: #selector(shareAction(sender:)), for: .touchUpInside)
        
    }
    
    func reloadNavBarData(index: Int){
        let data = dataPost as PostResponse
        nbsNav.lblTitle.text = data.post_title
        nbsNav.lblVote.text =  "\(data.post_point)" + NSLocalizedString("point", comment: "") + data.post_comment + NSLocalizedString("comment", comment: "")
        nbsToolbar.btnUnVote.tag = index
        nbsToolbar.btnVote.tag = index
        switch data.is_vote {
        case 0:
            nbsToolbar.btnVote.tintColor = UIColor.white
            nbsToolbar.btnUnVote.tintColor = UIColor.white
            break
        case 1:
            nbsToolbar.btnVote.tintColor = Common.blueColor()
            nbsToolbar.btnUnVote.tintColor = UIColor.white
            break
        case -1:
            nbsToolbar.btnUnVote.tintColor = Common.blueColor()
            nbsToolbar.btnVote.tintColor = UIColor.white
            break
        default:
            break
        }
    }
    
    func tapCloseFullScreen()
    {
        UIApplication.shared.statusBarStyle = .default
        galleryViewController.closeGallery(true, completion: nil)
        galleryViewController = nil
    }
}
extension UIImageView: DisplaceableView {}
