//
//  PostTableViewController.swift
//  gag
//
//  Created by ThanhToa on 11/16/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class PostTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate, UINavigationControllerDelegate, Post2TableViewCellDelegate, UITextViewDelegate{
    internal func switchAction(state: Bool) {
        if state {
            self.switchBtn = 1
        } else {
            self.switchBtn = 0
        }
    }
    open var postImageReceived = UIImage()
    
    @IBOutlet weak var postCancel: UIBarButtonItem!
    @IBOutlet weak var postNext: UIBarButtonItem!
    var navigationbar = UINavigationController()
    @IBOutlet weak var postTableView: UITableView!
    var switchBtn = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        postTableView.delegate = self
        postTableView.dataSource = self
        
//        let navItems = UINavigationItem()
//        let imgCancel = UIImage(named: "close-normal")
//         let  navCancel = UIBarButtonItem(image: imgCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(btn_CancelClicked(sender:)))
        let navCancel = UIBarButtonItem(title: NSLocalizedString("cancel", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(btn_CancelClicked(sender:)))
        let navNext = UIBarButtonItem(title: NSLocalizedString("next", comment: ""), style: UIBarButtonItemStyle.done, target: self, action: #selector(btn_NextClicked(sender:)))
        
        self.navigationItem.rightBarButtonItem = navNext
        self.navigationItem.leftBarButtonItem = navCancel
        self.title = NSLocalizedString("post", comment: "")
//
//        nav.items = [navItems]
//        self.view .addSubview(nav)
//        
//        self.navigationController?.delegate = self
        self.postTableView.reloadData()
    }
    
    func btn_CancelClicked(sender: UIBarButtonItem) {
        // Do something
        self.dismiss(animated: true, completion: nil)
    }
    
    func btn_NextClicked(sender: UIBarButtonItem) {
        // Do something
        let cell1 = getCellAtIndexPath(index: 0) as! Post1TableViewCell
        let cell2 = getCellAtIndexPath(index: 1) as! Post2TableViewCell
        if cell1.txtMessage.text.characters.count <= 8 {
            showAlertMessage(title: NSLocalizedString("error_nil_post_title", comment: ""),
                             message: NSLocalizedString("error_nil_post_message", comment: ""))
        } else {
            let postSelectionTableView = self.storyboard?.instantiateViewController(withIdentifier: "postSelectionTableView") as! PostSelectionTableView
            postSelectionTableView.post_title = cell1.txtMessage.text
            postSelectionTableView.post_safe = cell2.post2Switch.isOn ? "1" : "0"
            postSelectionTableView.data = cell1.postImage.image!
            self.navigationController?.pushViewController(postSelectionTableView, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1Identifier = "postCell1Identifier"
        let cell2Identifier = "postCell2Identifier"
        let cell3Identifier = "postCell3Identifier"
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cell1Identifier, for: indexPath) as! Post1TableViewCell
            cell.postImage.image = postImageReceived
            cell.txtMessage.text = NSLocalizedString("describe_post", comment: "")
            cell.txtMessage.textColor = UIColor.lightGray
            cell.txtMessage.delegate = self
            cell.postMessageCount.text = "23"
            return cell
        }
        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: cell2Identifier, for: indexPath) as! Post2TableViewCell
            cell.post2Message.text = NSLocalizedString("nsfw", comment: "")
            if self.switchBtn == 0 {
                cell.post2Switch.isOn = false
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: cell3Identifier, for: indexPath) as! Post3TableViewCell
            cell.post3Message.text = NSLocalizedString("nsfw_caution", comment: "")
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 106
        } else if indexPath.row == 1  {
            return 0
        } else {
            return 67
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = getCellAtIndexPath(index: indexPath.item)
    }
    
    @IBAction func postCancelAction(_ sender: Any) {
//        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "") as! HomeViewController
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func postNextAction(_ sender: Any) {
        
        let cell1 = getCellAtIndexPath(index: 0) as! Post1TableViewCell
        
        let postSelectionTableView = self.storyboard?.instantiateViewController(withIdentifier: "postSelectionTableViewIdentifier") as! PostSelectionTableView
        postSelectionTableView.post_title = cell1.txtMessage.text
        postSelectionTableView.post_safe = "\(self.switchBtn)"
        postSelectionTableView.data = cell1.postImage.image!
        self.show(postSelectionTableView, sender: nil)
    }
    
    func getCellAtIndexPath(index : Int) -> UITableViewCell
    {
        let indexPath = IndexPath(item: index, section: 0)
            let cell = self.postTableView.cellForRow(at: indexPath)
            return cell!
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        let cell = getCellAtIndexPath(index: 0) as! Post1TableViewCell
        if cell.txtMessage.text == NSLocalizedString("describe_post", comment: "") {
            cell.txtMessage.text = ""
            cell.txtMessage.textColor = UIColor.black
        }
        cell.txtMessage.becomeFirstResponder()
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        let cell = getCellAtIndexPath(index: 0) as! Post1TableViewCell
        if cell.txtMessage.text.isEmpty {
            cell.txtMessage.text = NSLocalizedString("describe_post", comment: "")
            cell.txtMessage.textColor = UIColor.lightGray
        }
        cell.txtMessage.resignFirstResponder()
    }
    
    func showAlertMessage(title: String, message: String) {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        alert.show()
    }
}
