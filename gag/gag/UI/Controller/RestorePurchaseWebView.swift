//
//  RestorePurchaseWebView.swift
//  gag
//
//  Created by ThanhToa on 11/23/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class RestorePurchaseWebView: UIViewController {
    @IBOutlet weak var restorePurchaseWebview: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let purchaseURL = NSURL(string: NSLocalizedString("purchase_url", comment: ""))
        let request = NSURLRequest(url: purchaseURL as! URL)
        restorePurchaseWebview.loadRequest(request as URLRequest)

    }
}
