//
//  SecondViewController.swift
//  gag
//
//  Created by buisinam on 10/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Kingfisher

class NewsViewController: BaseTableViewController {

    var userDefaults = UserDefaults()
    var tableViewCell: UITableView!
    var currentArray:[NewResponse]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        self.navigationItem.title = NSLocalizedString("news", comment: "")
        self.checkLogin()
        queryNotify()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkLogin()
        let user_id = BaseDAO.getUserId()
        let reachability = Reachability()
        if (reachability?.isReachable)! {
            if user_id != 0 {
                self.setViewData()
            }
        }
    }
    
    override func refreshCall() {
        queryNotify()
    }
    
    func queryNotify()
    {
        NewDAO.getNewList(since: "", last: "") { (success: Bool, data) in
            if success {
                self.currentArray = data
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
            }
            self.refControl.endRefreshing()
        }
    }
    
    func setViewData() {
        
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentArray != nil {
        return currentArray.count
        }
        return 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let newResponse = currentArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsTableViewCellIdentifier", for: indexPath) as! NewsTableViewCell
//        cell.userAvatar.image = UIImage(named: newResponse.that_avatar[indexPath.row])
        cell.userAvatar.kf.setImage(with: ImageResource(downloadURL: URL(string: newResponse.that_avatar)!,cacheKey: nil))
        let fullString = newResponse.first_str + " " + newResponse.second_str
        cell.userNewsStatus.textColor = .lightGray
        cell.userNewsStatus.attributedText = attributeText(fullText: fullString, colorString: [newResponse.first_str])
        if cell.lblDatePast == nil {return cell}
        cell.lblDatePast.text = newResponse.date_past
        if newResponse.is_read == 1{
            cell.backgroundColor = .white
        }
        else{
            cell.backgroundColor = UIColor(red: 246, green: 246, blue: 246, alpha: 1)
        }
        
        return cell
    }
    
    public func delay(bySeconds seconds: Double, dispatchLevel: DispatchLevel = .main, closure: @escaping () -> Void) {
        let dispatchTime = DispatchTime.now() + seconds
        dispatchLevel.dispatchQueue.asyncAfter(deadline: dispatchTime, execute: closure)
    }
    
    public enum DispatchLevel {
        case main, userInteractive, userInitiated, utility, background
        var dispatchQueue: DispatchQueue {
            switch self {
            case .main:                 return DispatchQueue.main
            case .userInteractive:      return DispatchQueue.global(qos: .userInteractive)
            case .userInitiated:        return DispatchQueue.global(qos: .userInitiated)
            case .utility:              return DispatchQueue.global(qos: .utility)
            case .background:           return DispatchQueue.global(qos: .background)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = currentArray[indexPath.row]
        
        if data.is_read == 0{
            data.is_read = 1
            tableView.reloadRows(at: [indexPath], with: .fade)
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
            (self.tabBarController as! MainTabbarController).reloadBadge()
        }
        
        
        let vcDetail = CommentTableViewController()
        vcDetail.post_id = data.post_id
        if data.type == "comment" {
            self.show(vcDetail, sender: nil)
        } else {
            vcDetail.isDetail = true
            vcDetail.isNews = true
            vcDetail.isComment = false
            self.show(vcDetail, sender: nil)
        }
    }
    
    func checkLogin() {
        if !BaseDAO.checkLogin() {
            let join9GagViewController = self.storyboard?.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            self.show(join9GagViewController, sender: nil)
        } else {
            
        }
    }
    
    // MARK: UICollectionView Delegate
    func attributeText(fullText : String, colorString: [String]) -> NSAttributedString
    {
        let text = fullText
        let nsText = text as NSString
        let textRange = NSMakeRange(0, nsText.length)
        let attributedString = NSMutableAttributedString(string: text)
        
        var strPlace = ""
        
        nsText.enumerateSubstrings(in: textRange, options: .byWords, using: {
            (substring, substringRange, _, _) in
            
            if colorString.contains(substring!) {
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: substringRange)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 15.0), range: substringRange)
                strPlace = ""
            }
            else{
                strPlace = (strPlace + " " + substring!).trimmingCharacters(in: .whitespacesAndNewlines)
                
                if colorString.contains(strPlace.trimmingCharacters(in: .whitespacesAndNewlines)) {
                    let rangeSub  = fullText.range(of: strPlace)
                    let lPos = fullText.distance(from: fullText.startIndex, to: (rangeSub?.lowerBound)!)
                    let uPos = fullText.distance(from: fullText.startIndex, to: (rangeSub?.upperBound)!)
                    let range = NSMakeRange(lPos, uPos)
                    
                    
                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: range)
                    attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 15.0), range: range)
                    strPlace = ""
                }
                
            }
        })
        return attributedString
    }
    
}



