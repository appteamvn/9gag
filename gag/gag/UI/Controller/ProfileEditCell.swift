//
//  ProfileEditCell.swift
//  gag
//
//  Created by ThanhToa on 11/23/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileEditCell: UITableViewCell {

    @IBOutlet weak var editProfileImage: UIImageView!
    @IBOutlet weak var editProfileTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        editProfileImage.layer.cornerRadius = editProfileImage.frame.height/2
        editProfileImage.layer.masksToBounds = true
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
