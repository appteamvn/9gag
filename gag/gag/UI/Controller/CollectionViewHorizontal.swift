//
//  CollectionViewHorizontal.swift
//  gag
//
//  Created by HieuNT50 on 11/11/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class CollectionViewHorizontal: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    //collectionview
    var collectionView:UICollectionView!
    var currentArray:[AnyObject]!
    var isShowLabel = true
    // properties cell
    open var cellWidth = 0.0
    open var cellHeight = 0.0

    var videoTitleArrayStub = ["This has got to be one of most awesome pictures of WW2 Engineers can relate So Accurate", "This has got to be one of most awesome pictures of WW2. ","Engineers can relate So Accurate", "This has got to be one of most awesome pictures of WW2 Engineers can relate So Accurate", "This has got to be one of most awesome pictures of WW2. ","Engineers can relate So Accurate"]
    var videoImageArrayStub = ["postImage1.jpg", "postImage2.jpg","postImage3.jpg", "postImage1.jpg", "postImage2.jpg","postImage3.jpg"]
    
    @nonobjc func cellForCollectionView(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int){

    }
    
    func reloadCollectionView()
    {
        
        let nib  = UINib(nibName: "CollectionCellHorizontal", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "CollectionViewHorizontalIdentifier")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return currentArray.count
                return videoTitleArrayStub.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "CollectionViewHorizontalIdentifier"
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! CollectionCellHorizontal
        cellForCollectionView(collectionViewCell: cell, cellForItemAt: indexPath.item)
        
        if (isShowLabel) {
            cell.cellLabel.text = String(format: "%@", videoTitleArrayStub[indexPath.row])
            cell.cellImage.image = UIImage(named: videoImageArrayStub[indexPath.row])
            cell.cellLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        } else {
            cell.cellImage.image = UIImage(named: videoImageArrayStub[indexPath.row])
            cell.cellLabel.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: cellWidth, height: cellHeight)
    }
    

}
