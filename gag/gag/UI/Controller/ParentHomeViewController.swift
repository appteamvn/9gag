//
//  ParentHomeViewController.swift
//  gag
//
//  Created by buisinam on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import TUSafariActivity

class ParentHomeViewController: BasePageViewController {
    
    var destinationVC: HomePageViewController!
    var isProfile = false
    var isExplore = false
    
    var isSearch = false
    var searchData: String = ""
    var caterogy_id:Int = 0
    var titleNavigation: String = ""
    var userDefaults = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isProfile{

            self.navigationItem.title = self.userDefaults.object(forKey: Common.USER_NAME) as! String?
            let settingBtn = UIBarButtonItem(title: NSLocalizedString(NSLocalizedString("share", comment: ""), comment: ""), style: .plain, target: self, action: #selector(shareAction(sender:)))
            self.navigationItem.rightBarButtonItem = settingBtn
        }
        
    }
    
    func shareAction(sender:Any)
    {
        var activityItems: [AnyObject]?
        let activity = TUSafariActivity()
        let linkToShare = URL(string: NSLocalizedString("web_app_link", comment: ""))
        activityItems = [linkToShare as AnyObject]
        let actionSheetShare = UIActivityViewController(activityItems: activityItems!, applicationActivities: [activity])
        self.present(actionSheetShare, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func segmentSelection(selection: Int) {
        destinationVC.segmentSelection(selection: selection)
    }
    
    private func findShadowImage(under view: UIView) -> UIImageView? {
        if view is UIImageView && view.bounds.size.height <= 1 {
            return (view as! UIImageView)
        }
        
        for subview in view.subviews {
            if let imageView = findShadowImage(under: subview) {
                return imageView
            }
        }
        return nil
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        UIApplication.shared.statusBarStyle = .default
        if isSearch || isExplore || isProfile{
            self.navigationController?.navigationBar.hideBottomHairline()
        }
        
        if isProfile == true{
            arrTitleSegment = [NSLocalizedString("overview", comment: ""), NSLocalizedString("post", comment: ""), NSLocalizedString("upvote", comment: ""), NSLocalizedString("comment", comment: "")]
            let view = setUpdateSegmentExtendView(isEqual: false)
            destinationVC.extendView = view
            return
        } else if isExplore {
            arrTitleSegment = [NSLocalizedString("hot", comment: ""), NSLocalizedString("fresh", comment: "")]
            self.navigationItem.title = titleNavigation
            let view = setUpdateSegmentExtendView(isEqual: true)
            destinationVC.extendView = view
            return
        } else if isSearch {
            arrTitleSegment = nil
            isSeagmenVisible = false
            self.navigationController?.navigationBar.isTranslucent = false
            self.automaticallyAdjustsScrollViewInsets = false
        }
        else {
            // Do any additional setup after loading the view.
            arrTitleSegment = [NSLocalizedString("hot", comment: ""), NSLocalizedString("trending", comment: ""), NSLocalizedString("fresh", comment: "")]
        }
        
        // Setup segmentview
        if !isSearch {
            if homeSegment == nil{
                setUpdateSegmentView(isEqual: true)
            }
            else{
                self.navigationController?.navigationBar.addSubview(self.homeSegment)
            }
        }
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isSearch {
            self.homeSegment.removeFromSuperview()
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? HomePageViewController {
            vc.isProfile = isProfile
            vc.isExplore = isExplore
            vc.category_id = caterogy_id
            vc.isSearch = isSearch
            vc.searchData = searchData
            vc.parentVC = self
            destinationVC = vc
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        if isProfile {
            self.removeHeader()
        }
    }
    
    override func setSelectedSegment(index: Int) {
        super.setSelectedSegment(index: index)
    }
    
}
