//
//  UITimelineViewController.swift
//  gag
//
//  Created by buisinam on 11/9/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Kingfisher
import FBAudienceNetwork

enum LoadMoreStatus {
    case Finished
    case Loading
}

class UITimelineViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,DownloadDelegate {
    
    let timelineCellIndentifier = "TimeLineCellIdentifier"
    let uploadCellIndentifier = "UploadViewCell"
    let adsCellIndentifier = "AdsCollectionViewCell"
    
    var isProfile = false
    let cornerRadius:CGFloat = 3.0
    
    let screenSize = UIScreen.main.bounds
    
    let settingControllers = SettingControllerSingleton.sharedInstance
    
    //collectionview
    var collectionView:UICollectionView!
    var currentArray:[AnyObject]!
    
    var loadingStatus = LoadMoreStatus.Finished
    var queryStatus = LoadMoreStatus.Finished
    
    var userDefaults = UserDefaults()
    
    @nonobjc func cellForCollectionView(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int){}
    @nonobjc func didSelectItemAtIndex(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int){}
    @nonobjc func reloadHandelPost(){}
    
    var url: URL!
    
    @nonobjc func cellDidEndDisplay(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int){
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.userDefaults.object(forKey: Common.USER_AVATAR) != nil && BaseDAO.getUserId() != 0 {
            url = self.setAvatarImg()
        }
        
        // Do any additional setup after loading the view.
                
        // Setup collection
        if collectionView != nil {
            collectionView.register(AdsCollectionViewCell.self, forCellWithReuseIdentifier: adsCellIndentifier)
//            self.collectionView.register(UINib(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: adsCellIndentifier)
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
        
        if isProfile{
            PostDAO.PostDaoSingleton.sharedInstance.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
            self.collectionView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadComplete(complete: Bool, progress: Double, index: Int) {
        let indedexPath = IndexPath(item: index, section: 0)
        if complete == true{
        
                self.collectionView.reloadData()
                self.reloadHandelPost()
        }
        else{
            if let cell: UploadViewCell = self.collectionView.cellForItem(at: indedexPath) as? UploadViewCell{
                cell.progressBar.progress = Float(progress)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //MARK: UICollectionView Datasources, UICollectionView Delegate
    
    func reloadCollectionView()
    {
        collectionView.register(AdsCollectionViewCell.self, forCellWithReuseIdentifier: adsCellIndentifier)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
        let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout // casting is required because UICollectionViewLayout doesn't offer header pin. Its feature of UICollectionViewFlowLayout
        if #available(iOS 9.0, *) {
            layout?.sectionHeadersPinToVisibleBounds = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
            return 2
        }
        else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var headerCell: ProfileCollectionReusableView?
        if isProfile{
            switch kind {
            case UICollectionElementKindSectionHeader:
                headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "profileCollectionReusableViewdentifier",
                                                                             for: indexPath) as? ProfileCollectionReusableView
                if isProfile {
                    headerCell?.usernameLabel.text = self.userDefaults.object(forKey: Common.USER_NAME) as! String?
                    headerCell?.myFunnyCollection.text = NSLocalizedString("my_funny_collection", comment: "")
                    if self.userDefaults.object(forKey: Common.USER_AVATAR) == nil {
                        headerCell?.userAvatar.image = UIImage(named:"use_default_icon@1x")
                    } else {
                        headerCell?.userAvatar.kf.setImage(with:ImageResource(downloadURL: self.url,cacheKey: nil))
                    }
                    headerCell?.reloadData()
                }
                return headerCell!
            case UICollectionElementKindSectionFooter:
                return UICollectionReusableView()
            default:
                assert(false, "Unexpected element kind")
                break
            }
            return UICollectionReusableView()
        } else {
            return UICollectionReusableView()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if isProfile{
            if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
                if section == 0{
                    return .zero
                }
                else{
                    return CGSize(width: collectionView.frame.width, height: 65)
                }
            }
            else{
                return CGSize(width: collectionView.frame.width, height: 65)
            }
        } else {
            return .zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // for sub only - start
        if isProfile{
            if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
                if section == 0{
                    return PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count
                }
                else{
                    if currentArray != nil{
                        return currentArray.count
                    }
                    else{
                        return 0
                    }
                }
            }
            else{
                if currentArray != nil{
                    return currentArray.count
                }
                else{
                    return 0
                }
            }
        }
        else{
            if currentArray != nil{
                return currentArray.count
            }
            else{
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isProfile{
            if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
                if indexPath.section == 0{
                    let uploadObj = PostDAO.PostDaoSingleton.sharedInstance.uploadObjs[indexPath.item]
                    let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: uploadCellIndentifier, for: indexPath as IndexPath) as! UploadViewCell
                    cell.imgAvatar.image = uploadObj?.image
                    cell.imgAvatar.layer.masksToBounds = true
                    cell.imgAvatar.layer.cornerRadius = 3
                    return cell
                }
                else{
                    let postResponse = currentArray[indexPath.item] as! PostResponse
                    if postResponse.isAds == false{
                        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: timelineCellIndentifier, for: indexPath as IndexPath) as! TimeLineCollectionViewCell
                        cellForCollectionView(collectionViewCell: cell, cellForItemAt: indexPath.item)
                        return cell
                    }
                    else{
                        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: adsCellIndentifier, for: indexPath as IndexPath) as! AdsCollectionViewCell
                        loadAdsCell(cell: cell)
                        return cell

                    }
                }
            }
            else{
                let postResponse = currentArray[indexPath.item] as! PostResponse
                if postResponse.isAds == false{
                    let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: timelineCellIndentifier, for: indexPath as IndexPath) as! TimeLineCollectionViewCell
                    cellForCollectionView(collectionViewCell: cell, cellForItemAt: indexPath.item)
                    return cell
                }
                else{
                    let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: adsCellIndentifier, for: indexPath as IndexPath) as! AdsCollectionViewCell
                    loadAdsCell(cell: cell)
                    return cell

                }
            }
        }
        else{
            let postResponse = currentArray[indexPath.item] as! PostResponse
            if postResponse.isAds == false{
                let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: timelineCellIndentifier, for: indexPath as IndexPath) as! TimeLineCollectionViewCell
                cellForCollectionView(collectionViewCell: cell, cellForItemAt: indexPath.item)
                return cell
            }else{
                let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: adsCellIndentifier, for: indexPath as IndexPath) as! AdsCollectionViewCell
                loadAdsCell(cell: cell)
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = getCellAtIndexPath(index: indexPath.item)
        self.cellDidEndDisplay(collectionViewCell: cell, cellForItemAt: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = getCellAtIndexPath(index: indexPath.item)
        didSelectItemAtIndex(collectionViewCell: cell, cellForItemAt: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if isProfile && PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
            if indexPath.section == 0
            {
                return CGSize(width:screenSize.width , height: 80.0)
            }
            else{
                let postResponse = currentArray[indexPath.item] as! PostResponse
                if postResponse.height_cell == nil {
                    return CGSize(width:screenSize.width , height: callHeighCell(index: indexPath.item))
                }
                else{
                    return CGSize(width:screenSize.width , height: CGFloat(postResponse.height_cell!))
                }
                
            }
        }
        else{
            let postResponse = currentArray[indexPath.item] as! PostResponse
            if postResponse.height_cell == nil {
                return CGSize(width:screenSize.width , height: callHeighCell(index: indexPath.item))
            }
            else{
                return CGSize(width:screenSize.width , height: CGFloat(postResponse.height_cell!))
            }
        }
    }
    
    func loadAdsCell(cell: AdsCollectionViewCell)
    {
        
    }
    
    func getCellAtIndexPath(index : Int) -> TimeLineCollectionViewCell?
    {
        var section = 0
        if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0{
            section = 1
        }
        let indexPath = IndexPath(item: index, section: section)
        let cell =  collectionView.cellForItem(at: indexPath)
        if cell == nil {return TimeLineCollectionViewCell()}
        return cell as? TimeLineCollectionViewCell
    }
    
    func callHeighCell(index: Int)-> CGFloat
    {
        let postResponse = currentArray[index] as! PostResponse
        
        if postResponse.isAds == false{
            if postResponse.height_cell != nil {
                return CGFloat(postResponse.height_cell!)
            }else{
                var height: CGFloat  = 0.0
                if isProfile{
                    height = 25.0
                }
                
                let label =  UILabel(frame: CGRect(x: 0, y: 0, width: screenSize.width - 16 , height: .greatestFiniteMagnitude))
                label.text = postResponse.post_title
                
                //cal height label
                height += heightForView(label: label)
                
                if postResponse.safe == 1 && settingControllers.isShowSensitiveContent == false{
                    height += screenSize.width
                }
                else if postResponse.is_long == 1{
                    height += screenSize.width
                }
                else{
                    
                    if postResponse.api_width < Double(screenSize.width)
                    {
                        height += CGFloat(postResponse.api_height)
                    }
                    else{
                        let ratio =  CGFloat(postResponse.api_width) / screenSize.width
                        if ratio > 1.0{
                            height += CGFloat(postResponse.api_height) / ratio
                        }
                        else{
                            height += CGFloat(postResponse.api_height)
                        }
                    }
                }
                //height rest part
                height += 125.0
                
                postResponse.height_cell = Double(height)
                
                return height
            }
        }
        else{
            return 300
        }
    }
    
    func heightForView(label : UILabel) -> CGFloat{
        
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: 17)
        label.sizeToFit()
        return label.frame.height
    }
    
    func setAvatarImg() -> URL{
        let urlString = userDefaults.object(forKey: Common.USER_AVATAR) as! String
        let url = URL(string: urlString)
        return url!
    }
    
    
}
