//
//  BasePageViewController.swift
//  gag
//
//  Created by buisinam on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import HidingNavigationBar

class BasePageViewController: BaseViewController {

    var pageViewController: UIPageViewController!
    
    //Include Segment + Page ViewController
    
    let cornerRadius:CGFloat = 3.0
    
    //subview should implement
    func segmentSelection(selection : Int) -> Void{}
    
    @nonobjc func cellForCollectionView(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int){}
    
    // properties
    var cellWidth = 0.0
    var segmentHeigh = 45.0
    var minWidth = 30.0
    
    open var arrTitleSegment:[String]!
    
    // Segment
    var isSeagmenVisible:Bool = true
    let homeApperence = SMSegmentAppearance()
    var homeSegment :SMSegmentView!
    var collectionView: UICollectionView!
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func removeHeader() {
        homeSegment.removeFromSuperview()
    }
    
    func setUpdateSegmentView(isEqual : Bool)
    {
        
        if (isSeagmenVisible) {
            homeApperence.segmentOnSelectionColour = UIColor.clear
            homeApperence.titleOnSelectionColour = UIColor.black
            homeApperence.titleOffSelectionColour = UIColor.gray
            homeApperence.segmentOffSelectionColour = UIColor.clear
            homeApperence.titleOnSelectionFont = UIFont.systemFont(ofSize: 13.0)
            homeApperence.titleOffSelectionFont = UIFont.systemFont(ofSize: 13.0)
            homeApperence.contentVerticalMargin = 10.0
            
            homeSegment = SMSegmentView(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! - CGFloat(segmentHeigh), width: (self.navigationController?.navigationBar.frame.size.width)!, height: CGFloat(segmentHeigh)), dividerColour: UIColor(white: 0.95, alpha: 0.3), dividerWidth: 0.0, segmentAppearance: homeApperence)
            homeSegment.equalAll = isEqual
            homeSegment.backgroundColor = .clear
            for title in arrTitleSegment{
                homeSegment.addSegmentWithTitle(title, onSelectionImage: nil, offSelectionImage: nil)
            }
            
            homeSegment.selectedSegmentIndex = 0
            homeSegment.addTarget(self, action: #selector(homeSegmentAction(sender:)), for: .valueChanged)
            self.navigationController?.navigationBar.addSubview(self.homeSegment)
        }
    }
    
    func setUpdateSegmentView(isEqual : Bool,padding: Float)
    {
        
        if (isSeagmenVisible) {
            homeApperence.segmentOnSelectionColour = UIColor.clear
            homeApperence.titleOnSelectionColour = Common.blueColor()
            homeApperence.titleOffSelectionColour = UIColor.gray
            homeApperence.segmentOffSelectionColour = UIColor.clear
            homeApperence.titleOnSelectionFont = UIFont.boldSystemFont(ofSize: 13.0)
            homeApperence.titleOffSelectionFont = UIFont.systemFont(ofSize: 13.0)
            homeApperence.contentVerticalMargin = 10.0
            
            homeSegment = SMSegmentView(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! - CGFloat(segmentHeigh), width: (self.navigationController?.navigationBar.frame.size.width)!, height: CGFloat(segmentHeigh)), dividerColour: .clear, dividerWidth: CGFloat(padding), segmentAppearance: homeApperence)
            homeSegment.equalAll = isEqual
            homeSegment.backgroundColor = .clear
            for title in arrTitleSegment{
                homeSegment.addSegmentWithTitle(title, onSelectionImage: nil, offSelectionImage: nil)
            }
            
            homeSegment.selectedSegmentIndex = selectedIndex
            homeSegment.addTarget(self, action: #selector(homeSegmentAction(sender:)), for: .valueChanged)
            self.navigationController?.navigationBar.addSubview(self.homeSegment)
        }
    }

    
    func setUpdateSegmentExtendView(isEqual : Bool) -> UIView
    {
        if (isSeagmenVisible) {
            homeApperence.segmentOnSelectionColour = UIColor.white
            homeApperence.titleOnSelectionFont = UIFont.boldSystemFont(ofSize: 17.0)
            homeApperence.titleOnSelectionColour = UIColor.black
            homeApperence.titleOffSelectionColour = UIColor.gray
            homeApperence.segmentOffSelectionColour = UIColor.white
            homeApperence.titleOnSelectionFont = UIFont.systemFont(ofSize: 12.0)
            homeApperence.titleOffSelectionFont = UIFont.systemFont(ofSize: 12.0)
            homeApperence.contentVerticalMargin = 10.0
            
            homeSegment = SMSegmentView(frame: CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.size.width)!, height: CGFloat(segmentHeigh)), dividerColour: UIColor(white: 0.95, alpha: 0.3), dividerWidth: 0.0, segmentAppearance: homeApperence)
            
            homeSegment.equalAll = isEqual
            
            for title in arrTitleSegment{
                homeSegment.addSegmentWithTitle(title, onSelectionImage: nil, offSelectionImage: nil)
            }
            
            homeSegment.selectedSegmentIndex = selectedIndex
            homeSegment.addTarget(self, action: #selector(homeSegmentAction(sender:)), for: .valueChanged)
            
            
            let extensionView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: CGFloat(segmentHeigh)))
            
            extensionView.backgroundColor = UIColor.white
            extensionView.addSubview(homeSegment)
            
            return extensionView
        }
        return UIView()
    }
    
    func setSelectedSegment(index: Int)
    {
        homeSegment.selectedSegmentIndex = index
    }
    
    //MARK: SegmentView setup
    func homeSegmentAction(sender: SMSegmentView) -> Void {
        segmentSelection(selection: sender.selectedSegmentIndex)
    }
    
    

}
