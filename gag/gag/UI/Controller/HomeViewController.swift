//
//  FirstViewController.swift
//  gag
//
//  Created by buisinam on 10/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//
// 016.7809.9179

import UIKit
import LGSemiModalNavController
import MEVFloatingButton
import Kingfisher
import Social
import MobileCoreServices
import TUSafariActivity
import HidingNavigationBar
import Photos

class HomeViewController: UITimelineViewController,GalleryItemsDataSource, GalleryDisplacedViewsDataSource, ActionSheetAddButtonTimeLineDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MEVFloatingButtonDelegate{
    
    @IBOutlet weak var timeLineCollectionView: UICollectionView!
    var hidingNavBarManager: HidingNavigationBarManager?
    open var pageIndex = 0
    
    var isExplore = false
    
    var isSearch = false
    
    var searchData: String = ""
    
    var category_id: Int = 0
    
    let alert = UIAlertView()
    
    var tapGestureRecognizer = UITapGestureRecognizer()
    // add button
    let addButton = MEVFloatingButton()
    // Actionsheet
    var actionSheetView:ActionSheetAddButtonTimeLine! = nil
    var actionController = LGSemiModalNavViewController()
    // Photo framework
    
    let defaults = UserDefaults.standard
    var isAutoPlayGif : Bool?
    
    //    var userDefaults:String =  UserDefaults.standard.string(forKey: "user_id")!
    var postID = "0"
    
    var since = ""
    var last = ""
    
    open var hasUpdate: Bool! = false
    
    let newPost = UIButton()
    
    open let newViewPost = UIView()
    
    var placeImage : UIImageView!
    
    var placeViewSafe : UIView!
    
    var galleryViewController:GalleryViewController!
    
    var isRefresh = false
    
    let nbsToolbar  = UINib(nibName: "NBSToolBarBlur", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NBSToolBarBlur
    
    let nbsNav = UINib(nibName: "NBSNavBlur", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NBSNavBlur
    
    let refreshControl = UIRefreshControl()
    
    let settingController = SettingControllerSingleton.sharedInstance
    
    //query new post
    var timer: Timer!
    let timerQuery = 10.0
    
    var parentVC: HomePageViewController!
    
    var currentFullScreenIndex: Int = 0
    let isAuthorizePhoto = PHPhotoLibrary.authorizationStatus()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView = timeLineCollectionView
        
        self.reloadCollectionView()
        
        //newPostButton
        let screenSize: CGRect = UIScreen.main.bounds
        newPost.setTitle("↑ New Posts", for: .normal)
        newPost.setTitleColor(UIColor.white, for: .normal)
        newPost.backgroundColor = UIColor.blue
        newPost.frame = CGRect(x:(screenSize.width - 100) / 2,y:5,width:100,height:30)
        newPost.backgroundColor = UIColor(colorLiteralRed: 0, green: 153.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        newPost.addTarget(self, action: #selector(refresh(sender:)), for: .touchUpInside)
        newPost.layer.cornerRadius = 15
        newPost.layer.masksToBounds = true
        newPost.titleLabel?.textAlignment = NSTextAlignment.center
        newPost.setTitleColor(UIColor.white, for: UIControlState.normal)
        newPost.titleLabel?.font = UIFont.systemFont(ofSize: 12.5)
        newPost.layer.borderWidth = 0.0
        newPost.layer.borderColor = UIColor.darkGray.cgColor
        
        newViewPost.frame  = CGRect(x: 0, y: 150, width: screenSize.width, height: 38)
        newViewPost.backgroundColor = UIColor.clear
        newViewPost.addSubview(self.newPost)
        
        
        
        // add button
        if !isProfile {
            self.setUpAddButton()
        }
        
        self.setUpToolBar()
        
        if isExplore {
            queryPostExplore()
        } else if isSearch {
            queryPostIsSearch()
        }
        else {
            queryPost()
        }
        
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        self.collectionView.addSubview(refreshControl)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setIsAutoPlayGif(notification:)), name: NSNotification.Name("NotificationSetIsAutoPlayGif"), object: nil)
        if !isProfile && !isExplore && !isSearch{
            queryNewPost()
        }
        //        let maintab = parentVC?.tabBarController as! MainTabbarController
        if (userDefaults.object(forKey: Common.NO_NETWORK_CONNECTION) != nil) {
            //            maintab.mainCheckNetwork()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Check login post flow
//        if self.defaults.bool(forKey: Common.NOT_LOGIN_POST) {
//            if(isAuthorizePhoto == PHAuthorizationStatus.authorized) {
//            actionSheetView = ActionSheetAddButtonTimeLine()
//            self.defaults.set(false, forKey: Common.NOT_LOGIN_POST)
//            self.actionController.view.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: 224)
//            self.actionController.navigationController?.setNavigationBarHidden(true, animated: true)
//            self.actionController.animationSpeed = 0.35
//            self.actionController.backgroundShadeColor = UIColor.black
//            self.actionController.isTapDismissEnabled = true
//            self.actionSheetView.delegateButton = self
//            self.actionController.springDamping = 0
//            self.actionController.springVelocity = 0
//            self.present(self.actionController, animated: true, completion: nil)
//            }
//        }
    }
    
    override func reloadHandelPost() {
        if self.isProfile {
            self.refresh(sender: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timeLineCollectionView.reloadData()
        setUpButtonBar()
    }
    
    @objc func setIsAutoPlayGif(notification: Notification ){
        isAutoPlayGif = defaults.bool(forKey: "AutoPlayGIFPost")
        print("setIsAutoPlayGif = \(isAutoPlayGif)")
    }
    
    
    
    override func viewDidLayoutSubviews()
    {
        
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        newViewPost.removeFromSuperview()
        super.viewWillDisappear(animated)
    }
    
    func queryNewPost()
    {
        timer = Timer.scheduledTimer(timeInterval: timerQuery,
                                     target: self,
                                     selector: #selector(getNewsPost),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    func getNewsPost()
    {
        if queryStatus == .Finished{
            if self.currentArray.count > 0
            {
                let data  = self.currentArray[0] as! PostResponse
                since = ""
                last = data.post_date
            }
            else{
                return
            }
            
            queryStatus = .Loading
            timer.invalidate()
            
            var type = ""
            switch pageIndex {
            case 0:
                type = "hot"
                break
            case 1:
                type = "trend"
                break
            case 2:
                type = "fresh"
                break
            default:
                type = "hot"
            }
            
            PostDAO.getPost(display: "Home", type: type, since: since, last: last) { (success: Bool, data) in
                if success {
                    if self.queryStatus == .Loading{
                        if (data?.count)! > 0 && self.parentVC.currentIndex == self.pageIndex{
                            
                            DispatchQueue.main.async {
                                self.hasUpdate = true
                                self.hidingNavBarManager?.expand()
                                self.hidingNavBarManager?.addExtensionView(self.newViewPost)
                                
                            }
                            
                        }
                    }
                }
                self.queryStatus = .Finished
                self.queryNewPost()
            }
        }
        else{
            timer.invalidate()
        }
    }
    
    func refresh(sender:AnyObject)
    {
        newViewPost.removeFromSuperview()
        if loadingStatus == .Finished{
            self.isRefresh = true
            hasUpdate = false;
            queryStatus = .Loading
            loadingStatus = .Loading
            since = ""
            if self.currentArray != nil && self.currentArray.count > 0{
                let lastData = self.currentArray[0] as! PostResponse
                last = lastData.post_date
            }
            else{
                last = ""
            }
            self.refreshControl.beginRefreshing()
            UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                self.collectionView.contentOffset = CGPoint(x: 0, y: -200)
                
            }, completion: { (complete) in
                if complete == true{
                    if !self.isExplore{
                        self.queryPost()
                    }
                    else if self.isExplore{
                        self.queryPostExplore()
                    }
                    else if self.isSearch{
                        self.queryPostIsSearch()
                    }
                }
            })
        }
        
        //        }
        // your refreshing logic here...
        
    }
    
    func queryPost()
    {
        if self.currentArray == nil {
            self.currentArray = []
        }
        var display = "home"
        var type = ""
        
        if isProfile{
            display = "category"
            switch pageIndex {
            case 0:
                type = "all"
                break
            case 1:
                type = "posts"
                break
            case 2:
                type = "likes"
                break
            case 3:
                type = "comments"
                break
            default:
                type = "hot"
            }
            let user_id = BaseDAO.getUserId()
            let author_id = user_id
            PostDAO.getPostProfile(author_id: "\(author_id)", user_id: "\(user_id)", type: type, since: since, last: last) { (success: Bool, data) in
                if success {
                    if self.loadingStatus == .Loading{
                        
                        var tag = self.currentArray.count
                        var rowUpdate = [IndexPath]()
                        if !self.isRefresh {
                            for i in 0..<data!.count{
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0  && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: tag, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: tag, section: 0))
                                }
                                self.currentArray.append((data?[i])!)
                                tag += 1
                            }
                            self.collectionView.insertItems(at: rowUpdate)
                            if self.galleryViewController != nil{
                                self.galleryViewController.reloadData(newItemsCount: self.currentArray.count)
                            }
                        } else {
                            data?.reversed().forEach({ (item) in
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: 0, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: 0, section: 0))
                                }
                                self.currentArray.insert(item, at: 0)
                            })
                            self.collectionView.reloadData()
                        }
                        self.isRefresh = false
                    }else{
                        self.currentArray = data
                        self.reloadCollectionView()
                    }
                }
                self.loadingStatus = .Finished
                self.refreshControl.endRefreshing()
            }
        }
        else{
            
            switch pageIndex {
            case 0:
                type = "hot"
                break
            case 1:
                type = "trend"
                break
            case 2:
                type = "fresh"
                break
            default:
                type = "hot"
            }
            PostDAO.getPost(display: display, type: type, since: since, last: last) { (success: Bool, data) in
                if success {
                    if self.loadingStatus == .Loading{
                        
                        var tag = self.currentArray.count
                        var rowUpdate = [IndexPath]()
                        
                        if !self.isRefresh {
                            for i in 0..<data!.count{
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: tag, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: tag, section: 0))
                                }
                                self.currentArray.append((data?[i])!)
                                tag += 1
                            }
                            if self.appDelegate.isAds == true{
                                if self.currentArray.count > 0{
                                    let response = self.currentArray[self.currentArray.count - 1] as! PostResponse
                                    if !response.isAds{
                                        // add Ads
                                        let ads = PostResponse()
                                        ads.isAds = true
                                        self.currentArray.append(ads)
                                        if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                            rowUpdate.append(IndexPath(row: tag, section: 1))
                                        }
                                        else{
                                            rowUpdate.append(IndexPath(row: tag, section: 0))
                                        }
                                    }
                                }
                            }
                            // End add cell
                            self.collectionView.insertItems(at: rowUpdate)
                            if self.galleryViewController != nil{
                                self.galleryViewController.reloadData(newItemsCount: self.currentArray.count)
                            }
                        } else {
                            data?.reversed().forEach({ (item) in
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: 0, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: 0, section: 0))
                                }
                                self.currentArray.insert(item, at: 0)
                            })
                            self.collectionView.reloadData()
                        }
                        self.isRefresh = false
                    }else{
                        self.currentArray = data
                        if self.appDelegate.isAds == true{
                            // add Ads
                            let ads = PostResponse()
                            ads.isAds = true
                            self.currentArray.append(ads)
                        }
                        self.reloadCollectionView()
                    }
                }
                self.loadingStatus = .Finished
                self.queryStatus = .Finished
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func queryPostExplore() {
        
        let display = "category"
        
        var type = ""
        switch pageIndex {
        case 0:
            type = "hot"
            break
        case 1:
            type = "fresh"
            break
        default:
            type = "hot"
        }
        
        if category_id >= 0{
            
            PostDAO.getPostExplore(display: display, type: type, category_id: category_id, since: since, last: last) { (success: Bool, data) in
                if success {
                    if self.loadingStatus == .Loading{
                        
                        var tag = self.currentArray.count
                        var rowUpdate = [IndexPath]()
                        
                        if !self.isRefresh {
                            for i in 0..<data!.count{
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: tag, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: tag, section: 0))
                                }
                                self.currentArray.append((data?[i])!)
                                tag += 1
                            }
                            self.collectionView.insertItems(at: rowUpdate)
                            if self.galleryViewController != nil{
                                self.galleryViewController.reloadData(newItemsCount: self.currentArray.count)
                            }
                        } else {
                            data?.reversed().forEach({ (item) in
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: 0, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: 0, section: 0))
                                }
                                self.currentArray.insert(item, at: 0)
                            })
                            self.collectionView.reloadData()
                        }
                        self.isRefresh = false
                    }else{
                        self.currentArray = data
                        self.reloadCollectionView()
                    }
                }
                self.loadingStatus = .Finished
                self.queryStatus = .Finished
                self.refreshControl.endRefreshing()
                
            }
        }
        else{
            var display = ""
            
            switch category_id {
            case -1:
                display = "gif"
                break
            case -2:
                display = "nsfw"
                break
            default:
                display = "nsfw"
            }
            PostDAO.getPost(display: display, type: type, since: since, last: last) { (success: Bool, data) in
                if success {
                    if self.loadingStatus == .Loading{
                        
                        var tag = self.currentArray.count
                        var rowUpdate = [IndexPath]()
                        
                        if !self.isRefresh {
                            for i in 0..<data!.count{
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: tag, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: tag, section: 0))
                                }
                                self.currentArray.append((data?[i])!)
                                tag += 1
                            }
                            self.collectionView.insertItems(at: rowUpdate)
                            if self.galleryViewController != nil{
                                self.galleryViewController.reloadData(newItemsCount: self.currentArray.count)
                            }
                        } else {
                            data?.reversed().forEach({ (item) in
                                if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                    rowUpdate.append(IndexPath(row: 0, section: 1))
                                }
                                else{
                                    rowUpdate.append(IndexPath(row: 0, section: 0))
                                }
                                self.currentArray.insert(item, at: 0)
                            })
                            self.collectionView.reloadData()
                        }
                        self.isRefresh = false
                    }else{
                        self.currentArray = data
                        self.reloadCollectionView()
                    }
                }
                self.loadingStatus = .Finished
                self.queryStatus = .Finished
                self.refreshControl.endRefreshing()
            }
        }
        
        
    }
    
    func queryPostIsSearch() {
        ExploreDAO.getListSearchExplore(since: since, last: last, search_value: searchData) {(success: Bool, data) in
            if success {
                if self.loadingStatus == .Loading{
                    
                    var tag = self.currentArray.count
                    var rowUpdate = [IndexPath]()
                    
                    if !self.isRefresh {
                        for i in 0..<data!.count{
                            if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                rowUpdate.append(IndexPath(row: tag, section: 1))
                            }
                            else{
                                rowUpdate.append(IndexPath(row: tag, section: 0))
                            }
                            self.currentArray.append((data?[i])!)
                            tag += 1
                        }
                        self.collectionView.insertItems(at: rowUpdate)
                        if self.galleryViewController != nil{
                            self.galleryViewController.reloadData(newItemsCount: self.currentArray.count)
                        }
                    } else {
                        data?.reversed().forEach({ (item) in
                            if PostDAO.PostDaoSingleton.sharedInstance.uploadObjs.count > 0 && self.isProfile == true{
                                rowUpdate.append(IndexPath(row: 0, section: 1))
                            }
                            else{
                                rowUpdate.append(IndexPath(row: 0, section: 0))
                            }
                            self.currentArray.insert(item, at: 0)
                        })
                        self.collectionView.reloadData()
                    }
                    self.isRefresh = false
                }else{
                    self.currentArray = data
                    self.reloadCollectionView()
                }
            }
            self.loadingStatus = .Finished
            self.refreshControl.endRefreshing()
            
        }
    }
    
    //MARK: - UICollectionView Delegate
    
    override func cellForCollectionView(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int) {
        
        if(indexPath >= self.currentArray.count - 4){
            if loadingStatus == .Finished {
                loadingStatus = .Loading
                var number = 1
                if appDelegate.isAds == true{
                    number = 2
                }
                let lastData = self.currentArray[self.currentArray.count - number] as! PostResponse
                since = lastData.post_date
                last = ""
                
                if !self.isExplore{
                    self.perform(#selector(queryPost))
                }
                else if self.isExplore{
                    self.perform(#selector(queryPostExplore))
                }
                else if self.isSearch{
                    self.perform(#selector(queryPostIsSearch))
                }
            }
        }
        
        let postResponse = currentArray[indexPath] as! PostResponse
        
        let cell  = collectionViewCell as! TimeLineCollectionViewCell
        if !isProfile{
            cell.profileActionStatus.text = ""
        } else {
            cell.profileActionStatus.text = NSLocalizedString("your_post", comment: "")
        }
        cell.postLikeButton.layer.cornerRadius = self.cornerRadius
        cell.postLikeButton.layer.borderWidth = 0.5
        cell.postLikeButton.layer.borderColor = UIColor.lightGray.cgColor
        cell.postUnlikeButton.layer.cornerRadius = self.cornerRadius
        cell.postUnlikeButton.layer.borderWidth = 0.5
        cell.postUnlikeButton.layer.borderColor = UIColor.lightGray.cgColor
        cell.postCommentButton.layer.cornerRadius = self.cornerRadius
        cell.postCommentButton.layer.borderWidth = 0.5
        cell.postCommentButton.layer.borderColor = UIColor.lightGray.cgColor
        cell.postShareButton.layer.cornerRadius = self.cornerRadius
        cell.postShareButton.setTitle(NSLocalizedString("share", comment: ""), for: .normal)
        cell.postImage.tag = indexPath
        cell.viewSafe.tag = indexPath
        cell.btnGif.tag = indexPath
        cell.postLikeButton.tag = indexPath
        cell.postUnlikeButton.tag = indexPath
        cell.postCommentButton.tag = indexPath
        cell.postShareButton.tag = indexPath
        cell.postTitle.tag = indexPath
        cell.postMoreButton.tag = indexPath
        cell.btnGif.addTarget(self, action: #selector(playGif(sender:)), for: .touchUpInside)
        cell.postLikeButton.addTarget(self, action: #selector(voteNormalAction(sender:)), for: .touchUpInside)
        cell.postUnlikeButton.addTarget(self, action: #selector(voteUnNormalAction(sender:)), for: .touchUpInside)
        cell.postCommentButton.addTarget(self, action: #selector(commentNormalAction(sender:)), for: .touchUpInside)
        cell.postShareButton.addTarget(self, action: #selector(shareActionNormal(sender:)), for: .touchUpInside)
        cell.postMoreButton.addTarget(self, action: #selector(postMoreAction(sender:)), for: .touchUpInside)
        tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tappedOnPostImage(sender:)))
        tapGestureRecognizer.numberOfTapsRequired  = 1
        cell.postImage.isUserInteractionEnabled = true
        cell.postImage.addGestureRecognizer(tapGestureRecognizer)
        
        tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tappedOnPostViewSafe(sender:)))
        tapGestureRecognizer.numberOfTapsRequired  = 1
        cell.viewSafe.isUserInteractionEnabled = true
        cell.viewSafe.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tappedOnPostTitle(sender:)))
        tapGestureRecognizer.numberOfTapsRequired  = 1
        cell.postTitle.isUserInteractionEnabled = true
        cell.postTitle.addGestureRecognizer(tapGestureRecognizer)
        // for sub only - start
        let postTitleString = String(format: "%@", postResponse.post_title)
        cell.postTitle.text = postTitleString
        cell.viewFullPost.text = NSLocalizedString("view_full_post", comment: "")
        // post_link
        let postLink = String(format: "%@", postResponse.post_link)
        cell.postLink = postLink
        cell.postPoints.text =  "\(postResponse.post_point)" + NSLocalizedString("point", comment: "") + postResponse.post_comment + NSLocalizedString("comment", comment: "")
        cell.postUnlikeButton.tintColor = Common.lighGrayColor()
        cell.postCommentButton.tintColor = Common.lighGrayColor()
        
        // btnVote
        if postResponse.is_vote == 0{
            cell.postLikeButton.tintColor = Common.lighGrayColor()
            cell.postUnlikeButton.tintColor = Common.lighGrayColor()
        }else if postResponse.is_vote == -1{
            cell.postUnlikeButton.tintColor = Common.blueColor()
            cell.postLikeButton.tintColor = Common.lighGrayColor()
        }else {
            cell.postLikeButton.tintColor = Common.blueColor()
            cell.postUnlikeButton.tintColor = Common.lighGrayColor()
        }
        if postResponse.safe == 0 || settingController.isShowSensitiveContent == true{
            cell.viewSafe.isHidden = true
            // type
            if postResponse.post_type == PostType.Video.rawValue{
                cell.viewGif.isHidden = false
                cell.btnGif.isHidden = false
                cell.longView.isHidden = true
                if cell.viewGif.url == nil || (postResponse.url != cell.viewGif.stringURL){
                    cell.viewGif.url = NSURL(string: postResponse.url)!
                    cell.viewGif.stringURL = postResponse.url
                }
                cell.contentView.bringSubview(toFront: cell.viewGif)
                cell.contentView.bringSubview(toFront: cell.btnGif)
                cell.btnGif.setImage(UIImage(named: "gif"), for: .normal)
                if settingControllers.isAutoPlayGIFPost == true {
                    cell.btnGif.setImage(nil, for: UIControlState.normal)
                    cell.viewGif.PlayVideo();
                }
            }
            else if postResponse.is_long == 0{
                cell.viewGif.isHidden = true
                cell.btnGif.isHidden = true
                cell.longView.isHidden = true
                cell.postImage.kf.setImage(with: ImageResource(downloadURL: URL(string: postResponse.url)!,cacheKey: nil))
                cell.contentView.bringSubview(toFront: cell.postImage)
            }
            else{
                cell.viewGif.isHidden = true
                cell.btnGif.isHidden = true
                cell.longView.isHidden = false
                cell.postImage.kf.setImage(with: ImageResource(downloadURL: URL(string: postResponse.url)!,cacheKey: nil))
                cell.contentView.bringSubview(toFront: cell.postImage)
                cell.contentView.bringSubview(toFront: cell.longView)
            }
        }
        else{
            cell.viewSafe.isHidden = false
            cell.contentView.bringSubview(toFront: cell.viewSafe)
        }
    }
    
    override func cellDidEndDisplay(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int) {
        
    }
    
    override func didSelectItemAtIndex(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int) {
        let postResponse = currentArray[indexPath] as! PostResponse
        if !postResponse.isAds{
            let cell  = collectionViewCell as! TimeLineCollectionViewCell
            cell.viewGif.destroy()
        }
    }
    
    
    
    func postImageAction(img: AnyObject) {
        alert.title = "GAG"
        alert.message = "Tap on image"
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
    func showReportPost(cellIndex: Int) {
        let alertReportPost = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertReportPost.addAction(UIAlertAction(title: NSLocalizedString("copy_violation", comment: ""), style: .default, handler: {(UIAlertAction)in
            self.postCopyViolation(cellIndex: cellIndex)
            print("press Violation")
        }))
        alertReportPost.addAction(UIAlertAction(title: NSLocalizedString("spam", comment: ""), style: .default, handler: {(UIAlertAction)in
            self.postSpam(cellIndex: cellIndex)
            print("press Spam")
        }))
        alertReportPost.addAction(UIAlertAction(title: NSLocalizedString("offensive_materials", comment: ""), style: .default, handler: {(UIAlertAction)in
            self.postOffensiveMaterial(cellIndex: cellIndex)
            print("press Materials")
        }))
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (action) in
        }
        alertReportPost.addAction(cancelAction)
        if galleryViewController != nil {
            galleryViewController.present(alertReportPost, animated: true, completion: nil)
        } else {
            self.present(alertReportPost, animated: true, completion: nil)
        }
    }
    
    func postCopyViolation(cellIndex: Int) {
        Common.showHubSuccess(string: NSLocalizedString("post_reported", comment: ""))
        // remove cell at index
        //        let indexPath = NSIndexPath(row: cellIndex, section: 0)
        //        self.collectionView.deleteItems(at: [indexPath as IndexPath])
        //        self.collectionView.reloadData()
    }
    
    func postSpam(cellIndex: Int) {
        Common.showHubSuccess(string: NSLocalizedString("post_reported", comment: ""))
    }
    
    func postOffensiveMaterial(cellIndex: Int) {
        Common.showHubSuccess(string: NSLocalizedString("post_reported", comment: ""))
    }
    
    func postMoreAction(sender: Any) {
        // get cell index
        let cell  = self.getCellAtIndexPath(index: (sender as AnyObject).tag)
        let alertMoreAction = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertMoreAction.addAction(UIAlertAction(title: NSLocalizedString("report_post", comment: ""), style: .default, handler: {(UIAlertAction)in
            if BaseDAO.checkLogin() {
                self.showReportPost(cellIndex: (sender as AnyObject).tag)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
                vc.isBackgroundBlack = true
                let nav = UINavigationController(rootViewController: vc)
                
                if self.galleryViewController != nil{
                    self.galleryViewController.present(nav, animated: true, completion: nil)
                } else {
                    self.navigationController?.present(nav, animated: true, completion: nil)
                }
            }
            print("press suggest")
        }))
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (action) in
        }
        alertMoreAction.addAction(cancelAction)
        if galleryViewController != nil {
            galleryViewController.present(alertMoreAction, animated: true, completion: nil)
        } else {
            self.present(alertMoreAction, animated: true, completion: nil)
        }
    }
    func tappedOnPostImage(sender: UITapGestureRecognizer) {
        placeImage = sender.view as! UIImageView
        
        guard (sender.view as? UIImageView) != nil else { return }
        let displacedViewIndex = placeImage.tag
        showFullScreen(displacedViewIndex: displacedViewIndex)
    }
    
    func tappedOnPostViewSafe(sender: UITapGestureRecognizer) {
        placeViewSafe = sender.view
        
        guard (sender.view) != nil else { return }
        let displacedViewIndex = placeViewSafe.tag
        showFullScreen(displacedViewIndex: displacedViewIndex)
    }
    
    func showFullScreen(displacedViewIndex: Int)
    {
        currentFullScreenIndex = displacedViewIndex
        galleryViewController = GalleryViewController(startIndex: displacedViewIndex, itemsDataSource: self, itemsDelegate: nil, displacedViewsDataSource: self, configuration: galleryConfiguration())
        
        galleryViewController.headerView = nbsNav
        galleryViewController.footerView = nbsToolbar
        galleryViewController.launchedCompletion = { self.reloadNavBarData(index: displacedViewIndex) }
        galleryViewController.closedCompletion = { UIApplication.shared.statusBarStyle = .default
            if self.isProfile{
                self.appDelegate.setStatusBarBackgroundColor(color: .white)
            }
            print("index: " + "\(displacedViewIndex)")
        }
        galleryViewController.closedCompletion = {
            print("index: " + "\(displacedViewIndex)")
        }
        
        galleryViewController.swipedToDismissCompletion = { UIApplication.shared.statusBarStyle = .default

            self.appDelegate.setStatusBarBackgroundColor(color: .white)
            print("index: " + "\(displacedViewIndex)")
            if !self.isProfile{
                self.collectionView.scrollToItem(at: IndexPath(item: self.currentFullScreenIndex, section: 0), at: .centeredVertically, animated: true)
            }
            self.galleryViewController = nil
        }
        galleryViewController.landedPageAtIndexCompletion = { index in
            self.currentFullScreenIndex = index
            self.reloadNavBarData(index: index)
        }
        
        self.presentImageGallery(galleryViewController)

    }
    
    func tappedOnPostTitle(sender: UITapGestureRecognizer) {
        let indexPath = sender.view!.tag
        let cell  = self.getCellAtIndexPath(index: indexPath)
        let data = currentArray[indexPath] as? PostResponse
        let vc = CommentTableViewController()
        vc.post_id = data?.post_id
        vc.imgAvatar = cell?.postImage.image
        vc.data = data
        vc.isDetail = true
        vc.isPostDetail = true
        self.show(vc, sender: nil)
    }
    
    func shareActionNormal(sender: Any) {
        shareAction(sender: (sender as AnyObject))
    }
    
    func commentNormalAction(sender: Any) {
        commentAction(sender: (sender as AnyObject))
    }
    
    func voteNormalAction(sender: Any)
    {
        if BaseDAO.checkLogin() {
            voteLikeAction(index: (sender as AnyObject).tag)
        } else {
            let join9GagViewController = self.storyboard?.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            join9GagViewController.isBackgroundBlack = true
            join9GagViewController.mainTabbar = self.tabBarController as! MainTabbarController
            let nav = UINavigationController(rootViewController: join9GagViewController)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func voteUnNormalAction(sender: Any)
    {
        if BaseDAO.checkLogin() {
            voteUnLikeAction(index: (sender as AnyObject).tag)
        } else {
            let join9GagViewController = self.storyboard?.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            join9GagViewController.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: join9GagViewController)
            self.navigationController?.present(nav, animated: true, completion: nil)
            //            self.present(join9GagViewController, animated: true, completion: nil)
        }
    }
    
    //voteAction
    func voteLikeAction(index: Int) {
        weak var data = currentArray[index] as? PostResponse
        if data?.is_vote == 0 {
            let cell  = self.getCellAtIndexPath(index: index)
            data?.post_point += 1
            cell?.postLikeButton.tintColor = Common.blueColor()
            cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 1
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        } else if data?.is_vote == 1 {
            let cell  = self.getCellAtIndexPath(index: index)
            data?.post_point -= 1
            cell?.postLikeButton.tintColor = Common.lighGrayColor()
            cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 0
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        } else {
            let cell  = self.getCellAtIndexPath(index: index)
            data?.post_point += 2
            cell?.postLikeButton.tintColor = Common.blueColor()
            cell?.postUnlikeButton.tintColor = Common.lighGrayColor()
            cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 1
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        }
        
    }
    
    func voteUnLikeAction(index: Int) {
        weak var data = currentArray[index] as? PostResponse
        if data?.is_vote == 0  {
            let cell  = self.getCellAtIndexPath(index: index)
            data?.post_point -= 1
            cell?.postUnlikeButton.tintColor = Common.blueColor()
            cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = -1
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        }else if data?.is_vote == -1 {
            let cell  = self.getCellAtIndexPath(index: index)
            data?.post_point += 1
            cell?.postUnlikeButton.tintColor = Common.lighGrayColor()
            cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = 0
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
        }else {
            let cell  = self.getCellAtIndexPath(index: index)
            data?.post_point -= 2
            cell?.postUnlikeButton.tintColor = Common.blueColor()
            cell?.postLikeButton.tintColor = Common.lighGrayColor()
            cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
            data?.is_vote = -1
            PostDAO.userVote(post_id: Int(data!.post_id)!, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    data?.post_point = Int(result!)
                    cell?.postPoints.text =  "\(Int((data?.post_point)!))" + NSLocalizedString("point", comment: "") + (data?.post_comment)! + NSLocalizedString("comment", comment: "")
                }
            }
            
        }
    }
    
    //MARK: - Imageviewer Delegate
    
    func itemCount() -> Int {
        return currentArray.count
    }
    
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        
        return  nil
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let postResponse = currentArray[index] as! PostResponse
        
        if postResponse.isAds == false{
            if postResponse.post_type == PostType.Video.rawValue {
                
                return GalleryItem.video(fetchPreviewImageBlock: { $0(UIImage())} , videoURL: URL(string: postResponse.url)!)
            }
            else {
                let url = URL(string: postResponse.url.replacingOccurrences(of: "_thumb", with: ""))
                
                return GalleryItem.image { callback in
                    KingfisherManager.shared.retrieveImage(with: ImageResource(downloadURL: url!), options: [], progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
                        callback(image)
                    })
                }
                
            }
        }
        else{
            let url = URL(string: "www.google.com")
            return GalleryItem.ads{ callback in
                KingfisherManager.shared.retrieveImage(with: ImageResource(downloadURL: url!), options: [], progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
                    callback(image)
                })
            }
        }
    }
    
    func galleryConfiguration() -> GalleryConfiguration {
        
        return [
            GalleryConfigurationItem.playButtonImage(UIImage(named: "gif")!),
            GalleryConfigurationItem.pagingMode(.standard),
            GalleryConfigurationItem.presentationStyle(.fade),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            GalleryConfigurationItem.maximumZoomScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.easeOut),
            GalleryConfigurationItem.statusBarHidden(false),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(true),
            GalleryConfigurationItem.displacementInsetMargin(0),
            GalleryConfigurationItem.closeButtonMode(.none),
            GalleryConfigurationItem.thumbnailsButtonMode(.none),
            GalleryConfigurationItem.deleteButtonMode(.none)
        ]
    }
    
    //MARK: - Add NewPost
    /* add button */
    func setUpAddButton() {
        //        addButton.animationType = MEVFloatingButtonAnimation.MEVFloatingButtonAnimationFromBottom
        addButton.displayMode = MEVFloatingButtonDisplayMode.whenScrolling
        addButton.position = MEVFloatingButtonPosition.bottomRight
        addButton.image = UIImage(named: "add-button")
        addButton.outlineWidth = 0.0;
        addButton.imagePadding = 30.0
        addButton.horizontalOffset = 13.0;
        addButton.verticalOffset = 13.0;
        addButton.isHideWhenScrollToTop = true
        addButton.shadowColor = UIColor.black
        addButton.shadowRadius = 1
        self.collectionView .setFloatingButton(addButton)
        self.collectionView.floatingButtonDelegate = self
    }
    
    //MARK: - Action Button
    func floatingButton(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        if !Common().checkNetworkConnect() {
            Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
                             message: NSLocalizedString("error_network_message", comment: ""),
                             titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                print("cancel")
            })
        } else {
            if(isAuthorizePhoto == PHAuthorizationStatus.authorized) {
                if BaseDAO.checkLogin() {
                    
                    for cell in self.collectionView.visibleCells  as [UICollectionViewCell]    {
                        if let adsCell:AdsCollectionViewCell  = cell as? AdsCollectionViewCell{
                            adsCell.nativeAdVIew.removeFromSuperview()
                        }
                    }
                    
                    self.defaults.set(false, forKey: Common.NOT_LOGIN_POST)
                    actionSheetView = ActionSheetAddButtonTimeLine()
//                    actionController = LGSemiModalNavViewController(rootViewController: self.actionSheetView)
                    let actionController = LGSemiModalNavViewController(rootViewController: actionSheetView)
                    actionController.view.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: 224)
                    actionController.navigationController?.setNavigationBarHidden(true, animated: true)
                    actionController.animationSpeed = 0.35
                    actionController.backgroundShadeColor = UIColor.black
                    actionController.isTapDismissEnabled = true
                    self.actionSheetView.delegateButton = self
                    actionController.springDamping = 0
                    actionController.springVelocity = 0
                    self.present(actionController, animated: true, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
                    vc.isBackgroundBlack = true
                    let nav = UINavigationController(rootViewController: vc)
                    self.navigationController?.present(nav, animated: true, completion: nil)
                }
            } else {
                PHPhotoLibrary.requestAuthorization() {statusPhoto in
                    switch statusPhoto {
                    case .authorized:
                        if BaseDAO.checkLogin() {
                            
                            for cell in self.collectionView.visibleCells  as [UICollectionViewCell]    {
                                if let adsCell:AdsCollectionViewCell  = cell as? AdsCollectionViewCell{
                                    adsCell.nativeAdVIew.removeFromSuperview()
                                }
                            }
                            
                            self.defaults.set(false, forKey: Common.NOT_LOGIN_POST)
                            self.actionSheetView = ActionSheetAddButtonTimeLine()
                            //                    actionController = LGSemiModalNavViewController(rootViewController: self.actionSheetView)
                            let actionController = LGSemiModalNavViewController(rootViewController: self.actionSheetView)
                            actionController.view.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: 224)
                            actionController.navigationController?.setNavigationBarHidden(true, animated: true)
                            actionController.animationSpeed = 0.35
                            actionController.backgroundShadeColor = UIColor.black
                            actionController.isTapDismissEnabled = true
                            self.actionSheetView.delegateButton = self
                            actionController.springDamping = 0
                            actionController.springVelocity = 0
                            self.present(actionController, animated: true, completion: nil)
                        } else {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
                            vc.isBackgroundBlack = true
                            let nav = UINavigationController(rootViewController: vc)
                            self.navigationController?.present(nav, animated: true, completion: nil)
                        }
                        break
                    case .denied, .restricted:
                        let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                            if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                                UIApplication.shared.openURL(appSettings as URL)
                            }
                        }
                        alertController.addAction(settingsAction)
                        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                        alertController.addAction(cancelAction)
                        DispatchQueue.main.async {
                            self.present(alertController, animated: true, completion: nil)
                        }
                        break
                    case .notDetermined: break
                        // won't happen but still
                    }
                }
            }
        }
        
        
        
        
        
        
        
        
//        if PHPhotoLibrary.authorizationStatus() != .authorized{
//            let alertController = UIAlertController(title: "", message: "Aktivieren Sie die Fotoberechtigungen in den Einstellungen", preferredStyle: .alert)
//            let settingsAction = UIAlertAction(title: "Einstellungen", style: .default) { (alertAction) in
//                if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
//                    UIApplication.shared.openURL(appSettings as URL)
//                }
//            }
//            alertController.addAction(settingsAction)
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//            alertController.addAction(cancelAction)
//            DispatchQueue.main.async {
//                self.present(alertController, animated: true, completion: nil)
//            }
//        } else {
//            let common = Common()
//            if !common.checkNetworkConnect() {
//                Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
//                                 message: NSLocalizedString("error_network_message", comment: ""),
//                                 titleCancel: NSLocalizedString("ok", comment: ""), complete: {
//                                    print("cancel")
//                })
//            }
//            if BaseDAO.checkLogin() {
//                
//                for cell in self.collectionView.visibleCells  as [UICollectionViewCell]    {
//                    if let adsCell:AdsCollectionViewCell  = cell as? AdsCollectionViewCell{
//                        adsCell.nativeAdVIew.removeFromSuperview()
//                    }
//                }
//                
//                self.defaults.set(false, forKey: Common.NOT_LOGIN_POST)
//                //                let actionController = LGSemiModalNavViewController(rootViewController: actionSheetView)
//                self.actionController.view.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: 224)
//                self.actionController.navigationController?.setNavigationBarHidden(true, animated: true)
//                self.actionController.animationSpeed = 0.35
//                self.actionController.backgroundShadeColor = UIColor.black
//                self.actionController.isTapDismissEnabled = true
//                self.actionSheetView.delegateButton = self
//                self.actionController.springDamping = 0
//                self.actionController.springVelocity = 0
//                self.present(self.actionController, animated: true, completion: nil)
//                
//            } else {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
//                vc.isBackgroundBlack = true
//                let nav = UINavigationController(rootViewController: vc)
//                self.navigationController?.present(nav, animated: true, completion: nil)
//            }
//        }
        
    }
    // add button action
    func btnCameraAction() {
        let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        if(cameraAuthorizationStatus == .authorized)
        {
            let cameraUI = UIImagePickerController()
            cameraUI.sourceType = UIImagePickerControllerSourceType.camera
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                if((self.presentationController) != nil) {
                    self.actionSheetView.dismiss(animated: true, completion: nil)
                    self.present(imagePicker, animated: true, completion: nil)
                }
            }
        } else {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                if granted {
                    let cameraUI = UIImagePickerController()
                    cameraUI.sourceType = UIImagePickerControllerSourceType.camera
                    
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                        imagePicker.allowsEditing = false
                        if((self.presentationController) != nil) {
                            self.actionSheetView.dismiss(animated: true, completion: nil)
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                    }
                } else {
                    if self.actionSheetView != nil {
                        self.actionSheetView.dismiss(animated: true, completion: nil)
                    }
                    let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(appSettings as URL)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    func btnGalleryAction() {
        if(isAuthorizePhoto == PHAuthorizationStatus.authorized)
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imageChooser = UIImagePickerController()
                imageChooser.delegate = self
                imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imageChooser.allowsEditing = true
                if((self.presentationController) != nil) {
                    self.actionSheetView.dismiss(animated: true, completion: nil)
                    self.present(imageChooser, animated: true, completion: nil)
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization() {statusPhoto in
                switch statusPhoto {
                case .authorized:
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imageChooser = UIImagePickerController()
                        imageChooser.delegate = self
                        imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imageChooser.allowsEditing = true
                        if((self.presentationController) != nil) {
                            self.actionSheetView.dismiss(animated: true, completion: nil)
                            self.present(imageChooser, animated: true, completion: nil)
                        }
                    }
                    break
                case .denied, .restricted:
                    let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(appSettings as URL)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                    break
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
    }
    
    func btnCancelAction() {
        self.actionSheetView.dismiss(animated: true) {
            for cell in self.collectionView.visibleCells  as [UICollectionViewCell]    {
                if let adsCell:AdsCollectionViewCell  = cell as? AdsCollectionViewCell{
                    adsCell.addSubview(adsCell.nativeAdVIew)
                }
            }
        }
    }
    
    //MARK: Image picker delegate
    func didSelectedImageAction(imageSelected: UIImage) {
        let postViewController = self.storyboard?.instantiateViewController(withIdentifier: "postTableViewControllerIdentifier") as! PostTableViewController
        postViewController.postImageReceived = imageSelected
        self.actionSheetView.dismiss(animated: true) {
            let nav = UINavigationController(rootViewController: postViewController)
            self.navigationController?.tabBarController?.present(nav, animated: true, completion: nil)
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let postViewController = self.storyboard?.instantiateViewController(withIdentifier: "postTableViewControllerIdentifier") as! PostTableViewController
        let common = Common()
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let scaledImage = common.scaleImageWidth(image: pickedImage, targetSize: CGSize(width: 750, height: 1334))
            postViewController.postImageReceived = scaledImage
        }
        dismiss(animated: true) {
            let nav = UINavigationController(rootViewController: postViewController)
            self.navigationController?.tabBarController?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    func playGif(sender: UIButton)
    {
        let cell = self.getCellAtIndexPath(index: sender.tag)
        if (cell?.viewGif.IsPlaying())!
        {
            cell?.viewGif.PauseVideo()
            sender.setImage(UIImage(named: "gif"), for: .normal)
        }else{
            cell?.viewGif.PlayVideo()
            sender.setImage(nil, for: .normal)
        }
    }
    
    //MARK: Newly Delegate
    func newlyDidTapped() {
        //        self.collectionView?.setContentOffset(CGPoint.zero, animated: true)
        //        self.collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0) as IndexPath, at: .top, animated: true)
        //        refreshControl.beginRefreshing()
        self.refresh(sender: self)
    }
    
    //MARK: - FullScreen NAVBAR
    
    func tapCloseFullScreen()
    {
        if self.isProfile{
            appDelegate.setStatusBarBackgroundColor(color: .white)
        }
        UIApplication.shared.statusBarStyle = .default
        if !self.isProfile{
            self.collectionView.scrollToItem(at: IndexPath(item: self.currentFullScreenIndex, section: 0), at: .centeredVertically, animated: true)
        }
        galleryViewController.closeGallery(true, completion: nil)
        galleryViewController = nil
    }
    
    //MARK: - FullScreen TOOLBAR
    func voteAction(sender: Any)
    {
        if BaseDAO.checkLogin() {
            let index: UIButton = sender as! UIButton
            let resultData = currentArray[index.tag] as! PostResponse
            switch resultData.is_vote {
            case 0:
                
                resultData.post_point += 1
                self.nbsToolbar.btnVote.tintColor = Common.blueColor()
                nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
                resultData.is_vote = 1
                
                PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 1) { (success: Bool, result) in
                    if success {
                        print("success")
                        resultData.post_point = Int(result!)
                        self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                        self.loadlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!, rowIndex: index.tag)
                    }
                }
                break
            case 1:
                resultData.post_point -= 1
                self.nbsToolbar.btnVote.tintColor = UIColor.white
                nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
                resultData.is_vote = 0
                
                PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 0) { (success: Bool, result) in
                    if success {
                        print("success")
                        resultData.post_point = Int(result!)
                        self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                        self.loadlikeHome(color: Common.lighGrayColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                    }
                }
                break
            case -1:
                resultData.post_point += 2
                self.nbsToolbar.btnVote.tintColor = Common.blueColor()
                self.nbsToolbar.btnUnVote.tintColor = UIColor.white
                nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
                resultData.is_vote = 1
                
                PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 1) { (success: Bool, result) in
                    if success {
                        print("success")
                        resultData.post_point = Int(result!)
                        self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                        self.loadlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                    }
                }
                break
            default:
                break
            }
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            galleryViewController.present(nav, animated: true, completion: nil)
        }
    }
    
    func loadlikeHome (color: UIColor, numberLike: String, rowIndex: Int)
    {
        let cell = self.getCellAtIndexPath(index: rowIndex)
        cell?.postLikeButton.tintColor = color
        cell?.postUnlikeButton.tintColor = Common.lighGrayColor()
        cell?.postPoints.text = numberLike
        
    }
    
    func loadUnlikeHome (color: UIColor, numberLike: String, rowIndex: Int)
    {
        let cell = self.getCellAtIndexPath(index: rowIndex)
        cell?.postUnlikeButton.tintColor = color
        cell?.postLikeButton.tintColor = Common.lighGrayColor()
        cell?.postPoints.text = numberLike
    }
    
    func unVoteAction(sender: Any)
    {
        if BaseDAO.checkLogin() {
            let index: UIButton = sender as! UIButton
            let resultData = currentArray[index.tag] as! PostResponse
            switch resultData.is_vote {
            case 0:
                
                resultData.post_point -= 1
                self.nbsToolbar.btnUnVote.tintColor = Common.blueColor()
                nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
                resultData.is_vote = -1
                
                PostDAO.userVote(post_id: Int(resultData.post_id)!, value: -1) { (success: Bool, result) in
                    if success {
                        print("success")
                        resultData.post_point = Int(result!)
                        self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                        self.loadUnlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                    }
                }
                break
            case 1:
                resultData.post_point -= 2
                nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
                resultData.is_vote = -1
                self.nbsToolbar.btnUnVote.tintColor = Common.blueColor()
                self.nbsToolbar.btnVote.tintColor = UIColor.white
                
                PostDAO.userVote(post_id: Int(resultData.post_id)!, value: -1) { (success: Bool, result) in
                    if success {
                        print("success")
                        resultData.post_point = Int(result!)
                        self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                        self.loadUnlikeHome(color: Common.blueColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                    }
                }
                break
            case -1:
                resultData.post_point += 1
                self.nbsToolbar.btnUnVote.tintColor = UIColor.white
                nbsNav.lblVote.text =  "\(resultData.post_point)" + NSLocalizedString("point", comment: "") + resultData.post_comment + NSLocalizedString("comment", comment: "")
                resultData.is_vote = 0
                
                PostDAO.userVote(post_id: Int(resultData.post_id)!, value: 0) { (success: Bool, result) in
                    if success {
                        print("success")
                        resultData.post_point = Int(result!)
                        self.nbsNav.lblVote.text =  "\(Int((resultData.post_point)))" + NSLocalizedString("point", comment: "") + (resultData.post_comment) + NSLocalizedString("comment", comment: "")
                        self.loadUnlikeHome(color: Common.lighGrayColor(), numberLike: self.nbsNav.lblVote.text!,  rowIndex: index.tag)
                    }
                }
                break
            default:
                break
            }
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            galleryViewController.present(nav, animated: true, completion: nil)
        }
    }
    
    func commentAction(sender: Any)
    {
        if galleryViewController != nil{
            
            let button = sender as! UIButton
            let data = currentArray[button.tag] as! PostResponse
            let vc = CommentTableViewController()
            vc.post_id = data.post_id
            vc.isPresented = true
            galleryViewController.presentDetail(UINavigationController(rootViewController: vc))
        }else{
            let button = sender as! UIButton
            let data = currentArray[button.tag] as! PostResponse
            let vc = CommentTableViewController()
            vc.post_id = data.post_id
            self.show(vc, sender: nil)
        }
    }
    
    // MARK: SHARE BUTTON
    func shareAction(sender:Any)
    {
        let linkToShare: URL
        let cell  = self.getCellAtIndexPath(index: (sender as! UIButton).tag)
        var activityItems: [AnyObject]?
        let activity = TUSafariActivity()
        if  cell?.postImage == nil && category_id == -1 ||  category_id == -2 {
            let linkToShare = URL(string: (cell?.postLink!)!)
            activityItems = [linkToShare as AnyObject]
        } else {
            if cell?.postLink == nil {
                linkToShare = URL(string:NSLocalizedString("web_app_link", comment: ""))!
                activityItems = [linkToShare as AnyObject]
            } else {
                linkToShare = URL(string: (cell?.postLink!)!)!
                let imgToShare = cell?.postImage.image
                if imgToShare != nil{
                    activityItems = [linkToShare as AnyObject , imgToShare!]
                }
            }
        }
        if activityItems != nil{
            let actionSheetShare = UIActivityViewController(activityItems: activityItems!, applicationActivities: [activity])
            if galleryViewController != nil {
                galleryViewController.present(actionSheetShare, animated: true, completion: nil)
            } else {
                self.present(actionSheetShare, animated: true, completion: nil)
            }
        }
    }
    
    func setUpButtonBar(){
        nbsNav.btnClose.addTarget(self, action: #selector(tapCloseFullScreen), for: .touchUpInside)
        
        var frame = nbsToolbar.frame
        frame.size.width = self.view.frame.width
        nbsToolbar.frame = frame
        
        frame = nbsNav.frame
        frame.size.width = self.view.frame.width
        nbsNav.frame = frame
        
    }
    
    func setUpToolBar()
    {
        nbsToolbar.btnShare.setTitle(NSLocalizedString("share", comment: ""), for: .normal)
        nbsToolbar.btnVote.addTarget(self, action: #selector(voteAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnUnVote.addTarget(self, action: #selector(unVoteAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnComment.addTarget(self, action: #selector(commentAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnMore.addTarget(self, action: #selector(postMoreAction(sender:)), for: .touchUpInside)
        nbsToolbar.btnShare.addTarget(self, action: #selector(shareAction(sender:)), for: .touchUpInside)
    }
    
    func reloadNavBarData(index: Int){
        
        if(index >= self.currentArray.count - 4){
            if loadingStatus == .Finished {
                loadingStatus = .Loading
                var number = 1
                if appDelegate.isAds == true{
                    number = 2
                }
                let lastData = self.currentArray[self.currentArray.count - number] as! PostResponse
                since = lastData.post_date
                last = ""
                self.perform(#selector(queryPost))
            }
        }

        
        
        let data = currentArray[index] as! PostResponse
        if data.isAds == false{
            nbsToolbar.isHidden = false
            nbsNav.lblTitle.text = data.post_title
            nbsNav.lblVote.text =  "\(data.post_point)" + NSLocalizedString("point", comment: "") + data.post_comment + NSLocalizedString("comment", comment: "")
            nbsToolbar.btnUnVote.tag = index
            nbsToolbar.btnVote.tag = index
            switch data.is_vote {
            case 0:
                nbsToolbar.btnVote.tintColor = UIColor.white
                nbsToolbar.btnUnVote.tintColor = UIColor.white
                break
            case 1:
                nbsToolbar.btnVote.tintColor = Common.blueColor()
                nbsToolbar.btnUnVote.tintColor = UIColor.white
                break
            case -1:
                nbsToolbar.btnUnVote.tintColor = Common.blueColor()
                nbsToolbar.btnVote.tintColor = UIColor.white
                break
            default:
                break
            }
        }
        else{
            nbsToolbar.isHidden = true
            nbsNav.lblTitle.text = "Zeit für Werbung"
            nbsNav.lblVote.text = ""
        }
    }
    
}



