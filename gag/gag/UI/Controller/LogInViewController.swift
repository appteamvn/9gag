//
//  LogInViewController.swift
//  gag
//
//  Created by ThanhToa on 12/2/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import GoogleSignIn
import Google
//import ProgressHUD
let NORMAL_USER = "email"
let FACEBOOK_USER = "facebook"
let GOOGLE_USER = "google"
class LogInViewController: UIViewController, FBSDKLoginButtonDelegate, GIDSignInDelegate, GIDSignInUIDelegate{
    
    var  mainTabbar: MainTabbarController!
    let common = Common()
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        Common.showLoading(view: self.view, string: NSLocalizedString("loading", comment: ""))
        if (error != nil) {
            print("log in google error")
            Common.hideLoading(view: self.view)
            return
        }
        // MARK: - PUSH GG USER TO SERVER
        UserDAO.userLogin(user_type: GOOGLE_USER, user_id: user.userID,
                          user_name: user.profile.givenName, user_email: user.profile.email,
                          user_password: "",
                          completeHandle: {(success, data) in
                            Common.hideLoading(view: self.view)
                            if success {
                                if data != nil {
                                    DispatchQueue.main.async{
                                        self.userDefaults.setValue(data?.secret_key, forKey: Common.SECRET_KEY)
                                        self.userDefaults.setValue(data?.user_id, forKey: Common.USER_ID)
                                        self.saveUserData(user_type: GOOGLE_USER,
                                                          user_id: (data?.user_id)!,
                                                          user_name: (data?.user_name)!,
                                                          user_email: (data?.user_email)!,
                                                          user_avatar: (data?.user_avatar)!)
                                        self.userDefaults.setValue(data?.device_token_ios, forKey: Common.TOKEN_ID)
                                        self.logInSuccess()
                                    }
                                }
                            }
        })
        print("logged in")
        //        logInSuccess()
    }
    
    func saveUserData(user_type: String, user_id: String, user_name: String, user_email: String, user_avatar: String) {
        userDefaults.setValue(user_type, forKey: Common.USER_TYPE)
        userDefaults.setValue(user_id, forKey: Common.USER_ID)
        userDefaults.setValue(user_name, forKey: Common.USER_NAME)
        userDefaults.setValue(user_email, forKey: Common.USER_EMAIL)
        userDefaults.setValue(user_avatar, forKey: Common.USER_AVATAR)
        userDefaults.synchronize()
    }
    /*!
     @abstract Sent to the delegate when the button was used to logout.
     @param loginButton The button that was clicked.
     */
    public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logged in")
    }
    
    /*!
     @abstract Sent to the delegate when the button was used to login.
     @param loginButton the sender
     @param result The results of the login
     @param error The error (if any) from the login
     */
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print("error")
            return
        }
        let parameters = ["fields": "email, id, name" ]
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: parameters)
        
        if let result = result as? [String: Any] {
            guard let userEmail = result["email"] as? String else {
                return
            }
            guard let userName = result["name"] as? String else {
                return
            }
            guard let userId = result["id"] as? String else {
                return
            }
                       let userLogin = User(user_type: 2,
                                 user_id: userId as NSString!,
                                 user_name: userName as NSString!,
                                 user_email: userEmail as NSString!,
                                 user_password: "",
                                 user_avatar: "")
            
            userDefaults.setValue(userLogin.user_type, forKey: Common.USER_TYPE)
            userDefaults.setValue(userLogin.user_id, forKey: Common.USER_ID)
            userDefaults.setValue(userLogin.user_name, forKey: Common.USER_NAME)
            userDefaults.setValue(userLogin.user_email, forKey: Common.USER_EMAIL)
            userDefaults.setValue(userLogin.user_avatar, forKey: Common.USER_AVATAR)
            userDefaults.synchronize()

            
            print("UID: \(userId)")
            print("UNAME: \(userName)")
            print("UEmail: \(userEmail)")
        }
        print("facebook log in successfully")
    }
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
//    @IBOutlet weak var forgotPassword: UILabel!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var btnPrivacy: UIButton!
    @IBOutlet weak var lblAnd: UILabel!
    
    let errorPointer: NSError? = nil
    var isSignUp = false
    var isLogIn = false
    let signInGG = GIDSignIn.sharedInstance()
    let fblogInManager = FBSDKLoginManager()
    var userDefaults = UserDefaults()
    let whatNewsURL = NSURL(string: NSLocalizedString("web_app_link", comment: ""))
    let userResponse = UserResponse()
    let alert = UIAlertView()
    override func viewDidLoad() {
        super.viewDidLoad()
        fullNameTextField.placeholder = NSLocalizedString("full_name", comment: "")
        emailTextField.placeholder = NSLocalizedString("email", comment: "")
        passwordTextField.placeholder = NSLocalizedString("password", comment: "")
        self.facebookButton.setTitle(NSLocalizedString("facebook", comment: ""), for: .normal)
        self.googleButton.setTitle(NSLocalizedString("google", comment: ""), for: .normal)
        if self.tabBarController != nil {
            mainTabbar = self.tabBarController as! MainTabbarController
        } else  {
            if BaseDAO.checkLogin() {
                mainTabbar = MainTabbarController()
            } else {
                self.navigationController?.navigationBar.barTintColor = UIColor.white
            }
        }
        self.btnTerms.setTitle(NSLocalizedString("terms", comment: ""), for: .normal)
        self.btnPrivacy.setTitle(NSLocalizedString("policy", comment: ""), for: .normal)
        self.lblAnd.text = NSLocalizedString("and", comment: "")
        if isSignUp {
            self.navigationItem.title = NSLocalizedString("join_app", comment: "")
//            self.forgotPassword.text = NSLocalizedString("term_and_policy", comment: "")
            self.signUpButton.setTitle(NSLocalizedString("sign_up", comment: ""), for: .normal)
        } else {
            self.fullNameTextField.isHidden = true
            self.navigationItem.title = NSLocalizedString("welcome_back", comment: "")
//            self.forgotPassword.text = NSLocalizedString("forgot_password", comment: "")
            self.signUpButton.setTitle(NSLocalizedString("log_in", comment: ""), for: .normal)
        }
        
        self.signUpButton.layer.cornerRadius = 3
//        facebookButton.isHidden = true
//        googleButton.isHidden = true
        
//        setupFacebookButtons()
//        setupGoogleButtons()
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapForgotPassword))
//        self.forgotPassword.isUserInteractionEnabled = true
//        self.forgotPassword.addGestureRecognizer(tap)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Common.hideLoading(view: self.view)
    }
    
    @IBAction func tapTerms(_ sender: Any) {
        let url = URL(string: NSLocalizedString("terms_url", comment: ""))!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func tapPrivacyBtn(_ sender: Any) {
        let url = URL(string: NSLocalizedString("policy_url", comment: ""))!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
//    func tapForgotPassword() {
//        if self.forgotPassword.text == NSLocalizedString("forgot_password", comment: "") {
//            let forgotPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController")  as! ForgotPasswordViewController
//            self.show(forgotPasswordViewController, sender: nil)
//        } else {
//            // go to fackju policy
//            let url = URL(string: NSLocalizedString("app_privacy_link", comment: ""))!
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
//    }
    
    func setupFacebookButtons() {
        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        loginButton.frame = CGRect(x: 16, y: 70, width: view.frame.width - 32, height: 50)
        loginButton.delegate = self
    }
    func setupGoogleButtons() {
        let googleButton = GIDSignInButton()
        googleButton.frame = CGRect(x: 16, y: 125 , width: view.frame.width - 32, height: 50)
        view.addSubview(googleButton)
        GIDSignIn.sharedInstance().delegate = self
        if errorPointer != nil {
            print("log in error")
            return
        }
        print("gg log in successfully")
    }
    
    
    @IBAction func facebookBtnAction(_ sender: Any) {
        if !common.checkNetworkConnect() {
            Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
                             message: NSLocalizedString("error_network_connect_server", comment: ""),
                             titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                print("cancel")
                                return
            })
        } else {
            Common.showLoading(view: self.view, string: nil)
            fblogInManager.logIn(withReadPermissions: [ "public_profile", "email"], from: self, handler: {(result, err) in
                if err != nil {
                    print("Failed log in FB: " + (err?.localizedDescription)!)
                    Common.hideLoading(view: self.view)
                    return
                } else {
                    print("Facebook login successfully")
                    let fbLogInresult: FBSDKLoginManagerLoginResult = result!
                    if fbLogInresult.grantedPermissions != nil {
                        if fbLogInresult.grantedPermissions.contains("email") {
                            if FBSDKAccessToken.current() != nil {
                                self.getFBUserData()
                            }
                        }
                    }
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                                Common.hideLoading(view: self.view)
                            })
                }
            })
        }
    }
    
    // MARK: - PUSH FB USER TO SERVER
    func getFBUserData() {
        Common.showLoading(view: self.view, string: NSLocalizedString("loading", comment: ""))
            if FBSDKAccessToken.current() != nil {
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, birthday ,picture.tupe(large), email, gender"]).start(completionHandler: {(connection, result, error) -> Void in
                    print("LOGIN FB")
                    if error != nil {
                        print("error when getFBUserData")
                        Common.hideLoading(view: self.view)
                    } else {
                        let data = result as! [String: AnyObject]
                        print("data" + data.description)
                        let fbid = data["id"] as? String
                        let urlAvatar = "https://graph.facebook.com/\(fbid!)/picture?type=large&return_ssl_resources=1"
                        print(result!)
                        var gender = 3
                        if data["gender"] as! String == "unspecified" {
                            gender = 0
                        } else if data["gender"] as! String == "male" {
                            gender = 2
                        } else if data["gender"] as! String == "female" {
                            gender = 1
                        }
                        self.userDefaults.setValue(gender, forKey: Common.USER_GENDER)
//                        let user_birthday = data["birthday"]
                        if data["email"] == nil {
                           self.showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("login_fb_email", comment: ""))
                        } else {
                            UserDAO.userFacebookLogin(user_type: FACEBOOK_USER, user_id: data["id"] as! String,
                                                      user_name: data["name"] as! String, user_email: data["email"] as! String,
                                                      user_password: "",
                                                      completeHandle: {(success, data) in
                                                        if success {
                                                            if data != nil {
                                                                DispatchQueue.main.async{
                                                                    self.userDefaults.setValue(data?.secret_key, forKey: Common.SECRET_KEY)
                                                                    self.userDefaults.setValue(data?.user_id, forKey: Common.USER_ID)
                                                                    self.saveUserData(user_type: FACEBOOK_USER,
                                                                                      user_id: (data?.user_id)!,
                                                                                      user_name:(data?.user_name)!,
                                                                                      user_email: (data?.user_email)!,
                                                                                      user_avatar: (data?.user_avatar)!)
                                                                    self.userDefaults.setValue(data?.device_token_ios, forKey: Common.TOKEN_ID)
                                                                    self.userDefaults.synchronize()
                                                                    self.logInSuccess()
                                                                    Common.hideLoading(view: self.view)
                                                                }
                                                            }
                                                        }
                            })
                        }
                    }
                })
        }
    }
    // google sign in - start
    @IBAction func googleBtnAction(_ sender: Any) {
        if !common.checkNetworkConnect() {
            Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
                             message: NSLocalizedString("error_network_connect_server", comment: ""),
                             titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                print("cancel")
                                return
            })
        } else {
            signInGG?.delegate = self
            signInGG?.uiDelegate = self
            signInGG?.shouldFetchBasicProfile = true
            signInGG?.signIn()
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    // google sign in - start
    // MARK: - ACTION LOGIN-SIGNUP
    @IBAction func signUpLoginAction(_ sender: Any) {
        if isSignUp {
            self.actionForSignUp()
        } else {
            self.actionForLogIn()
        }
    }
    
    func actionForSignUp() {
        self.view.endEditing(true)
        if (self.fullNameTextField.text?.isEmpty)! && (self.emailTextField.text?.isEmpty)! && (self.passwordTextField.text?.isEmpty)! {
            self.showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("error_nil_sigup", comment: ""))
        } else if (self.emailTextField.text?.isEmpty)! &&  (self.passwordTextField.text?.isEmpty)!{
            self.showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("error_nil_email", comment: ""))
        } else if (self.passwordTextField.text?.isEmpty)! {
            self.showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("error_nil_password", comment: ""))
        } else {
            if !common.checkNetworkConnect() {
                Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
                                 message: NSLocalizedString("error_network_connect_server", comment: ""),
                                 titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                    print("cancel")
                                    return
                })
            }
            let userFullname = self.fullNameTextField.text
            let userEmail = self.emailTextField.text
            let userPassword = self.passwordTextField.text
            UserDAO.registerUser(login_name: userFullname!, login_email: userEmail!,
                                 login_password: userPassword!, completeHandle: {(success, data) in
                                    Common.hideLoading(view: self.view)
                                    if data != nil {
                                        DispatchQueue.main.async{
                                            self.userDefaults.setValue(data?.secret_key, forKey: Common.SECRET_KEY)
                                            self.userDefaults.setValue(data?.user_id, forKey: Common.USER_ID)
                                            self.userDefaults.setValue(userFullname, forKey: Common.USER_NAME)
                                            self.userDefaults.setValue(userEmail, forKey: Common.USER_EMAIL)
                                            self.userDefaults.setValue(userPassword, forKey: Common.USER_PASSWORD)
                                            self.userDefaults.synchronize()
                                            // process show profile
                                            self.processAfterSignup(email: userEmail!, password: userPassword!)
                                            Common.showHubSuccess(string: NSLocalizedString("sign_up_success", comment: ""))
                                            }
                                    } else {
                                        Common.showHubWithMessage(string: NSLocalizedString("sign_up_fail", comment: ""))
                                    }
            })
        }
    }

    
    func actionForLogIn() {
        self.view.endEditing(true)
        if (self.emailTextField.text?.isEmpty)! && (self.passwordTextField.text?.isEmpty)! {
            self.showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("error_nil_login", comment: ""))
        } else {
            if !common.checkNetworkConnect() {
                Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
                                 message: NSLocalizedString("error_network_connect_server", comment: ""),
                                 titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                    print("cancel")
                                    return
                })
            } else {
                Common.showHubSuccess(string: NSLocalizedString("loading", comment: ""))
                let userEmail = self.emailTextField.text
                let userPassword = self.passwordTextField.text
                UserDAO.userEmailLogin(user_type: NORMAL_USER, user_email: userEmail!, user_password: userPassword!, completeHandle: {(success, data) in
                    Common.hideLoading(view: self.view)
                    if success {
                        if data != nil {
                            DispatchQueue.main.async{
                                self.userDefaults.setValue(data?.user_id, forKey: Common.USER_ID)
                                self.userDefaults.setValue(data?.user_name, forKey: Common.USER_NAME)
                                self.userDefaults.setValue(userEmail, forKey: Common.USER_EMAIL) // temp
                                self.userDefaults.setValue(userPassword, forKey: Common.USER_PASSWORD)
                                self.userDefaults.setValue(data?.user_avatar, forKey: Common.USER_AVATAR)
                                self.userDefaults.setValue(data?.secret_key, forKey: Common.SECRET_KEY)
                                self.userDefaults.setValue(data?.device_token_ios, forKey: Common.TOKEN_ID)
                                self.userDefaults.synchronize()
                                self.logInSuccess()
                            }
                        } else {
                            self.alert.title = NSLocalizedString("app_name", comment: "")
                            self.alert.message = NSLocalizedString("incorrect_password", comment: "")
                            self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                            self.alert.show()
                        }
                    } else {
                        self.alert.title = NSLocalizedString("app_name", comment: "")
                        self.alert.message = NSLocalizedString("incorrect_password", comment: "")
                        self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                        self.alert.show()
                    }
                    
                })
            }
        }
    }

    func logInSuccess()
    {
        Common.showHubSuccess(string: NSLocalizedString("login_success_message_dialog", comment: ""))
        self.dismiss(animated: true, completion: nil)
        self.mainTabbar?.loginNav.popViewController(animated: false)
        self.mainTabbar?.loginNavNew.popViewController(animated: false)
        self.userDefaults.set(true, forKey: Common.NOT_LOGIN_POST)

        let notLoginNotify = self.userDefaults.bool(forKey: Common.NOT_LOGIN_NOTIFY)
        if notLoginNotify {
            if mainTabbar.selectedIndex == 4 {
                self.mainTabbar?.switchProfiles()
            } else {
                self.mainTabbar?.switchNews()
            }
//            let profileNotificationTableView = self.storyboard?.instantiateViewController(withIdentifier: "profileNotificationTableViewIdentifier") as! ProfileNotificationTableView
//            mainTabbar.show(profileNotificationTableView, sender: nil)
//            self.navigationController?.show(profileNotificationTableView, sender: nil)
            
//            self.show(profileNotificationTableView, sender: nil)
        } else {
            if self.mainTabbar?.selectedIndex == 3 {
                self.mainTabbar?.switchNews()
            } else {
                self.mainTabbar?.switchProfiles()
            }
        }

    }
    
    func showAlertMessage(title: String, message: String) {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        alert.show()
    }
    
    func processAfterSignup(email:String, password: String) {
        UserDAO.userEmailLogin(user_type: NORMAL_USER, user_email: email, user_password: password, completeHandle: {(success, data) in
            Common.hideLoading(view: self.view)
            if success {
                if data != nil {
                    DispatchQueue.main.async{
                        self.userDefaults.setValue(data?.user_id, forKey: Common.USER_ID)
                        self.userDefaults.setValue(data?.user_name, forKey: Common.USER_NAME)
                        self.userDefaults.setValue(email, forKey: Common.USER_EMAIL) // temp
                        self.userDefaults.setValue(password, forKey: Common.USER_PASSWORD)
                        self.userDefaults.setValue(data?.user_avatar, forKey: Common.USER_AVATAR)
                        self.userDefaults.setValue(data?.secret_key, forKey: Common.SECRET_KEY)
                        self.userDefaults.synchronize()
                        self.logInSuccess()
                    }
                }
            }
        })
    }
}
