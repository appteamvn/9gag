//
//  PostSelectionTableView.swift
//  gag
//
//  Created by HieuNT50 on 11/25/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import KYNavigationProgress
import Kingfisher

class PostSelectionTableView: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var postSelectionTable: UITableView!
    @IBOutlet weak var postButton: UIButton!
    var userDefaults = UserDefaults()
    let alert = UIAlertView()
    var post_title = String()
    var post_safe: String!
    let post_source_url = ""
    var data = UIImage()
    var arrayCategory : [CategoryResponse] = []
    var category_id = Int(0)
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("select_session", comment: "")
        postSelectionTable.delegate = self
        postSelectionTable.dataSource = self
        postButton.setTitle(NSLocalizedString("post", comment: ""), for: .normal)
        postButton.isUserInteractionEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCategoryList()
    }
    
    func getCategoryList()
    {
        arrayCategory = PostDAO.PostDaoSingleton.sharedInstance.categoryPost
        if arrayCategory.count > 0{
            self.postSelectionTable.reloadData()
        }else{
            PostDAO.getCategoryPost { (success, data: [CategoryResponse]?) in
                if success{
                    self.arrayCategory = data!
                    self.postSelectionTable.reloadData()
                }
            }
        }
    }
    // MARK: - USER POST
    @IBAction func postSelectionAction(_ sender: Any) {
        let button = sender as! UIButton
        button.isEnabled = false
        
        PostDAO.userPost(user_id: "\(BaseDAO.getUserId())",
                         title: self.post_title,
                         category_id: String(self.category_id),
                         safe: self.post_safe,
                         source_url: self.post_source_url,
                         data: data.size.width > 500 ? data.resizeWith(width: 500.0)! : data,
                         navigationBar:  self.navigationController,
                         completeHandle: {(success) in
                
        })
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Common.UPLOADING), object: nil)
            })
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCategory.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "postSelectionTableViewIdentifier", for: indexPath) as! PostSelectionTableViewCell
        
       // set up checkbox
        cell.postCheckBoxView.line             = .thin
        cell.postCheckBoxView.bgColorSelected  = UIColor(red: 46/255, green: 119/255, blue: 217/255, alpha: 1)
        cell.postCheckBoxView.bgColor          =  UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        cell.postCheckBoxView.color            = UIColor.white
        cell.postCheckBoxView.borderColor      = UIColor.white
        cell.postCheckBoxView.borderWidth      = 2
        cell.postCheckBoxView.cornerRadius = cell.postCheckBoxView.frame.height / 2
        
        cell.postTypeTitle.text = arrayCategory[indexPath.row].category_name
        cell.postTypeSubtitle.text = arrayCategory[indexPath.row].category_description
        let url = URL(string: arrayCategory[indexPath.row].category_image)
        cell.postTypeImage.kf.setImage(with: ImageResource(downloadURL: url!,cacheKey: nil))
        cell.postTypeImage.layer.masksToBounds = true
        cell.postTypeImage.layer.cornerRadius = 3
        if indexPath.row ==  selectedIndex
        {
            cell.postCheckBoxView.setOn(true)
        }
        else{
            cell.postCheckBoxView.setOn(false)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        let cell  = tableView.cellForRow(at: indexPath) as!  PostSelectionTableViewCell
        self.category_id = arrayCategory[indexPath.row].category_id
        cell.postCheckBoxView.setOn(true)
        self.postSelectionTable.reloadData()
        self.postButton.backgroundColor = UIColor.init(hexString: "3379D6")
        postButton.isUserInteractionEnabled = true
        
    }
//    func switchAction(state:Bool) {
//        self.post_safe = state ? 1 : 0
//    }

}
