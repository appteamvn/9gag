//
//  MessageCommentCell.swift
//  gag
//
//  Created by Hieu Nguyen on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
class MessageCommentCell : UITableViewCell {
    
    // Initialized Super Comment Cell
    @IBOutlet weak var imgAvatar:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblOP:UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnUnlike: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblShowReply: UILabel!
    
    func viewDidLoad()
    {
//        super.viewDidLoad()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnReply.setTitle(NSLocalizedString("reply", comment: ""), for: .normal)
        btnMore.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
    }
    
//    override init (frame:CGRect)
//    {
//        super.init(frame:frame)
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}


/* 
 
 - (void)configureSubviews
 {
 [self.contentView addSubview:self.thumbnailView];
 [self.contentView addSubview:self.titleLabel];
 [self.contentView addSubview:self.bodyLabel];
 
 NSDictionary *views = @{@"thumbnailView": self.thumbnailView,
 @"titleLabel": self.titleLabel,
 @"bodyLabel": self.bodyLabel,
 };
 
 NSDictionary *metrics = @{@"tumbSize": @(kMessageTableViewCellAvatarHeight),
 @"padding": @15,
 @"right": @10,
 @"left": @5
 };
 
 [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[thumbnailView(tumbSize)]-right-[titleLabel(>=0)]-right-|" options:0 metrics:metrics views:views]];
 [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[thumbnailView(tumbSize)]-right-[bodyLabel(>=0)]-right-|" options:0 metrics:metrics views:views]];
 [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-right-[thumbnailView(tumbSize)]-(>=0)-|" options:0 metrics:metrics views:views]];
 
 if ([self.reuseIdentifier isEqualToString:MessengerCellIdentifier]) {
 [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-right-[titleLabel(20)]-left-[bodyLabel(>=0@999)]-left-|" options:0 metrics:metrics views:views]];
 }
 else {
 [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options:0 metrics:metrics views:views]];
 }
 }
 
 - (void)prepareForReuse
 {
 [super prepareForReuse];
 
 self.selectionStyle = UITableViewCellSelectionStyleNone;
 
 CGFloat pointSize = [MessageTableViewCell defaultFontSize];
 
 self.titleLabel.font = [UIFont boldSystemFontOfSize:pointSize];
 self.bodyLabel.font = [UIFont systemFontOfSize:pointSize];
 
 self.titleLabel.text = @"";
 self.bodyLabel.text = @"";
 
 }
 
 #pragma mark - Getters
 
 - (UILabel *)titleLabel
 {
 if (!_titleLabel) {
 _titleLabel = [UILabel new];
 _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
 _titleLabel.backgroundColor = [UIColor clearColor];
 _titleLabel.userInteractionEnabled = NO;
 _titleLabel.numberOfLines = 0;
 _titleLabel.textColor = [UIColor grayColor];
 _titleLabel.font = [UIFont boldSystemFontOfSize:[MessageTableViewCell defaultFontSize]];
 }
 return _titleLabel;
 }
 
 - (UILabel *)bodyLabel
 {
 if (!_bodyLabel) {
 _bodyLabel = [UILabel new];
 _bodyLabel.translatesAutoresizingMaskIntoConstraints = NO;
 _bodyLabel.backgroundColor = [UIColor clearColor];
 _bodyLabel.userInteractionEnabled = NO;
 _bodyLabel.numberOfLines = 0;
 _bodyLabel.textColor = [UIColor darkGrayColor];
 _bodyLabel.font = [UIFont systemFontOfSize:[MessageTableViewCell defaultFontSize]];
 }
 return _bodyLabel;
 }
 
 - (UIImageView *)thumbnailView
 {
 if (!_thumbnailView) {
 _thumbnailView = [UIImageView new];
 _thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
 _thumbnailView.userInteractionEnabled = NO;
 _thumbnailView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
 
 _thumbnailView.layer.cornerRadius = kMessageTableViewCellAvatarHeight/2.0;
 _thumbnailView.layer.masksToBounds = YES;
 }
 return _thumbnailView;
 }
 
 + (CGFloat)defaultFontSize
 {
 CGFloat pointSize = 16.0;
 
 NSString *contentSizeCategory = [[UIApplication sharedApplication] preferredContentSizeCategory];
 pointSize += SLKPointSizeDifferenceForCategory(contentSizeCategory);
 
 return pointSize;
 }
 
*/
