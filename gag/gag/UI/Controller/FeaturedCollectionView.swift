//
//  FeaturedCollectionView.swift
//  gag
//
//  Created by ThanhToa on 11/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Kingfisher

class FeaturedCollectionView: BaseCollectionViewController, UICollectionViewDelegateFlowLayout {
    
    open var arrayFeatures: [PostResponse]! = []
    
    let gradientLayer = CAGradientLayer()
    let color1 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.05).cgColor
    let color2 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65).cgColor
    let color3 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.75).cgColor
    let color4 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.86).cgColor

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("featured", comment: "")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
       // self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return arrayFeatures.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featuredCollectionViewCellIdentifier", for: indexPath) as! FeaturedCollectionViewCell
        
        let data = arrayFeatures[indexPath.item]
        cell.featuredTitle.text = data.post_title
        cell.featuredPoint.text =  "\(data.post_point)" + " Points  " + data.post_comment + NSLocalizedString("comment", comment: "")
        if let url: URL = URL(string: data.url){
            cell.featuredImage.kf.setImage(with: ImageResource(downloadURL: url,cacheKey: nil))
        }
        cell.contentView.layer.cornerRadius = 3
        cell.contentView.layer.masksToBounds = true
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell  = collectionView.cellForItem(at: indexPath) as! FeaturedCollectionViewCell
        
        let data = arrayFeatures[indexPath.item]
        let vc = CommentTableViewController()
        vc.post_id = data.post_id
        vc.imgAvatar = cell.featuredImage.image
        vc.data = data
        vc.isDetail = true
        self.show(vc, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.size.width * 0.9, height: (collectionView.frame.size.width * 0.9) * 9 / 16)
    }

    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
