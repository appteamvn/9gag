    //
//  ProfileSettingsTableViewController.swift
//  gag
//
//  Created by ThanhToa on 11/20/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Cosmos
import FBSDKLoginKit
import GoogleSignIn
import Google
import AJRNight
    
class ProfileSettingsTableViewController: UITableViewController, AJRNightDelegate {
    
    var section = [NSLocalizedString("fackju_pro", comment: ""),
                   NSLocalizedString("display_session", comment: ""),
                   NSLocalizedString("account_session", comment: ""),
                   NSLocalizedString("settings_session", comment: ""),
                   NSLocalizedString("others_session", comment: ""),
                   NSLocalizedString("logout_session", comment: "")]
    
    var footer = [NSLocalizedString("set_private_msg", comment: ""),
                  NSLocalizedString("language_introduction", comment: "")]
    
    var items = [[NSLocalizedString("show_ad", comment: ""),
                  NSLocalizedString("auto_night_mode", comment: ""),
                  NSLocalizedString("show_new_post", comment: "")],
                 [NSLocalizedString("auto_play_gif", comment: ""),
//                  NSLocalizedString("auto_expand_long_post", comment: ""),
                  NSLocalizedString("show_sensitive", comment: ""),
                  NSLocalizedString("night_mode", comment: "")],
                 [NSLocalizedString("edit_profile", comment: ""),
                  NSLocalizedString("change_password", comment: ""),
                ], //   NSLocalizedString("private_upvote", comment: "")
                 [NSLocalizedString("notification", comment: "")],
                 [NSLocalizedString("tell_app_to_others_user", comment: ""),
                  NSLocalizedString("provide_feedback", comment: ""),
                  NSLocalizedString("what_new", comment: "")],
                 [NSLocalizedString("clear_cache", comment: ""),
                  NSLocalizedString("logout", comment: "")]]
    
    var heighFooter = 0.0
    var userDefaults = UserDefaults()
    var  mainTabbar: MainTabbarController!
    let defaults = UserDefaults.standard
    let settingControllers = SettingControllerSingleton.sharedInstance
    let black = UIColor.black
    var isNotLogin = false
    var isProVersion = false

    override func viewDidLoad() {
        super.viewDidLoad()
        mainTabbar = self.tabBarController as! MainTabbarController
        self.navigationItem.title = NSLocalizedString("setting", comment: "")
        self.view.nightProperties = ["backgroundColor" : UIColor.black]
        UILabel.appearance().nightProperties = ["textColor": UIColor.white]
        AJRNight.sharedClient().addViewController(self)
        if isNotLogin {
            self.setDataIfNotLogin()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if userDefaults.bool(forKey: Common.NIGHT_MODE) {
            UINavigationBar.appearance().backgroundColor = black
        }
        var common = Common()
        common.cacheCalculator()
        self.tableView.reloadData()
        if !Common().checkNetworkConnect() {
            Common.showHubWithMessage(string: NSLocalizedString("error_network_connect_server", comment: ""))
        }
        
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section [section]
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 2 {
            return self.footer [0]
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = UIColor.lightGray
        headerView.textLabel?.font = UIFont.systemFont(ofSize: 14)
        if userDefaults.bool(forKey: Common.NIGHT_MODE) {
            view.backgroundColor = UIColor.black
        }

    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if userDefaults.bool(forKey: Common.NIGHT_MODE) {
        view.backgroundColor = UIColor.black
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 2 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 45))
            var frame  = headerView.bounds
            frame.origin.x = 10
            frame.origin.y = -6
            let title = UILabel(frame: frame)
            title.textAlignment = .left
            title.text = self.footer [0]
            title.textColor = UIColor.lightGray
            title.font = UIFont.systemFont(ofSize: 13.5)
            title.lineBreakMode = NSLineBreakMode.byWordWrapping
            title.numberOfLines = 2
            headerView.addSubview(title)
            headerView.backgroundColor = UIColor("#F3F3F3")
            return headerView
        }
        if !isNotLogin {
            if section == 3 {
                let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
                var frame  = headerView.bounds
                frame.origin.x = 10
                frame.origin.y = 5
                frame.size.width -= 20
                let title = UILabel(frame: frame)
                title.textAlignment = .left
                title.text = self.footer [1]
                title.textColor = UIColor.lightGray
                title.font = UIFont.systemFont(ofSize: 13.5)
                title.lineBreakMode = NSLineBreakMode.byWordWrapping
                title.numberOfLines = 3
                headerView.addSubview(title)
                headerView.backgroundColor = UIColor("#F3F3F3")
                return headerView
            }
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2 {
            return 35.0
        }
        if !isNotLogin {
            if section == 3 {
                return 70
            } else {
                return 10
            }
        } else {
            switch section {
            case 0:
                return 5
            case 1:
                return 5
//            case 2:
//                return 20.0
            case 3:
                return 30.0
            case 4:
                return 30.0
            default:
                break
            }
        }
        return 15.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
            return self.section.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items [section].count
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if userDefaults.bool(forKey: Common.DEFAULT_AUTO_NIGHT_MODE) {
            cell.contentView.backgroundColor = black
            cell.accessoryView?.backgroundColor = black
            cell.textLabel?.backgroundColor = black
            cell.detailTextLabel?.backgroundColor = black
            cell.backgroundColor = black
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
        let swt = UISwitch(frame: CGRect(x: 150, y: 300, width: 0, height: 0))
        cell.accessoryView = swt
        cell.detailTextLabel?.isHidden = true
        cell.accessoryType = .disclosureIndicator
        swt.addTarget(self, action: #selector(switchValueChanged(sender:)), for: UIControlEvents.valueChanged)
        
        let versionNumber = UILabel(frame: CGRect(x: (cell.accessoryView?.frame.origin.x)! - 20, y: (cell.accessoryView?.frame.origin.y)!, width: (cell.accessoryView?.frame.size.width)! + 20, height: (cell.accessoryView?.frame.size.height)!))
        versionNumber.lineBreakMode = .byWordWrapping
        versionNumber.numberOfLines = 0
        versionNumber.textColor = UIColor.lightGray
        // check login
        if isNotLogin {
            switch indexPath.section {
            case 0:
                cell.detailTextLabel?.text = nil
                switch indexPath.row {
                case 0:
                    if settingControllers.isShowAds == true {
                        swt.isOn = true
                    }
                    swt.tag = 100
                    break
                case 1:
                    if settingControllers.isAutoNightMode == true {
                        swt.isOn = true
                    }
                    swt.tag = 101
                    break
                case 2:
                    if settingControllers.isShowNewPost == true {
                        swt.isOn = true
                    }
                    swt.tag = 102
                    break
                default:
                    break
                }
                break
            case 1:
                switch indexPath.row {
                case 0:
                    if settingControllers.isAutoPlayGIFPost == true {
                        swt.isOn = true
                    }
                    swt.tag = 200
                    cell.detailTextLabel?.isHidden = false
                    cell.detailTextLabel?.text = NSLocalizedString("use_more_data", comment: "")
                    break
                case 1:
                    if settingControllers.isShowSensitiveContent == true {
                        swt.isOn = true
                    }
                    swt.tag = 201
                    cell.detailTextLabel?.text = nil
                    break
                case 2 :
                    if settingControllers.isNightMode == true {
                        swt.isOn = true
                    }
                    swt.tag = 202
                    cell.detailTextLabel?.text = nil
                    break
                default:
                    break
                }
                break
            case 2:
                switch indexPath.row {
                case 0:
                    cell.accessoryView = nil
                    cell.accessoryType = .disclosureIndicator
                    cell.detailTextLabel?.text = nil
                    break
                default:
                    break
                }
                break
            case 3:
                cell.accessoryView?.isHidden = true
                cell.detailTextLabel?.text = nil
                if cell.textLabel?.text == NSLocalizedString("what_new", comment: ""){
                    versionNumber.text = NSLocalizedString("version_number", comment: "")
                    //                cell.accessoryView = nil
                    cell.accessoryView = versionNumber
                    cell.accessoryType = .disclosureIndicator
                }
                break
            case 4:
                cell.accessoryView?.isHidden = true
                cell.detailTextLabel?.text = nil
                if cell.textLabel?.text == NSLocalizedString("clear_cache", comment: ""){
                    versionNumber.text =  self.userDefaults.object(forKey: Common.CACHE_SIZE) as? String // Cache
                    cell.accessoryView = nil
                    cell.accessoryView = versionNumber
                    cell.accessoryType = .disclosureIndicator
                }
                break
            default:
                break
            }
        } else {
            switch indexPath.section {
            case 0:
                cell.detailTextLabel?.text = nil
                switch indexPath.row {
                case 0:
                    if settingControllers.isShowAds == true {
                        swt.isOn = true
                    }
                    swt.tag = 100
                    break
                case 1:
                    if settingControllers.isAutoNightMode == true {
                        swt.isOn = true
                    }
                    swt.tag = 101
                    break
                case 2:
                    if settingControllers.isShowNewPost == true {
                        swt.isOn = true
                    }
                    swt.tag = 102
                    break
                default:
                    break
                }
                break
            case 1:
                switch indexPath.row {
                case 0:
                    if settingControllers.isAutoPlayGIFPost == true {
                        swt.isOn = true
                    }
                    swt.tag = 200
                    cell.detailTextLabel?.isHidden = false
                    cell.detailTextLabel?.text = NSLocalizedString("use_more_data", comment: "")
                    break
                case 1:
                    if settingControllers.isShowSensitiveContent == true {
                        swt.isOn = true
                    }
                    swt.tag = 201
                    cell.detailTextLabel?.text = nil
                    break
                case 2 :
                    if settingControllers.isNightMode == true {
                        swt.isOn = true
                    }
                    swt.tag = 202
                    cell.detailTextLabel?.text = nil
                    break
                default:
                    break
                }
                break
            case 2:
                switch indexPath.row {
                case 0:
                    cell.accessoryView = nil
                    cell.accessoryType = .disclosureIndicator
                    cell.detailTextLabel?.text = nil
                    break
                case 1:
                    cell.accessoryView = nil
                    cell.accessoryType = .disclosureIndicator
                    cell.detailTextLabel?.text = nil
                    break
                default:
                    break
                }
                break
            case 3:
                cell.accessoryView = nil
                cell.accessoryType = .disclosureIndicator
                cell.detailTextLabel?.text = nil
                break
            case 4:
                cell.accessoryView?.isHidden = true
                cell.detailTextLabel?.text = nil
                if cell.textLabel?.text == NSLocalizedString("what_new", comment: ""){
                    versionNumber.text = NSLocalizedString("version_number", comment: "")
                    //                cell.accessoryView = nil
                    cell.accessoryView = versionNumber
                    cell.accessoryType = .disclosureIndicator
                }
                break
            case 5:
                cell.accessoryView?.isHidden = true
                cell.detailTextLabel?.text = nil
                if cell.textLabel?.text == NSLocalizedString("clear_cache", comment: ""){
                    versionNumber.text = self.userDefaults.object(forKey: Common.CACHE_SIZE) as? String // Cache
                    cell.accessoryView = nil
                    cell.accessoryView = versionNumber
                    cell.accessoryType = .disclosureIndicator
                } else {
                    cell.accessoryType = .none
                }
                
                break
            default:
                break
            }
        }
                return cell
    }
    // TOANT1  continue setting not login
    func switchValueChanged(sender: Any){
        let swt = sender as! UISwitch
        switch swt.tag {
        case 100:
            if !Common().checkNetworkConnect() {
                Common.showAlert(title: NSLocalizedString("error", comment: ""),
                                 message: NSLocalizedString("can_not_connect_itune", comment: ""),
                                 titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                    print("cancel")
                })
                defaults.set(true, forKey: Common.DEFAULT_SHOW_ADS)
                self.tableView.reloadData()
                break
            }
            if !isProVersion {
                self.showAlertUnlock()
            } else {
                self.settingControllers.isShowAds = swt.isOn
                if self.settingControllers.isShowAds == true {
                    defaults.set(true, forKey: Common.DEFAULT_SHOW_ADS)
                }
                else
                {
                    defaults.removeObject(forKey: Common.DEFAULT_SHOW_ADS)
                }
            }
            break
        case 101:
            if !Common().checkNetworkConnect() {
                Common.showAlert(title: NSLocalizedString("error", comment: ""),
                                 message: NSLocalizedString("can_not_connect_itune", comment: ""),
                                 titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                    print("cancel")
                })
                defaults.set(true, forKey: Common.DEFAULT_SHOW_ADS)
                self.tableView.reloadData()
                break
            }
            if !isProVersion {
                self.showAlertUnlock()
            } else {
                self.settingControllers.isAutoNightMode = swt.isOn
                if self.settingControllers.isAutoNightMode == true {
                    defaults.set(true, forKey: Common.DEFAULT_AUTO_NIGHT_MODE)
                }
                else
                {
                    defaults.removeObject(forKey:  Common.DEFAULT_AUTO_NIGHT_MODE)
                }
            }
            break
        case 102:
            if !Common().checkNetworkConnect() {
                Common.showAlert(title: NSLocalizedString("error", comment: ""),
                                 message: NSLocalizedString("can_not_connect_itune", comment: ""),
                                 titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                    print("cancel")
                })
                defaults.set(true, forKey: Common.DEFAULT_SHOW_ADS)
                self.tableView.reloadData()
                break
            }
            if !isProVersion {
                self.showAlertUnlock()
            } else {
                self.settingControllers.isShowNewPost = swt.isOn
                if self.settingControllers.isShowNewPost == true {
                    defaults.set(true, forKey: Common.SHOW_NEWS_POST)
                }
                else
                {
                    defaults.removeObject(forKey: Common.SHOW_NEWS_POST)
                }
            }
            break
        case 200:
            self.settingControllers.isAutoPlayGIFPost = swt.isOn
            if self.settingControllers.isAutoPlayGIFPost == true {
                defaults.set(true, forKey: Common.AUTO_PLAY_GIF)
            }
            else
            {
                defaults.removeObject(forKey: Common.AUTO_PLAY_GIF)
            }
            break
        case 201:
            self.settingControllers.isShowSensitiveContent = swt.isOn
            if self.settingControllers.isShowSensitiveContent == true {
                defaults.set(true, forKey: Common.SENSITIVE_POST)
            }
            else
            {
                defaults.removeObject(forKey: Common.SENSITIVE_POST)
            }
            break
        case 202:
            self.settingControllers.isNightMode = swt.isOn
            if self.settingControllers.isNightMode == true {
                defaults.set(true, forKey: Common.NIGHT_MODE)
                AJRNight.sharedClient().isNight = true
                self.navigationController?.navigationBar.barTintColor = UIColor.black
                self.tableView.reloadData()
            }
                
            else
            {
                defaults.removeObject(forKey: Common.NIGHT_MODE)
                AJRNight.sharedClient().isNight = false
                self.navigationController?.navigationBar.barTintColor = UIColor.white
                self.tableView.reloadData()
            }
            break
        default:
            break
        }
        defaults.synchronize()
    }
    
    func getCellAtIndexPath(section : Int,index : Int ) -> UITableViewCell
    {
        let indexPath = IndexPath(item: index, section: section
        )
        let cell = self.tableView.cellForRow(at: indexPath)
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user_id = BaseDAO.getUserId()
        let secret_key = BaseDAO.getSecretKey()
        if isNotLogin {
            switch indexPath.section {
            case 0:
                switch indexPath.row {
                case 0:
                    break
                case 1:
                    break
                case 2:
                    break
                default:
                    break
                }
                break
            case 1:
                switch indexPath.row {
                case 0:
                    break
                case 1:
                    break
                case 2:
                    break
                case 3:
                    break
                default:
                    break
                }
                break
            case 2:
                switch indexPath.row {
                case 0:
                    if isNotLogin {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
                        vc.mainTabbar = self.mainTabbar
                        vc.isBackgroundBlack = true
                        let nav = UINavigationController(rootViewController: vc)
                        self.navigationController?.present(nav, animated: true, completion: nil)
                        self.userDefaults.set(true, forKey: Common.NOT_LOGIN_NOTIFY)
                    } else {
                        let profileNotificationTableView = self.storyboard?.instantiateViewController(withIdentifier: "profileNotificationTableViewIdentifier") as! ProfileNotificationTableView
                        self.show(profileNotificationTableView, sender: nil)
                    }
                    break
                default:
                    break
                }
                break
            case 3:
                switch indexPath.row {
                case 0:
                    let linkToShare = URL(string: NSLocalizedString("app_download_link", comment: ""))
                    var activityItems: [AnyObject]?
                    activityItems = [linkToShare as AnyObject]
                    let actionSheetShare = UIActivityViewController(activityItems: activityItems!, applicationActivities: nil)
                    self.present(actionSheetShare, animated: true, completion: nil)
                    break
                case 1:
                    let actionSheetSettings = UIAlertController(title: NSLocalizedString("feedback_send", comment: ""), message: nil, preferredStyle: .alert)
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_suggest", comment: ""), style: .default, handler: {(UIAlertAction)in
                        let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                        self.show(feedBackTableView, sender: nil)
                        print("press suggest")
                    }))
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_report", comment: ""), style: .default, handler: {(UIAlertAction)in
                        let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                        self.show(feedBackTableView, sender: nil)
                        print("press report")
                    }))
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_rate", comment: ""), style: .default, handler: {(UIAlertAction)in
                        // ratting
                        self.showRatingAlert()
                        print("press rate")
                    }))
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction)in
                        print("press cancel")
                    }))
                    self.present(actionSheetSettings, animated: true, completion: nil)
                    break
                case 2:
                    let whatNewsWebView = self.storyboard?.instantiateViewController(withIdentifier: "whatNewsWebViewIdentifier")  as! WhatNewsWebView
                    self.show(whatNewsWebView, sender: nil)
                    break
                default:
                    break
                }
                break
            case 4:
                switch indexPath.row {
                case 0:
                    let alertController = UIAlertController(title: NSLocalizedString("delete_caution", comment: ""), message: nil, preferredStyle: .actionSheet)
                    let actionClearCache = UIAlertAction(title: NSLocalizedString("clear_cache", comment: ""), style: .default, handler:  {(UIAlertAction) in
                        self.clearCache()
                        self.tableView.reloadData()
                        Common.showHubSuccess(string: NSLocalizedString("cache_clear", comment: ""))
//                        Common.showHubWithMessage(string: NSLocalizedString("cache_clear", comment: ""))
                        print("press delete cache")
                    })
                    actionClearCache.setValue(UIColor.red, forKey: "titleTextColor")
                    alertController.addAction(actionClearCache)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (action) in
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                default:
                    break
                }
            default:
                break
            }
        } else {
            switch indexPath.section {
            case 0:
                switch indexPath.row {
                case 0:
                    break
                case 1:
                    break
                case 2:
                    break
                default:
                    break
                }
                break
            case 1:
                switch indexPath.row {
                case 0:
                    break
                case 1:
                    break
                case 2:
                    break
                case 3:
                    break
                default:
                    break
                }
                break
            case 2:
                switch indexPath.row {
                case 0:
                    let profileEditTableView = self.storyboard?.instantiateViewController(withIdentifier: "editProfilesViewControllerIdentifier")  as! EditProfilesViewController
                    self.show(profileEditTableView, sender: true)
                    break
                case 1:
                    let profileChangPasswordTableView = self.storyboard?.instantiateViewController(withIdentifier: "profileChangPasswordTableViewIdentifier")  as! ProfileChangPasswordTableView
                    self.show(profileChangPasswordTableView, sender: nil)
                    break
                case 2:
                    break
                default:
                    break
                }
                break
            case 3:
                switch indexPath.row {
                case 0:
                    let profileNotificationTableView = self.storyboard?.instantiateViewController(withIdentifier: "profileNotificationTableViewIdentifier") as! ProfileNotificationTableView
                    self.show(profileNotificationTableView, sender: nil)
                    break
                default:
                    break
                }
                break
            case 4:
                switch indexPath.row {
                case 0:
                    let linkToShare = URL(string: NSLocalizedString("app_download_link", comment: ""))
                    var activityItems: [AnyObject]?
                    activityItems = [linkToShare as AnyObject]
                    let actionSheetShare = UIActivityViewController(activityItems: activityItems!, applicationActivities: nil)
                    self.present(actionSheetShare, animated: true, completion: nil)
                    break
                case 1:
                    let actionSheetSettings = UIAlertController(title: NSLocalizedString("feedback_send", comment: ""), message: nil, preferredStyle: .alert)
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_suggest", comment: ""), style: .default, handler: {(UIAlertAction)in
                        let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                        self.show(feedBackTableView, sender: nil)
                        print("press suggest")
                    }))
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_report", comment: ""), style: .default, handler: {(UIAlertAction)in
                        let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                        self.show(feedBackTableView, sender: nil)
                        print("press report")
                    }))
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_rate", comment: ""), style: .default, handler: {(UIAlertAction)in
                        // ratting
                        self.showRatingAlert()
                        print("press rate")
                    }))
                    actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction)in
                        print("press cancel")
                    }))
                    self.present(actionSheetSettings, animated: true, completion: nil)
                    break
                case 2:
                    let whatNewsWebView = self.storyboard?.instantiateViewController(withIdentifier: "whatNewsWebViewIdentifier")  as! WhatNewsWebView
                    self.show(whatNewsWebView, sender: nil)
                    break
                default:
                    break
                }
                break
            case 5:
                switch indexPath.row {
                case 0:
                    let alertController = UIAlertController(title: NSLocalizedString("delete_caution", comment: ""), message: nil, preferredStyle: .actionSheet)
                    let actionClearCache = UIAlertAction(title: NSLocalizedString("clear_cache", comment: ""), style: .default, handler:  {(UIAlertAction) in
                        self.clearCache()
                        self.tableView.reloadData()
                        print("press delete cache")
                    })
                    actionClearCache.setValue(UIColor.red, forKey: "titleTextColor")
                    alertController.addAction(actionClearCache)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (action) in
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    break
                case 1:
                    // MARK: - USER LOGOUT
                    let alertController = UIAlertController(title: NSLocalizedString("log_out_confirm", comment: ""), message: nil, preferredStyle: .actionSheet)
//                    let loginView : FBSDKLoginManager = FBSDKLoginManager()
                    let actionLogOut = UIAlertAction(title: NSLocalizedString("logout", comment: ""), style: .default, handler:  {(UIAlertAction) in
                        // FACEBOOK USER LOG OUT
                        if FBSDKAccessToken.current() != nil {
//                            FBSDKLoginManager().logOut()
//                            FBSDKAccessToken.setCurrent(nil)
//                            FBSDKProfile.setCurrent(nil)
//                            loginView.loginBehavior = FBSDKLoginBehavior.web
                        } else {
                            // GOOGLE USER LOG OUT
                            if GIDSignIn.sharedInstance().currentUser != nil {
                                GIDSignIn.sharedInstance().signOut()
                            }
                            // NORMAL USER LOG OUT
                            if BaseDAO.getUserId() != 0 && !BaseDAO.getSecretKey().isEmpty {
                                UserDAO.logout(user_id: String(user_id), secret_key: secret_key , completeHandle: {(success, data) in
                                    if data != nil {
                                        DispatchQueue.main.async{
                                            self.mainTabbar.switchLogin()
                                            if let bundle  = Bundle.main.bundleIdentifier {
                                                UserDefaults.standard.removePersistentDomain(forName: bundle)
                                            }
                                        }
                                    }
                                })
                            }
                            self.mainTabbar.switchLogin()
                        }
                        if let bundle  = Bundle.main.bundleIdentifier {
                            UserDefaults.standard.removePersistentDomain(forName: bundle)
                        }
                        self.mainTabbar.switchLogin()
                    })
                    actionLogOut.setValue(UIColor.red, forKey: "titleTextColor")
                    alertController.addAction(actionLogOut)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (action) in
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    break
                default:
                    break
                }
                break
            default:
                break
            }
        }
        
    }
    
    func showRatingAlert() {
        let ratingAlertViewController = UIAlertController(title: NSLocalizedString("rate_star_title", comment: ""), message: NSLocalizedString("rate_star_msg", comment: ""), preferredStyle: .alert)
        let ratingView = CosmosView()
        ratingView.frame = CGRect(x: 0, y: 0, width: ratingAlertViewController.view.frame.width - 40, height: 30)
//        let xCoord = ratingAlertViewController.view.frame.width/2 - 155
        let xCoord = ratingAlertViewController.view.frame.width / 2 - ratingView.frame.width / 3 - 15 //NamBS
        let yCoord = CGFloat(65.0)
        ratingView.rating = 0.0
        ratingView.settings.starSize = 30
        ratingView.settings.emptyBorderColor = UIColor.black
        ratingView.settings.updateOnTouch = true
        ratingView.frame.origin.x = xCoord
        ratingView.frame.origin.y = yCoord
        ratingAlertViewController.view.addSubview(ratingView)
        let cancelAction = UIAlertAction(title: NSLocalizedString("feedback_not_now", comment: ""), style: .cancel) { (action) in
        }
        ratingAlertViewController.addAction(cancelAction)
        self.present(ratingAlertViewController, animated: true, completion: nil)
        ratingView.didFinishTouchingCosmos = {
        rating in
            self.receiveRating()
            ratingAlertViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    func showFeedBackView() {
        let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
        self.show(feedBackTableView, sender: nil)
    }

    func setDataIfNotLogin() {
        section = [NSLocalizedString("fackju_pro", comment: ""),
                   NSLocalizedString("display_session", comment: ""),
                   NSLocalizedString("settings_session", comment: ""),
                   NSLocalizedString("others_session", comment: ""),
                   ""]
        
        footer = [NSLocalizedString("language_introduction", comment: "")]
        
        items = [[NSLocalizedString("show_ad", comment: ""),
                  NSLocalizedString("auto_night_mode", comment: ""),
                  NSLocalizedString("show_new_post", comment: "")],
                 [NSLocalizedString("auto_play_gif", comment: ""),
                  NSLocalizedString("show_sensitive", comment: ""),
                  NSLocalizedString("night_mode", comment: "")],
                 [NSLocalizedString("notification", comment: "")],
                 [NSLocalizedString("tell_app_to_others_user", comment: ""),
                  NSLocalizedString("provide_feedback", comment: ""),
                  NSLocalizedString("what_new", comment: "")],
                 [NSLocalizedString("clear_cache", comment: "")]]
        
    }
    func showAlertUnlock() {
        let alertController = UIAlertController.init(title:NSLocalizedString("unlock_pro_version", comment: ""), message: NSLocalizedString("get_pro_version_msg", comment: ""), preferredStyle: .alert)
        let btnUnlock = UIAlertAction.init(title: NSLocalizedString("unlock_pro_version_price", comment: ""), style: .default, handler:{
            (action) -> Void in
            self.showAlertLogin()
        })
        let  btnRestorePurchase = UIAlertAction.init(title: NSLocalizedString("restore_purchase", comment: ""), style: .default, handler: {
            (action) -> Void in
            print("btnRestorePurchase press")
        })
        let  btnCancel = UIAlertAction.init(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {
            (action) -> Void in
            self.defaults.set(false, forKey: Common.DEFAULT_SHOW_ADS)
            self.defaults.set(false, forKey:Common.DEFAULT_AUTO_NIGHT_MODE)
            self.defaults.set(false, forKey: Common.SHOW_NEWS_POST)
            self.tableView.reloadData()
            print("btnRestorePurchase press")
        })
        alertController.addAction(btnUnlock)
        alertController.addAction(btnRestorePurchase)
        alertController.addAction(btnCancel)
        
        self.present(alertController , animated: true, completion: nil)
    }
    func  showAlertLogin(){
        let title = NSLocalizedString("login_itune_store", comment: "")
        let account = NSLocalizedString("your_account", comment: "")
        let message = NSLocalizedString("enter_pass", comment: "") + account
        let password = "1"
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        var passwordTextField : UITextField?
        alertController.addTextField(configurationHandler: {(textField) -> Void in
            // Enter the textfiled customization code here.
            passwordTextField = textField
            passwordTextField?.placeholder = NSLocalizedString("password", comment: "")
            passwordTextField?.isSecureTextEntry = true
        })
        
        
        let ok = UIAlertAction.init(title: "OK", style: .default, handler: { (action) -> Void in
            print("OK Button Pressed")
            if passwordTextField?.text == password{
                print("success")
                self.showAlertConfirmPurchase()
            }
            else{
                print("fail")
                self.showAlertFail()
                
            }
            
        })
        let cancel = UIAlertAction.init(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(action) -> Void in
            print("Cancel Button pressed")
        })
        alertController.addAction(ok)
        alertController.addAction(cancel)
        self.present(alertController , animated: true, completion: nil)
    }
    
    func showAlertConfirmPurchase() {
        let title = "Xác nhận mục Mua In-App"
        let message = "Bạn có muốn mua Unlock 9Gag Pro với giá 69.000đ không?"
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let  btnCancel = UIAlertAction.init(title: "Huỷ", style: .default, handler: {
            (action) -> Void in
            print("showAlertConfirmPurchase btnCancel press")
        })
        let  btnPurchase = UIAlertAction.init(title: "Mua", style: .default, handler: {
            (action) -> Void in
            print("showAlertConfirmPurchase btnPurchase press")
        })
        alertController.addAction(btnCancel)
        alertController.addAction(btnPurchase)
        self.present(alertController , animated: true, completion: nil)
    }
    
    func showAlertFail() {
        let title = NSLocalizedString("incorrect_apple_id", comment: "")
        let message = ""
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let  btnCancel = UIAlertAction.init(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: {
            (action) -> Void in
            print("showAlertFail btnCancel press")
        })
        let  btnTryAgain = UIAlertAction.init(title: NSLocalizedString("incorrect_apple_id", comment: ""), style: .default, handler: {
            (action) -> Void in
            print("showAlertFail btnTryAgain press")
            self.showAlertLogin()
        })
        alertController.addAction(btnCancel)
        alertController.addAction(btnTryAgain)
        self.present(alertController , animated: true, completion: nil)
        
    }
    
    func receiveRating() {
        showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("fackju_pro", comment: ""))
    }
    
    func showAlertMessage(title: String, message: String) {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: NSLocalizedString("thank_rating", comment: ""))
        alert.show()
    }
    
    func clearCache() {
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        let fm = FileManager.default
//        let cacheURL = URL(string: cacheDirectory[0])
//        let fm = NSFileManager.defaultManager()
        do {
            let folderPath = cacheDirectory[0]
            let paths = try fm.contentsOfDirectory(atPath: folderPath)
            for path in paths
            {
                try fm.removeItem(atPath: "\(folderPath)/\(path)")
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
//        let enumerator = fileManager.enumeratorAtURL(cacheURL!, includingPropertiesForKeys: nil, options: nil, errorHandler: nil)
//        while let file = enumerator?.nextObject() as? String {
//            fileManager.removeItemAtURL(cacheURL.URLByAppendingPathComponent(file), error: nil)
//        }
        defaults.set("0 KB", forKey: Common.CACHE_SIZE)
    }
    
}
