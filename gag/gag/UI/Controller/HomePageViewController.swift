//
//  HomePageViewController.swift
//  gag
//
//  Created by buisinam on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

import HidingNavigationBar

class HomePageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var hidingNavBarManager: HidingNavigationBarManager?
    
    var currentIndex = 0
    var isProfile = false
    var isExplore = false
    var isSearch = false
    var searchData: String = ""
    var category_id: Int = 0
    var extendView: UIView!{
        didSet{
            if hidingNavBarManager != nil{
                hidingNavBarManager?.addExtensionView(extendView)
            }
        }
    }
    var parentVC: ParentHomeViewController!
    
    open var arrTitleSegment:[String]!
    private var arrHomeViewController = [HomeViewController]()
    private var arrNav: [HidingNavigationBarManager?]! = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        var max = 0
        if isProfile{
            max  = 4
        }else if isExplore {
            max = 2
        } else if isSearch {
            max = 1
        }
        else{
            max = 3
        }
        for var index in 0..<max{
            // Create a new view controller and pass suitable data.
            let pageContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewControllerIndentifier") as! HomeViewController
            pageContentViewController.isProfile = isProfile
            pageContentViewController.isExplore = isExplore
            pageContentViewController.category_id = category_id
            pageContentViewController.pageIndex = index
            pageContentViewController.isSearch = isSearch
            pageContentViewController.searchData = searchData
            pageContentViewController.parentVC = self
            pageContentViewController.hidingNavBarManager = hidingNavBarManager
            arrHomeViewController.append(pageContentViewController)
            arrNav.append(nil)
        }
        
        self.dataSource = self
        self.delegate = self
        self.setViewControllers([getViewControllerAtIndex(parentVC.selectedIndex)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        getViewControllerAtIndex(parentVC.selectedIndex).hidingNavBarManager = hidingNavBarManager
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isProfile{
            hidingNavBarManager?.viewWillAppear(false)
        }
        
        if !isSearch {
            if hidingNavBarManager == nil{
                hidingNavBarManager = HidingNavigationBarManager(viewController: self, scrollView: getViewControllerAtIndex(parentVC.selectedIndex).timeLineCollectionView)
                if extendView != nil{
                    hidingNavBarManager?.addExtensionView(extendView)
                }
               
                getViewControllerAtIndex(parentVC.selectedIndex).hidingNavBarManager = hidingNavBarManager
                arrNav.append(hidingNavBarManager!)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isProfile{
            hidingNavBarManager?.viewDidLayoutSubviews()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        hidingNavBarManager?.expand()
        super.viewWillDisappear(animated)
        if isProfile{
            hidingNavBarManager?.viewWillDisappear(animated)
        }
    }
    
    //// TableView datasoure and delegate
    
    func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        hidingNavBarManager?.shouldScrollToTop()
        
        return true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK:- UIPageViewControllerDataSource Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let pageContent: HomeViewController = viewController as! HomeViewController
        
        var index = pageContent.pageIndex
        
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        
        index -= 1;
        return getViewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let pageContent: HomeViewController = viewController as! HomeViewController
        
        var index = pageContent.pageIndex
        
        if (index == NSNotFound)
        {
            return nil;
        }
        
        index += 1;
        if (index == arrHomeViewController.count)
        {
            return nil;
        }
        let vc = getViewControllerAtIndex(index)
        return vc
    }
    
    // MARK:- Other Methods
    func getViewControllerAtIndex(_ index: NSInteger) -> HomeViewController
    {
        return arrHomeViewController[index]
    }
    
    //subview should implement
    open func segmentSelection(selection : Int) -> Void{
        if selection == currentIndex{
            currentIndex = selection
            return
        }
        else if selection > currentIndex
        {
            self.setViewControllers([getViewControllerAtIndex(selection)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        }
        else{
            self.setViewControllers([getViewControllerAtIndex(selection)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
        }
        let oldVC = getViewControllerAtIndex(currentIndex)
        oldVC.newViewPost.removeFromSuperview()
        
        let vc = getViewControllerAtIndex(selection)
        vc.collectionView.reloadData()
        setCurrentHeader(vc: vc)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        // If the page did not turn
        if (!completed)
        {
            // You do nothing because whatever page you thought
            // the book was on before the gesture started is still the correct page
            print("The page number did not change.")
            return;
        }
        
        // This is where you would know the page number changed and handle it appropriately
        print("The page number has changed.")
        if finished {
            let vc = pageViewController.viewControllers?.first as! HomeViewController
            parentVC.setSelectedSegment(index: vc.pageIndex)
            setCurrentHeader(vc: vc)
        }
        
        
    }
    
    func setCurrentHeader(vc: HomeViewController){
        if checkExistNav(vc: vc){
            hidingNavBarManager = arrNav[vc.pageIndex]
            if !isProfile {
                vc.hidingNavBarManager = hidingNavBarManager
                if vc.hasUpdate == true {
                    hidingNavBarManager?.addExtensionView(vc.newViewPost)
                }
                else{
                    hidingNavBarManager?.addExtensionView(UIView())
                }
            
            }
            if extendView != nil{
                hidingNavBarManager = vc.hidingNavBarManager
                hidingNavBarManager?.addExtensionView(extendView)
            }
            currentIndex = vc.pageIndex
            return
        }
        vc.timeLineCollectionView.tag = vc.pageIndex
        hidingNavBarManager = HidingNavigationBarManager(viewController: self, scrollView: vc.timeLineCollectionView)
        
        vc.hidingNavBarManager = hidingNavBarManager
        if extendView != nil{
            hidingNavBarManager?.addExtensionView(extendView)
            vc.hidingNavBarManager = hidingNavBarManager
        }
        
        arrNav[vc.pageIndex] = hidingNavBarManager
        currentIndex = vc.pageIndex
    }
    
    func checkExistNav(vc: HomeViewController)->Bool
    {
        if arrNav[vc.pageIndex] == nil{
            return false
        }
        return true
    }
    
}
