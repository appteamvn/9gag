//
//  CommentTableViewController.swift
//  gag
//
//  Created by buisinam on 12/25/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import FBAudienceNetwork
import Firebase
let CellIdentifier = "cell"

let MaxToolbarHeight: CGFloat = 180.0
let heightSendButton: CGFloat = 30.0
let widthSendButton: CGFloat = 60.0
import NSDate_TimeAgo
import Kingfisher
import TUSafariActivity
class CommentTableViewController: BaseTableViewController,UITextViewDelegate {

    var toolbar: UIToolbar!
    var sendButton: UIButton!
    var headerView: CommentHeaderView!
    open var post_id: String!
    private var type = "hot"
    var messages = [CommentResponse]()
    let textView = RDRGrowingTextView()
    var commentData: CommentResponse!
    let cornerRadius:CGFloat = 3.0
    var userDefaults = UserDefaults()
    let alert = UIAlertView()
    var isPostDetail = false
    var isNews = false
    var isComment = false
    
    var isPresented : Bool = false
    
    let settingControllers = SettingControllerSingleton.sharedInstance
    //Go from detail
    open var isDetail: Bool = false {
        didSet{
            self.configureDataSource()
        }
    }
    open var imgAvatar: UIImage!
    open var data: PostResponse!
    
    let commentModal = CommentModal()
    var tapGestureRecognizer: UITapGestureRecognizer! = nil

    var adView =  GADBannerView()

    // properties
    var cellWidth = 0.0
    var segmentHeigh = 45.0
    var minWidth = 30.0
    
    let homeApperence = SMSegmentAppearance()
    var homeSegment :SMSegmentView!
    var isActionSheetShowing = false
    
    var heightContrainst : [NSLayoutConstraint]!
//    var heightToolBarShow = 
    
    // MARK: - Overrides
    override var canBecomeFirstResponder: Bool{
        if isActionSheetShowing {
            return false
        } else {
            return true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.separatorStyle = .none
        let nib = UINib(nibName: "MessageCommentCell", bundle: nil)
        let subNib = UINib(nibName: "SubMessageCommentCell", bundle: nil)
        let postCell = UINib(nibName: "PostViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MessageCommentCell" )
        self.tableView.register(subNib, forCellReuseIdentifier: "SubMessageCommentCell")
        self.tableView.register(postCell, forCellReuseIdentifier: "PostViewCellIndentifier")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        
        let screenSize = UIScreen.main.bounds
        headerView = CommentHeaderView.init(frame: CGRect(x: 0, y: 0, width: 50, height: screenSize.size.width))
        self.configHeader()
        
        // Example's configuration
        commentModal.dataPost = data
        commentModal.controller = self
        
        if !isDetail{
            self.configureDataSource()
        } else if isNews{
            self.getPostDetail()
        }
        
        self.configureHeaderView()
        
        if isPresented{
            UIApplication.shared.statusBarStyle = .default
            
            let backButton = UIButton(frame: CGRect(x: 0, y: 0 , width: 35, height: 45))
            backButton.setImage(UIImage(named: "SVWebViewControllerBack"), for: .normal)
            // left-pointing shape!
            backButton.addTarget(self, action: #selector(self.back), for: .touchUpInside)
            
            // create button item -- possible because UIButton subclasses UIView!
            let backItem = UIBarButtonItem(image: UIImage(named: "SVWebViewControllerBack"), style: .plain, target: self, action: #selector(self.back))
            self.navigationItem.leftBarButtonItem = backItem
        }
    }

    func back(){
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.dismissDetail()
    }
    
    fileprivate func configureHeaderView() {
        
        
        //Ads Facebook
        self.adView = GADBannerView(adSize: kGADAdSizeBanner)
        
        adView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        
        adView.adUnitID = "ca-app-pub-3861856777735024/2293198342"
        adView.rootViewController = self
        adView.load(GADRequest())
        
        adView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.tableHeaderView = adView
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func refreshCall() {
        if isNews {
            getPostDetail()
        } else {
            configureDataSource()
        }
    }
    
    // MARK: - Configuration
    
    func configureDataSource() {
        CommentDAO.getListPostComment(post_id: post_id, type: type) { (success, results:[CommentResponse]?) in
            if success{
                self.messages = []
                if let data:[CommentResponse] = results {
                    self.messages = data
                    self.messages.sort(by: { (item1, item2) -> Bool in
                        return item1.comment_date_unix > item2.comment_date_unix
                    })
                    
                    var i = 0

                    while (i < self.messages.count)
                    {
                        let item = self.messages[i]
                        if item.comment_children.count > 0{
                            let childrens = item.comment_children
                            let children = childrens[0]
                            children.is_last = true
                            if childrens.count == 1
                            {
                                children.is_last = false
                            }
                            self.messages.insert(children, at: i+1)
                            i += 1
                        }
                        i += 1
                    }
                    
                    self.reloadHeader()
//                    self.isComment = true
                    self.tableView.reloadData()
                }
            }
            self.refControl.endRefreshing()
        }
        
    }
    
    func getPostDetail() {
        PostDAO.getPostDetail(post_id: post_id) { (success, results) in
            if success {
                self.data = results!
//                self.isComment = true
                self.tableView.reloadData()
            }
            self.refControl.endRefreshing()
        }
    }
    
    func configHeader(){
        
        homeApperence.segmentOnSelectionColour = UIColor.clear
        homeApperence.titleOnSelectionFont = UIFont.boldSystemFont(ofSize: 15.0)
        homeApperence.titleOnSelectionColour = Common.blueColor()
        homeApperence.titleOffSelectionColour = UIColor.gray
        homeApperence.segmentOffSelectionColour = UIColor.clear
        homeApperence.titleOnSelectionFont = UIFont.systemFont(ofSize: 13.0)
        homeApperence.titleOffSelectionFont = UIFont.systemFont(ofSize: 13.0)
        homeApperence.contentVerticalMargin = 10.0
        
        homeSegment = SMSegmentView(frame: CGRect(x: 0, y: (headerView.segmentView.frame.size.height) - CGFloat(segmentHeigh), width: (headerView.segmentView.frame.size.width), height: CGFloat(segmentHeigh)), dividerColour: UIColor(white: 0.95, alpha: 0.3), dividerWidth: 0.0, segmentAppearance: homeApperence)
        homeSegment.equalAll = true
        homeSegment.backgroundColor = .clear
        
        homeSegment.addSegmentWithTitle(NSLocalizedString("hot", comment: ""), onSelectionImage: nil, offSelectionImage: nil)
        homeSegment.addSegmentWithTitle(NSLocalizedString("fresh", comment: ""), onSelectionImage: nil, offSelectionImage: nil)
        
        homeSegment.selectedSegmentIndex = 0
        homeSegment.addTarget(self, action: #selector(homeSegmentAction(sender:)), for: .valueChanged)
        
        headerView.segmentView.addSubview(self.homeSegment)
        
    }
    
    //MARK: SegmentView setup
    func homeSegmentAction(sender: SMSegmentView) -> Void {
//        segmentSelection(selection: sender.selectedSegmentIndex)
        self.type = (sender.selectedSegment?.title?.lowercased())!
        self.configureDataSource()

    }

    
    func reloadHeader(){
        headerView.lblNumber.text = "\(messages.count)" + NSLocalizedString("comment", comment: "")
        if isPostDetail {
            self.navigationItem.title = NSLocalizedString("post", comment: "")
            let saveItem = UIBarButtonItem(title: NSLocalizedString("share", comment: ""), style: .plain, target: self, action: #selector(shareBarItemButtonAction))
            self.navigationItem.rightBarButtonItem = saveItem
        } else {
            self.navigationItem.title = String(format: "%i %@", messages.count, NSLocalizedString("comments", comment: ""))
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if isDetail{
            return 2
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            if isDetail && data != nil{
                return 1
            }
            return self.messages.count
        }
        else{
            return self.messages.count
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        return .none
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDetail && data != nil{
            if indexPath.section == 0{
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "PostViewCellIndentifier") as! PostViewCell
                
                cell.postLikeButton.layer.cornerRadius = self.cornerRadius
                cell.postLikeButton.layer.borderWidth = 0.5
                cell.postLikeButton.layer.borderColor = UIColor.lightGray.cgColor
                cell.postUnlikeButton.layer.cornerRadius = self.cornerRadius
                cell.postUnlikeButton.layer.borderWidth = 0.5
                cell.postUnlikeButton.layer.borderColor = UIColor.lightGray.cgColor
                cell.postCommentButton.layer.cornerRadius = self.cornerRadius
                cell.postCommentButton.layer.borderWidth = 0.5
                cell.postCommentButton.layer.borderColor = UIColor.lightGray.cgColor
                cell.postShareButton.layer.cornerRadius = self.cornerRadius
                cell.postShareButton.setTitle( NSLocalizedString("share", comment: ""), for: .normal)
                cell.postImage.tag = indexPath.row
                
                cell.postLikeButton.tag = indexPath.row
                cell.postUnlikeButton.tag = indexPath.row
                cell.postCommentButton.tag = indexPath.row
                cell.postShareButton.tag = indexPath.row
                cell.postMoreButton.tag = indexPath.row
                cell.btnGift.tag = indexPath.row

                cell.postLikeButton.addTarget(commentModal, action: #selector(commentModal.voteNormalAction(sender:)), for: .touchUpInside)
                cell.postUnlikeButton.addTarget(commentModal, action: #selector(commentModal.voteUnNormalAction(sender:)), for: .touchUpInside)
                cell.postCommentButton.addTarget(commentModal, action: #selector(commentModal.commentNormalAction(sender:)), for: .touchUpInside)
                cell.postShareButton.addTarget(commentModal, action: #selector(commentModal.shareActionNormal(sender:)), for: .touchUpInside)
                cell.postMoreButton.addTarget(commentModal, action: #selector(commentModal.moreAction(sender:)), for: .touchUpInside)
                cell.btnGift.addTarget(self, action: #selector(playGif(sender:)), for: .touchUpInside)
                tapGestureRecognizer = UITapGestureRecognizer(target:commentModal, action:#selector(commentModal.tappedOnPostImage(sender:)))
                tapGestureRecognizer.numberOfTapsRequired  = 1
                cell.postImage.isUserInteractionEnabled = true
                cell.postImage.addGestureRecognizer(tapGestureRecognizer)
                if (isNews || isDetail) && isComment == false {
                    if data.url.contains(".mp4") {
                        cell.videoView.isHidden = false
                        cell.btnGift.isHidden = false
//                        let ratio = CGFloat(data.api_width) / self.tableView.frame.size.width
//                        cell.heightContraint.constant = CGFloat(data.api_height) / ratio
                        cell.videoView.url = NSURL(string: data.url)!
                        cell.contentView.bringSubview(toFront: cell.videoView)
                        cell.contentView.bringSubview(toFront: cell.btnGift)
                        cell.btnGift.setImage(UIImage(named: "gif"), for: .normal)
                        if settingControllers.isAutoPlayGIFPost == true {
                            cell.btnGift.setImage(nil, for: UIControlState.normal)
                            cell.videoView.PlayVideo()
                        }
                    } else {
                        cell.videoView.isHidden = true
                        cell.btnGift.isHidden = true
                        cell.postImage.kf.setImage(with: ImageResource(downloadURL: URL(string: data.url)!,cacheKey:nil))
                        cell.contentView.bringSubview(toFront: cell.postImage)
                    }
                    let ratio = CGFloat(data.api_width) / self.tableView.frame.size.width
                    if ratio > 0.0{
                        cell.heightContraint.constant = CGFloat(data.api_height) / ratio
                    }
                    cell.postPoints.text = "\(data.post_point)" + NSLocalizedString("points", comment: "") + data.post_comment + NSLocalizedString("comment", comment: "")
                } else {
                    cell.postImage.image = self.imgAvatar
                }
                cell.postTitle.text =  String(format: "%@", self.data.post_title)
                cell.postUnlikeButton.tintColor = Common.lighGrayColor()
                cell.postCommentButton.tintColor = Common.lighGrayColor()
                // post_link
                let postLink = String(format: "%@", self.data.post_link)
                cell.postLink = postLink
                // btnVote
                if data.is_vote == 0{
                    cell.postLikeButton.tintColor = Common.lighGrayColor()
                    cell.postUnlikeButton.tintColor = Common.lighGrayColor()
                }else if data.is_vote == -1{
                    cell.postUnlikeButton.tintColor = Common.blueColor()
                    cell.postLikeButton.tintColor = Common.lighGrayColor()
                }else {
                    cell.postLikeButton.tintColor = Common.blueColor()
                    cell.postUnlikeButton.tintColor = Common.lighGrayColor()
                }
                commentModal.postCell = cell
                return cell
            }
            else{
                
                let message = self.messages[(indexPath as NSIndexPath).row]
                if message.is_subcomment{
                    return self.subMessageCellForRowAtIndexPath(indexPath)
                }
                else{
                    return self.messageCellForRowAtIndexPath(indexPath)
                }
            }
        }
        else{
            let message = self.messages[(indexPath as NSIndexPath).row]
            if message.is_subcomment{
                return self.subMessageCellForRowAtIndexPath(indexPath)
            }
            else{
                return self.messageCellForRowAtIndexPath(indexPath)
            }
        }
    }
    
    func playGif(sender: UIButton)
    {
        let cell = self.getCellAtIndexPathForGif(index: sender.tag)
        if (cell?.videoView.IsPlaying())!
        {
            cell?.videoView.PauseVideo()
            sender.setImage(UIImage(named: "gif"), for: .normal)
        }else{
            cell?.videoView.PlayVideo()
            sender.setImage(nil, for: .normal)
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.messages.count > 0
        {
            if isDetail{
                if section == 0
                {
                    return 0
                }
                else{
                    return 50
                }
            }
            else{
                return 50
            }
        }else{
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.messages.count > 0
        {
            if isDetail{
                if section == 0
                {
                    return nil
                }
                else{
                    return headerView
                }
            }
            else{
                return headerView
            }
        }else{
            return nil
        }
    }
    
    func subMessageCellForRowAtIndexPath(_ indexPath: IndexPath) -> SubMessageCommentCell{
        let message = self.messages[(indexPath as NSIndexPath).row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SubMessageCommentCell") as! SubMessageCommentCell
        
        // Assign Data Super Comment Cell
        if let url: URL = URL(string: message.comment_author.author_avatar){
            cell.imgAvatar.kf.setImage(with: ImageResource(downloadURL: url,cacheKey: nil))
        }
        cell.imgAvatar.layer.masksToBounds = true
        cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.bounds.size.width / 2
        cell.lblName.text = message.comment_author.author_name
        cell.lblOP.text = NSLocalizedString("op", comment: "")
        cell.lblPoint.text = "\(message.comment_voted)" + NSLocalizedString("points", comment: "")
        cell.lblContent.text = message.comment_content
        cell.btnReply.tag = indexPath.row
        cell.btnReply.setTitle(NSLocalizedString("reply", comment: ""), for: .normal)
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        cell.transform = self.tableView.transform
        
        if message.comment_voted == 0{
            cell.btnLike.tintColor = Common.lighGrayColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
        }else if message.comment_voted == -1{
            cell.btnUnlike.tintColor = Common.blueColor()
            cell.btnLike.tintColor = Common.lighGrayColor()
        }else {
            cell.btnLike.tintColor = Common.blueColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
        }
        
        cell.btnLike.tag = indexPath.row
        cell.btnUnlike.tag = indexPath.row
        cell.btnMore.tag = indexPath.row
        cell.btnViewMore.tag = indexPath.row
        cell.btnViewMore.setTitle(NSLocalizedString("view_previous_reply", comment: ""), for: .normal)
        cell.btnLike.addTarget(self, action: #selector(likeCommentAction(sender:)), for: .touchUpInside)
        cell.btnUnlike.addTarget(self, action: #selector(unLikeCommentAction(sender:)), for: .touchUpInside)
        cell.btnReply.addTarget(self, action: #selector(replyComment(sender:)), for: .touchUpInside)
        cell.btnMore.addTarget(self, action: #selector(moreCommentAction(sender:)), for: .touchUpInside)
        cell.btnViewMore.addTarget(self, action: #selector(loadMoreComment(sender:)), for: .touchUpInside)
        if message.is_last == true {
            cell.heighLblMore.constant = 30
        }
        else{
            cell.heighLblMore.constant = 0
        }
        return cell
    }
    
    func messageCellForRowAtIndexPath(_ indexPath: IndexPath) -> MessageCommentCell{
        let message = self.messages[(indexPath as NSIndexPath).row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MessageCommentCell") as! MessageCommentCell
        
        // Assign Data Super Comment Cell
        if let url: URL = URL(string: message.comment_author.author_avatar){
            cell.imgAvatar.kf.setImage(with: ImageResource(downloadURL: url,cacheKey: nil))
        }
        
        cell.imgAvatar.layer.masksToBounds = true
        cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.bounds.size.width / 2
        cell.lblName.text = message.comment_author.author_name
        cell.lblOP.text = NSLocalizedString("op", comment: "")
        cell.lblPoint.text = "\(message.comment_voted)" + NSLocalizedString("points", comment: "")
        cell.lblContent.text = message.comment_content
        cell.btnReply.tag = indexPath.row
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        cell.transform = self.tableView.transform
        
        if message.comment_voted == 0{
            cell.btnLike.tintColor = Common.lighGrayColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
        }else if message.comment_voted == -1{
            cell.btnUnlike.tintColor = Common.blueColor()
            cell.btnLike.tintColor = Common.lighGrayColor()
        }else {
            cell.btnLike.tintColor = Common.blueColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
        }
        
        cell.btnLike.tag = indexPath.row
        cell.btnUnlike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(likeCommentAction(sender:)), for: .touchUpInside)
        cell.btnUnlike.addTarget(self, action: #selector(unLikeCommentAction(sender:)), for: .touchUpInside)
        cell.btnReply.addTarget(self, action: #selector(replyComment(sender:)), for: .touchUpInside)
        cell.btnMore.addTarget(self, action: #selector(moreCommentAction(sender:)), for: .touchUpInside)
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - View lifecycle
    
    override func loadView() {
        super.loadView()
//        self.title = "RDRGrowingTextView"
//        self.tableView.tableFooterView! = UIView()
        self.tableView.keyboardDismissMode = .interactive
//        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        let nib = UINib(nibName: "MessageCommentCell", bundle: nil)
        let subNib = UINib(nibName: "SubMessageCommentCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MessageCommentCell" )
        self.tableView.register(subNib, forCellReuseIdentifier: "SubMessageCommentCell")
    }
    
    func loadMoreComment(sender: AnyObject)
    {
        var section = 0
        if isDetail{
            section = 1
        }
        var tag = (sender as! UIButton).tag
        let currentData =  messages[tag]
        currentData.is_last = false
        let indexParent = findParent(from: tag)
        let data =  messages[indexParent]
        
        self.messages.remove(at: tag)
        self.tableView.deleteRows(at: [IndexPath(row: tag, section: section)], with: .top)
        self.messages.insert(contentsOf: data.comment_children, at: tag)
        
        var rowUpdate = [IndexPath]()
        for i in 0..<data.comment_children.count{
            rowUpdate.append(IndexPath(row: tag, section: section))
            tag += 1
        }
        self.tableView.insertRows(at: rowUpdate, with: .bottom)
        
       
    }
    
    func findParent(from: Int) -> Int{
        var start = from
        var index = -1
        while(index == -1)
        {
            start -= 1
            let data =  messages[start]
            if data.is_subcomment == false{
                index = start
            }
        }
        return index
    }
    
    override var inputAccessoryView: UIView?{
        if toolbar != nil {
            return toolbar
        }
        let screenSize = UIScreen.main.bounds
        self.toolbar = UIToolbar()
        self.toolbar.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: 84)
        self.toolbar.isTranslucent = true
        self.toolbar.backgroundColor = .white
        
        textView.font = UIFont.systemFont(ofSize: CGFloat(15.0))
        textView.textContainerInset = UIEdgeInsetsMake(5.0, 3.0, 5.0, 3.0)
        textView.layer.cornerRadius = 1.0
        textView.layer.borderColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 205.0 / 255.0, alpha: 1.0).cgColor
        textView.layer.borderWidth = 0.3
        textView.layer.masksToBounds = true
        textView.placeholder = NSLocalizedString("write_a_comment", comment: "")
        toolbar.addSubview(textView)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.delegate = self
        
        //toolbar autolayout
        self.toolbar.translatesAutoresizingMaskIntoConstraints = false
        
        
        textView.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: .horizontal)
        textView.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: .vertical)
        toolbar.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: .vertical)
        if heightContrainst == nil{
            
            heightContrainst = NSLayoutConstraint.constraints(withVisualFormat: "V:[textView]-15-|", options: [], metrics: nil, views: ["textView":textView])
        }
        toolbar.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[textView]-8-|", options: [], metrics: nil, views: ["textView":textView]))
        toolbar.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[textView]", options: [], metrics: nil, views: ["textView":textView]))
        toolbar.addConstraints(heightContrainst)
        toolbar.addConstraint(NSLayoutConstraint(item: toolbar, attribute: .height, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: MaxToolbarHeight))
        
        //send autolayout
        self.sendButton = UIButton()
        self.sendButton.frame = CGRect(x: 0, y: 0, width: widthSendButton, height: heightSendButton)
        toolbar.addSubview(sendButton)
        self.sendButton.translatesAutoresizingMaskIntoConstraints = false
        toolbar.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[sendButton(60)]-8-|", options: [], metrics: nil, views: ["sendButton":sendButton]))
        toolbar.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[textView]-8-[sendButton(33)]-8-|", options: [], metrics: nil, views: ["textView":textView,"sendButton":sendButton]))
//        toolbar.addConstraint(NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: sendButton, attribute: .top, multiplier: 1.0, constant: 8))
        
        self.sendButton.setTitle("Send", for: .normal)
        self.sendButton.backgroundColor = Common.blueColor()
        self.sendButton.layer.masksToBounds =  true
        self.sendButton.layer.cornerRadius = 3
        self.sendButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        sendButton.isHidden = true
        return toolbar
    }
    
    func getCellAtIndexPath(index : Int) -> UITableViewCell
    {
        var section  = 0
        if isDetail{
            section = 1
        }
        let indexPath = IndexPath(item: index, section: section)
        let cell =  self.tableView.cellForRow(at: indexPath)
        return cell!
    }
    
    func getCellAtIndexPathForGif(index : Int) -> PostViewCell?
    {
        let indexPath = IndexPath(item: index, section: 0)
        let cell =  tableView.cellForRow(at: indexPath)
        if cell == nil {return PostViewCell()}
        return cell as! PostViewCell
    }
    
    func likeCommentAction(sender: Any) {
        if BaseDAO.checkLogin() {
            let dataResult = self.messages[(sender as AnyObject).tag] as CommentResponse
            
            let cellRow = getCellAtIndexPath(index: (sender as AnyObject).tag)
            var cell: UITableViewCell?
            if (cellRow.isKind(of: SubMessageCommentCell.self)){
                cell = cellRow as! SubMessageCommentCell
                processLike(dataResult: dataResult, cell: cell as! MessageCommentCell?)
            }
            else{
                cell = cellRow as! MessageCommentCell
                processLike(dataResult: dataResult, cell: cell as! MessageCommentCell?)
            }
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func processLike(dataResult: CommentResponse, cell:MessageCommentCell?)
    {
        switch dataResult.comment_voted {
        case 0:
            dataResult.comment_point += 1
            cell?.btnLike.tintColor = Common.blueColor()
            cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
            cell?.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
                }
            }
            break
        case 1:
            dataResult.comment_point -= 1
            cell?.btnLike.tintColor = Common.lighGrayColor()
            cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
            cell?.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 0
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
                }
            }
            break
        case -1:
            dataResult.comment_point += 2
            cell?.btnLike.tintColor = Common.blueColor()
            cell?.btnUnlike.tintColor = Common.lighGrayColor()
            cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
            cell?.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
                }
            }
            break
        default:
            break
        }
    }
    
    func unLikeCommentAction(sender: Any) {
        if BaseDAO.checkLogin() {
            let dataResult = self.messages[(sender as AnyObject).tag] as CommentResponse
            
            let cellRow = getCellAtIndexPath(index: (sender as AnyObject).tag)
            var cell: UITableViewCell?
            if (cellRow.isKind(of: SubMessageCommentCell.self)){
                cell = cellRow as! SubMessageCommentCell
                processUnlike(dataResult: dataResult, cell: cell as! MessageCommentCell?)
            }
            else{
                cell = cellRow as! MessageCommentCell
                processUnlike(dataResult: dataResult, cell: cell as! MessageCommentCell?)
            }
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func processUnlike(dataResult: CommentResponse, cell:MessageCommentCell?){
        switch dataResult.comment_voted {
        case 0:
            dataResult.comment_point -= 1
            cell?.btnUnlike.tintColor = Common.blueColor()
            cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
            cell?.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = -1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
                }
            }
            break
        case 1:
            dataResult.comment_point -= 2
            cell?.btnUnlike.tintColor = Common.blueColor()
            cell?.btnLike.tintColor = Common.lighGrayColor()
            cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
            cell?.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = -1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
                }
            }
            break
        case -1:
            dataResult.comment_point += 1
            cell?.btnUnlike.tintColor = Common.lighGrayColor()
            cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
            cell?.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 0
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell?.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("points", comment: "")
                }
            }
            
            break
        default:
            break
        }

    }
    
    func replyComment(sender: Any)
    {
        if BaseDAO.checkLogin() {
            self.textView.becomeFirstResponder()
            let data = self.messages[(sender as! UIButton).tag]
            self.textView.text = "@" + data.comment_author.author_name
            commentData = data
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func moreCommentAction(sender: Any) {
        // show action sheet
        let dataResult = self.messages[(sender as AnyObject).tag] as CommentResponse
        let cellRow = getCellAtIndexPath(index: (sender as AnyObject).tag)
        
        if (cellRow.isKind(of: SubMessageCommentCell.self)){
            let cell = cellRow as! SubMessageCommentCell
            self.showActionSheetMoreAction(dataResult: dataResult, cell:cell)
        }
        else{
            let cell = cellRow as! MessageCommentCell
            self.showActionSheetMoreAction(dataResult: dataResult, cell:cell)
        }
    }
    
    func sendAction(){
        var comment_parent = "0"
        if self.textView.text.range(of: "@") != nil {
            comment_parent = commentData.comment_id
        }
        else{
            comment_parent = "0"
        }
        CommentDAO.commentPost(post_id: post_id, comment_parent: comment_parent, comment_content: self.textView.text){ (success) in
            if success{
                self.configureDataSource()
            }
        }

        self.textView.text = ""
        self.textView.resignFirstResponder()
    }
    
    //MARK: -TextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if BaseDAO.checkLogin() {
            heightContrainst[0].constant = 45
            toolbar.layoutIfNeeded()
            sendButton.isHidden = false
        } else {
            self.textView.resignFirstResponder()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        heightContrainst[0].constant = 15
        toolbar.layoutIfNeeded()
        sendButton.isHidden = true
    }
    func showActionSheetMoreAction(dataResult: CommentResponse, cell:MessageCommentCell?) {
        let actionSheetMore = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        let userNameInCell = cell?.lblName.text
//        let titleGoTo = String(format: "%@ %@", NSLocalizedString("go_to", comment: ""), userNameInCell!)
//        let actionGoToUser = UIAlertAction(title: titleGoTo , style: .default, handler:  {(UIAlertAction) in
//            let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
//            parentHomeViewController.isProfile = true
//            self.show(parentHomeViewController, sender: nil)
//            print("press Go to")
//        })
//        actionSheetMore.addAction(actionGoToUser)
        actionSheetMore.addAction(UIAlertAction(title: NSLocalizedString("copy_cmt", comment: ""), style: .default, handler: {(UIAlertAction)in
            print("press Copy Comment")
            UIPasteboard.general.string = dataResult.comment_content
            self.textView.isHidden = false
        }))
        let actionDeleteComment = UIAlertAction(title: NSLocalizedString("delete_cmt", comment: ""), style: .default, handler:  {(UIAlertAction) in
                self.processDeleteComment(dataResult: dataResult, cell: cell)
            print("press delete Comment")
        })
        actionDeleteComment.setValue(UIColor.red, forKey: "titleTextColor")
        actionSheetMore.addAction(actionDeleteComment)
        actionSheetMore.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction)in
            print("press cancel")
            self.textView.isHidden = false
        }))
        self.present(actionSheetMore, animated: true, completion: {
            self.textView.isHidden = true
            self.view.endEditing(true)
            self.isActionSheetShowing = true
        })
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
        
        }
        
    }
    
    func processDeleteComment(dataResult: CommentResponse, cell:MessageCommentCell?) {
        if BaseDAO.checkLogin() {
            CommentDAO.userDeleteComment(user_id: String(BaseDAO.getUserId()),
                                         comment_id: dataResult.comment_id,
                                         secret_key: BaseDAO.getSecretKey()) { (success: Bool) in
                                            if success {
                                                self.messages.remove(at: (cell?.tag)!)
                                                self.tableView.reloadData()
                                                self.alert.title = NSLocalizedString("comment_deleted", comment: "")
                                                self.alert.message = NSLocalizedString("comment_has_deleted", comment: "")
                                                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                                                self.alert.show()
                                                print("delete comment success")
                                                self.textView.isHidden = false
                                            } else {
                                                self.alert.title = NSLocalizedString("comment_deleted", comment: "")
                                                self.alert.message = NSLocalizedString("comment_delete_fail", comment: "")
                                                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                                                self.alert.show()
                                            }
            }
            
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            vc.isBackgroundBlack = true
            let nav = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let vc = segue.destination as? ParentHomeViewController {
            vc.isProfile = true
        }
    }
    
    func shareBarItemButtonAction()  {
        let linkToShare = URL(string: self.data.post_link)
        let imgToShare = self.imgAvatar
        var activityItems: [AnyObject]?
        let activity = TUSafariActivity()
        if imgToShare == nil {return}
        activityItems = [linkToShare as AnyObject , imgToShare!]
        let actionSheetShare = UIActivityViewController(activityItems: activityItems!, applicationActivities: [activity])
        self.present(actionSheetShare, animated: true, completion: nil)
        
    }
}
