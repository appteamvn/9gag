//
//  HomePageViewController.swift
//  gag
//
//  Created by buisinam on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import HidingNavigationBar

class VideosPageViewController: UIPageViewController, UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    private var arrHomeViewController = [VideosViewController]()
    private var arrNav: [HidingNavigationBarManager?]! = []
    var hidingNavBarManager: HidingNavigationBarManager?
    var currentIndex = 0
    var parentVC: ParentVideosViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        for var index in 0..<(1 + VideoDao.VideoDaoSingleton.sharedInstance.categoryVideo.count){
            // Create a new view controller and pass suitable data.
            let pageContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "VideosViewControllerIndentifier") as! VideosViewController
            
            pageContentViewController.pageIndex = index
            
            arrHomeViewController.append(pageContentViewController)
            arrNav.append(nil)
        }
        
        self.dataSource = self
        self.delegate = self
        self.setViewControllers([getViewControllerAtIndex(0)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if hidingNavBarManager == nil{
            hidingNavBarManager = HidingNavigationBarManager(viewController: self, scrollView: getViewControllerAtIndex(0).collectionView)
            arrNav.append(hidingNavBarManager!)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        hidingNavBarManager?.shouldScrollToTop()
        
        return true
    }
    
    // MARK:- UIPageViewControllerDataSource Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let pageContent: VideosViewController = viewController as! VideosViewController
        
        var index = pageContent.pageIndex
        
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        
        index -= 1;
        return getViewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let pageContent: VideosViewController = viewController as! VideosViewController
        
        var index = pageContent.pageIndex
        
        if (index == NSNotFound)
        {
            return nil;
        }
        
        index += 1;
        if (index == arrHomeViewController.count)
        {
            return nil;
        }
        return getViewControllerAtIndex(index)
    }
    
    // MARK:- Other Methods
    func getViewControllerAtIndex(_ index: NSInteger) -> VideosViewController
    {
        return arrHomeViewController[index]
    }
    
    
    //subview should implement
    open func segmentSelection(selection : Int) -> Void{
        if selection == currentIndex{
            currentIndex = selection
            return
        }
        else if selection > currentIndex
        {
            self.setViewControllers([getViewControllerAtIndex(selection)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        }
        else{
            self.setViewControllers([getViewControllerAtIndex(selection)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
        }
        setCurrentHeader(vc: getViewControllerAtIndex(selection))
        currentIndex = selection
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        // If the page did not turn
        if (!completed)
        {
            // You do nothing because whatever page you thought
            // the book was on before the gesture started is still the correct page
            print("The page number did not change.")
            return;
        }
        
        // This is where you would know the page number changed and handle it appropriately
        print("The page number has changed.")
        if finished {
            let vc = pageViewController.viewControllers?.first as! VideosViewController
            parentVC?.setSelectedSegment(index: vc.pageIndex)
            setCurrentHeader(vc: vc)
        }
        
        
    }
    
    func setCurrentHeader(vc: VideosViewController){
        if checkExistNav(vc: vc){
            hidingNavBarManager = arrNav[vc.pageIndex]
            return
        }
        vc.collectionView.tag = vc.pageIndex
        hidingNavBarManager = HidingNavigationBarManager(viewController: self, scrollView: vc.collectionView)
        
        arrNav[vc.pageIndex] = hidingNavBarManager
    }
    
    func checkExistNav(vc: VideosViewController)->Bool
    {
        if arrNav[vc.pageIndex] ==  nil{
            return false
        }
        return true
    }

}
