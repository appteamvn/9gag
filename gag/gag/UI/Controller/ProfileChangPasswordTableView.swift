//
//  ProfileChangPasswordTableView.swift
//  gag
//
//  Created by ThanhToa on 11/22/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileChangPasswordTableView: UITableViewController, UITextViewDelegate {
    let section = [" "," "]
    let items = [[NSLocalizedString("current_password", comment: "")],
                 [NSLocalizedString("new_password", comment: ""),
                  NSLocalizedString("confirm_new_password", comment: "")]]
    let itemsPictures = [["ic_lock_closed_black_16"],["ic_lock_closed_black_16", "ic_lock_closed_black_16"]]
    var userDefaults = UserDefaults()
    let alert = UIAlertView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = nil
        let saveItem = UIBarButtonItem(title: NSLocalizedString("save", comment: ""), style: .plain, target: self, action: #selector(saveChange))
        self.navigationItem.rightBarButtonItem = saveItem
        self.navigationItem.title = NSLocalizedString("change_password", comment: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.section.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.items [section].count
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = UIColor.lightGray
        headerView.textLabel?.font = UIFont.systemFont(ofSize: 14)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "changPasswordCustomCell", for: indexPath) as! ProfileChangPassewordCell
        cell.passwordInput.placeholder = self.items[indexPath.section][indexPath.row]
        cell.passwordIcon.image = UIImage(named: self.itemsPictures[indexPath.section][indexPath.row])
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                self.dismiss(animated: true, completion: nil)
                break
            default:
                break
            }
            break
        case 1:
            switch indexPath.row {
            case 0:
                self.dismiss(animated: true, completion: nil)
                break
            case 1:
                self.dismiss(animated: true, completion: nil)
                break
            default:
                break
            }
            break
        default:
            break
        }
    }
    
    func saveChange() {
        let cell1 = getCellAtIndexPath(section: 0, index: 0) as! ProfileChangPassewordCell
        let cell2 = getCellAtIndexPath(section: 1, index: 0) as! ProfileChangPassewordCell
        let cell3 = getCellAtIndexPath(section: 1, index: 1) as! ProfileChangPassewordCell
        let currentPass = cell1.passwordInput.text //  ? equal with server
        let newPass = cell2.passwordInput.text
        let confirmPass = cell3.passwordInput.text
        if userDefaults.object(forKey: Common.USER_PASSWORD) == nil {
            self.alert.title = NSLocalizedString("app_name", comment: "")
            self.alert.message = NSLocalizedString("not_set_password", comment: "")
            self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
            self.alert.show()
        return
        }
        let user_password = userDefaults.object(forKey: Common.USER_PASSWORD) as! String
        if BaseDAO.getUserId() != 0 &&
            !(newPass?.isEmpty)! &&
            newPass == confirmPass &&
            currentPass == user_password {
            if !Common().checkNetworkConnect() {
                Common.showHubWithMessage(string: NSLocalizedString("error", comment: ""))
                return
            }
            let user_id = BaseDAO.getUserId()
            let secret_key = BaseDAO.getSecretKey()
            UserDAO.changePassword(user_id: String(user_id), new_password: newPass!, secret_key: secret_key, completeHandle: {(success: Bool, _ data:[UserResponse]?) in
                if success {
                    self.userDefaults.setValue(currentPass, forKey: Common.USER_PASSWORD)
                    self.alert.title = NSLocalizedString("app_name", comment: "")
                    self.alert.message = NSLocalizedString("change_password_success", comment: "")
                    self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                    self.alert.show()
                } else {
                    self.alert.title = NSLocalizedString("app_name", comment: "")
                    self.alert.message = NSLocalizedString("change_password_failed", comment: "")
                    self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                    self.alert.show()
                }
            })
        } else {
            self.alert.title = NSLocalizedString("app_name", comment: "")
            self.alert.message = NSLocalizedString("change_password_caution", comment: "")
            self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
            self.alert.show()
        }
    }
    
    func btn_SaveClicked(sender: UIBarButtonItem) {
        
        
    }
    
    func getCellAtIndexPath(section : Int,index : Int ) -> UITableViewCell
    {
        let indexPath = IndexPath(item: index, section: section
        )
        let cell = self.tableView.cellForRow(at: indexPath)
        return cell!
    }

}
