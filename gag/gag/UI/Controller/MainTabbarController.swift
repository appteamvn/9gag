//
//  MainTabbarController.swift
//  gag
//
//  Created by buisinam on 12/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class MainTabbarController: UITabBarController,UITabBarControllerDelegate {

    var userDefaults = UserDefaults()
    var arrayViewController:NSMutableArray!
    
    var profileNav: UINavigationController!
    var loginNav: UINavigationController!
    var newsNav: UINavigationController!
    var loginNavNew: UINavigationController!
    var notConnectView: UIView!
    let common = Common()
    var news : UINavigationController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
        profileNav = storyboard.instantiateViewController(withIdentifier: "ProfilesNav") as! UINavigationController
        loginNav = storyboard.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        newsNav = storyboard.instantiateViewController(withIdentifier: "NewsNav") as! UINavigationController
        loginNavNew = storyboard.instantiateViewController(withIdentifier: "loginNavNew") as! UINavigationController

        arrayViewController = NSMutableArray.init(array: self.viewControllers!)
        self.selectedViewController = arrayViewController.object(at: 1) as? UINavigationController
        
        NotificationCenter.default.addObserver(self, selector: #selector(onPostLoaded(notification:)), name: NSNotification.Name(rawValue: Common.UPLOADING), object: nil)
        news = arrayViewController.object(at: 3) as? UINavigationController

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mainCheckNetwork()
    }
    
    func reloadBadge(){
//        let alertController = UIAlertController(title: "", message: "\(UIApplication.shared.applicationIconBadgeNumber)", preferredStyle: UIAlertControllerStyle.alert)
//        
//        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//        {
//            (result : UIAlertAction) -> Void in
//            print("You pressed OK")
//        }
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)

        news.tabBarItem.badgeValue = "\(UIApplication.shared.applicationIconBadgeNumber)"
    }
    
    func mainCheckNetwork() {
        if !Common().checkNetworkConnect() {
            notConnectView =  UIView(frame: CGRect(x: 0, y: self.view.bounds.height - (self.tabBar.frame.height * 2), width: self.tabBar.frame.width, height: self.tabBar.frame.height))
            notConnectView.backgroundColor = UIColor(red: 215.0/255 , green: 59.0/255, blue: 116.0/255, alpha: 1.0)
            let btnNoInternet = UIButton(frame: CGRect(x: 0, y: 0, width: notConnectView.frame.width, height: notConnectView.frame.height))
            btnNoInternet.titleLabel?.font = btnNoInternet.titleLabel?.font.withSize(13)
            btnNoInternet.titleLabel?.numberOfLines = 2
            btnNoInternet.setTitle(NSLocalizedString("message_no_internet_connection", comment: ""), for: .normal)
            btnNoInternet.addTarget(self, action: #selector(dismissNoConnectView), for: UIControlEvents.touchUpInside)
            notConnectView.addSubview(btnNoInternet)
            UIApplication.shared.keyWindow?.addSubview(notConnectView)
            print("NO INTERNET CONNECTION")
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onPostLoaded(notification: NSNotification)
    {
        self.selectedViewController = arrayViewController.object(at: 4) as? UINavigationController
        delay(bySeconds: 1.0) {
            // delayed code, by default run in main thread
            let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
            parentHomeViewController.isProfile = true
            parentHomeViewController.selectedIndex = 1
            self.profileNav.pushViewController(parentHomeViewController, animated: false)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        self.tabBarController?.selectedIndex = 2
//        return false
//    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if self.selectedIndex == 4
        {
            if checkLogin(){
                arrayViewController[4] = profileNav
            }
            else{
                arrayViewController[4] = loginNav
            }
            self.setViewControllers((arrayViewController as NSArray) as? [UIViewController], animated: false)
        }
        
        if self.selectedIndex == 3 {
            if checkLogin() {
                arrayViewController[3] = newsNav
            } else {
                arrayViewController[3] = loginNavNew
            }
            self.setViewControllers((arrayViewController as NSArray) as? [UIViewController], animated: false)
        }
    }
    
    //MARK: CHECK LOGIN
    func checkLogin() -> Bool{
        if userDefaults.object(forKey: Common.USER_ID) == nil {
            return false
        } else {
            return true
        }
    }
    
    func switchProfiles()
    {
        profileNav.popToRootViewController(animated: false)
        arrayViewController[4] = profileNav
        self.setViewControllers((arrayViewController as NSArray) as? [UIViewController], animated: false)
    }
    
    func switchNews()
    {
        newsNav.popToRootViewController(animated: false)
        arrayViewController[3] = newsNav
        self.setViewControllers((arrayViewController as NSArray) as? [UIViewController], animated: false)
    }
    
    func tapOnBanner(post_id: Int, type: String)
    {
        self.selectedViewController = arrayViewController.object(at: 3) as? UINavigationController
        Common.showLoading(view: self.view, string: NSLocalizedString("loading", comment: ""))
        delay(bySeconds: 0.5) {
            // delayed code, by default run in main thread
            let vcDetail = CommentTableViewController()
            vcDetail.post_id = String(post_id)
            if type == "comment" {
                self.newsNav.pushViewController(vcDetail, animated: false)
            } else {
                vcDetail.isDetail = true
                vcDetail.isNews = true
                vcDetail.isComment = false
                self.newsNav.pushViewController(vcDetail, animated: false)
                self.arrayViewController[3] = self.newsNav
                self.setViewControllers((self.arrayViewController as NSArray) as? [UIViewController], animated: false)
                Common.hideLoading(view: self.view)
            }
        }
    }
    
    func switchLogin()
    {
        profileNav.popToRootViewController(animated: false)
        arrayViewController[4] = loginNav
        self.setViewControllers((arrayViewController as NSArray) as? [UIViewController], animated: false)
    }
    
    
    public func delay(bySeconds seconds: Double, dispatchLevel: DispatchLevel = .main, closure: @escaping () -> Void) {
        let dispatchTime = DispatchTime.now() + seconds
        dispatchLevel.dispatchQueue.asyncAfter(deadline: dispatchTime, execute: closure)
    }
    
    public enum DispatchLevel {
        case main, userInteractive, userInitiated, utility, background
        var dispatchQueue: DispatchQueue {
            switch self {
            case .main:                 return DispatchQueue.main
            case .userInteractive:      return DispatchQueue.global(qos: .userInteractive)
            case .userInitiated:        return DispatchQueue.global(qos: .userInitiated)
            case .utility:              return DispatchQueue.global(qos: .utility)
            case .background:           return DispatchQueue.global(qos: .background)
            }
        }
    }
    
    func dismissNoConnectView() {
        userDefaults.set(false, forKey: Common.NO_NETWORK_CONNECTION)
        notConnectView.removeFromSuperview()
    }
}
