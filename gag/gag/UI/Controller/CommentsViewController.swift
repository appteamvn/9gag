//
//  MessageViewController.swift
//  gag
//
//  Created by Hieu Nguyen on 11/12/16.
//  Copyright © 2016 buisinam. All rights reserved.
//


let DEBUG_CUSTOM_TYPING_INDICATOR = false

import SlackTextViewController
import NSDate_TimeAgo
import Kingfisher

class CommentsViewController: SLKTextViewController {
    open var post_id: String!
    private var type = "hot"
    
    var messages = [CommentResponse]()
    
    var users: Array = ["Allen", "Anna", "Alicia", "Arnold", "Armando", "Antonio", "Brad", "Catalaya", "Christoph", "Emerson", "Eric", "Everyone", "Steve"]
    //    var channels: Array = ["General", "Random", "iOS", "Bugs", "Sports", "Android", "UI", "SSB"]
    //    var emojis: Array = ["-1", "m", "man", "machine", "block-a", "block-b", "bowtie", "boar", "boat", "book", "bookmark", "neckbeard", "metal", "fu", "feelsgood"]
    var commands: Array = ["msg", "call", "text", "skype", "kick", "invite"]
    
    var searchResult: [String]?
    
    var pipWindow: UIWindow?
    
    var editingMessage = Message()
    
    var headerView: CommentHeaderView!
    
    override var tableView: UITableView {
        get {
            return super.tableView!
        }
    }
    
    override class func tableViewStyle (for decoder: NSCoder) -> UITableViewStyle {
        return .plain
    }
    
    func commonInit() {
        
        NotificationCenter.default.addObserver(self.tableView, selector: #selector(UITableView.reloadData), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
        //        NotificationCenter.default.addObserver(self,  selector: #selector(CommentsViewController.textInputbarDidMove(_:)), name: NSNotification.Name.SLKTextInputbarDidMove, object: nil)
        //
        // Register a SLKTextView subclass, if you need any special appearance and/or behavior customisation.
        //        self.registerClass(forTextView: CommentsViewController.classForCoder())
        
        //        if DEBUG_CUSTOM_TYPING_INDICATOR == true {
        //            // Register a UIView subclass, conforming to SLKTypingIndicatorProtocol, to use a custom typing indicator view.
        //            self.registerClass(forTypingIndicatorView: TypingIndicatorView.classForCoder())
        //        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let screenSize = UIScreen.main.bounds
        headerView = CommentHeaderView.init(frame: CGRect(x: 0, y: 0, width: 50, height: screenSize.size.width))
        self.configHeader()
        
        self.commonInit()
        
        // Example's configuration
        self.configureDataSource()
        
        // SLKTVC's configuration
        self.bounces = true
        self.shakeToClearEnabled = true
        self.isKeyboardPanningEnabled = true
        
        self.shouldScrollToBottomAfterKeyboardShows = true
        self.isInverted = false
        
        self.leftButton.setImage(UIImage(named: "icn_upload"), for: UIControlState())
        self.leftButton.tintColor = UIColor.gray
        
        self.rightButton.setTitle(NSLocalizedString("Send", comment: ""), for: UIControlState())
        self.textView.placeholder = NSLocalizedString("write_a_comment", comment: "")
        self.textInputbar.autoHideRightButton = true
        self.textInputbar.maxCharCount = 256
        self.textInputbar.counterStyle = .split
        self.textInputbar.counterPosition = .top
        
        self.textInputbar.editorTitle.textColor = UIColor.darkGray
        self.textInputbar.editorLeftButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        self.textInputbar.editorRightButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        self.textInputbar.isTranslucent = false
        self.textInputbar.barTintColor = .white
        
        if DEBUG_CUSTOM_TYPING_INDICATOR == false {
            self.typingIndicatorView!.canResignByTouch = true
        }
        
        self.tableView.separatorStyle = .none
        let nib = UINib(nibName: "MessageCommentCell", bundle: nil)
        let subNib = UINib(nibName: "SubMessageCommentCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MessageCommentCell" )
        self.tableView.register(subNib, forCellReuseIdentifier: "SubMessageCommentCell")
        
//        self.autoCompletionView.register(MessageTableViewCell.classForCoder(), forCellReuseIdentifier: AutoCompletionCellIdentifier)
        self.registerPrefixes(forAutoCompletion: ["@",  "#", ":", "+:", "/"])
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Configuration
    
    func configureDataSource() {
        CommentDAO.getListPostComment(post_id: post_id, type: type) { (success, data:[CommentResponse]?) in
            if success{
                self.messages = data!
                self.messages.sort(by: { (item1, item2) -> Bool in
                    return item1.comment_date_unix > item2.comment_date_unix
                })
                self.reloadHeader()
                self.tableView.reloadData()
            }
        }
        
    }
    
    func configHeader(){
        headerView.segment.didSelectItemWith = { (index, title) -> () in
            print("Selected item \(index)")
            self.type = (title?.lowercased())!
            self.configureDataSource()
        }
    }
    
    func reloadHeader(){
        headerView.lblNumber.text = "\(messages.count)" + NSLocalizedString("comment", comment: "")
    }
    
    // MARK: - Override func
    
    override func didChangeAutoCompletionPrefix(_ prefix: String, andWord word: String) {
        
        var array:Array<String> = []
        let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", word);
        
        self.searchResult = nil
        
        if prefix == "@" {
            if word.characters.count > 0 {
                array = self.users.filter { wordPredicate.evaluate(with: $0) };
            }
            else {
                array = self.users
            }
        }
        
        var show = false
        
        if array.count > 0 {
            let sortedArray = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            self.searchResult = sortedArray
            show = sortedArray.count > 0
        }
        
        self.showAutoCompletionView(show)
    }
    
    // MARK: - UITableViewDataSource Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableView {
            return self.messages.count
        }
        else {
            if let searchResult = self.searchResult {
                return searchResult.count
            }
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //        if tableView == self.tableView {
        //            return self.messageCellForRowAtIndexPath(indexPath)
        //        }
        return self.messageCellForRowAtIndexPath(indexPath)
    }
    
    //     Load Data
    func messageCellForRowAtIndexPath(_ indexPath: IndexPath) -> MessageCommentCell{
        let message = self.messages[(indexPath as NSIndexPath).row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MessageCommentCell") as! MessageCommentCell
        
        // Assign Data Super Comment Cell
        if let url: URL = URL(string: message.comment_author.author_avatar){
            cell.imgAvatar.kf.setImage(with: ImageResource(downloadURL: url,cacheKey: nil))
        }
        cell.imgAvatar.layer.masksToBounds = true
        cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.bounds.size.width / 2
        cell.lblName.text = message.comment_author.author_name
        cell.lblOP.text = "OP"
        cell.lblPoint.text = "\(message.comment_voted)" + " Points"
        cell.lblContent.text = message.comment_content
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        cell.transform = self.tableView.transform
        
        if message.comment_voted == 0{
            cell.btnLike.tintColor = Common.lighGrayColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
        }else if message.comment_voted == -1{
            cell.btnUnlike.tintColor = Common.blueColor()
            cell.btnLike.tintColor = Common.lighGrayColor()
        }else {
            cell.btnLike.tintColor = Common.blueColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
        }

        cell.btnLike.tag = indexPath.row
        cell.btnUnlike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(likeCommentAction(sender:)), for: .touchUpInside)
        cell.btnUnlike.addTarget(self, action: #selector(unLikeCommentAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        return headerView
    }
    
//    func subMessageCellForRowAtIndexPath(_ indexPath: IndexPath) -> SubMessageCommentCell {
//        
////        let message = self.messages[(indexPath as NSIndexPath).row]
////        
////        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SubMessageCommentCell") as! SubMessageCommentCell
////        // Assign Data Sub Comment Cell
////        cell.imgSubAvatar = message.imgAvatar
////        cell.lblSubName.text = message.username as String
////        cell.lblSubOP.text = message.isOP ? "OP" : ""
////        cell.lblSubPoint.text =  "\(message.points)" + (message.points > 1 ? "points" : "point")
////        cell.lblSubTime.text = "\(message.timer)"
////        cell.lblSubText.text = message.textMessage as String
//        
//        // Cells must inherit the table view's transform
//        // This is very important, since the main table view may be inverted
//        cell.transform = self.tableView.transform
//        return cell
//    }
    
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    override func didPressRightButton(_ sender: Any!) {
        
        // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
        self.textView.refreshFirstResponder()
        
//        let data :CommentResponse = CommentResponse.newInstance() as! CommentResponse
//        message.username = "Mona Media"
//        message.textMessage = self.textView.text as NSString
//        message.isOP = true
//        message.points = Int(arc4random_uniform(100))
//        let date:NSDate = NSDate()
//        let ago = date.timeAgo()
//        message.timer = ago! as NSString
        
//        let indexPath = IndexPath(row: 0, section: 0)
//        let rowAnimation: UITableViewRowAnimation = self.isInverted ? .bottom : .top
//        let scrollPosition: UITableViewScrollPosition = self.isInverted ? .bottom : .top
//        
//        
//        self.tableView.beginUpdates()
//        self.tableView.insertRows(at: [indexPath], with: rowAnimation)
//        self.tableView.endUpdates()
//        
//        self.tableView.scrollToRow(at: indexPath, at: scrollPosition, animated: true)
//        
//        // Fixes the cell from blinking (because of the transform, when using translucent cells)
//        // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
//        self.tableView.reloadRows(at: [indexPath], with: .automatic)
        
        CommentDAO.commentPost(post_id: post_id, comment_parent: "0", comment_content: self.textView.text){ (success) in
            if success{
                self.configureDataSource()
            }
        }
        self.textView.resignFirstResponder()
        self.scrollView?.slk_scrollToTop(animated: true)
        super.didPressRightButton(sender)
    }
    
    // MARK: - UITableViewDelegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.autoCompletionView {
            
            guard let searchResult = self.searchResult else {
                return
            }
            
            var item = searchResult[(indexPath as NSIndexPath).row]
            
            if self.foundPrefix == "@" && self.foundPrefixRange.location == 0 {
                item += ":"
            }
            
            item += " "
            
            self.acceptAutoCompletion(with: item, keepPrefix: true)
        }
    }
    
    // MARK: - UIScrollViewDelegate Methods
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
    func getCellAtIndexPath(index : Int) -> MessageCommentCell
    {
        let indexPath = IndexPath(item: index, section: 0)
        let cell =  self.tableView.cellForRow(at: indexPath)
        return cell as! MessageCommentCell
    }
    
    func likeCommentAction(sender: Any) {
        let dataResult = self.messages[(sender as AnyObject).tag] as CommentResponse
        switch dataResult.comment_voted {
        case 0:
            print("nhức nách quá")
            let cell = getCellAtIndexPath(index: (sender as AnyObject).tag)
            dataResult.comment_point += 1
            cell.btnLike.tintColor = Common.blueColor()
            cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
            cell.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
                }
            }
            break
        case 1:
            print("nhức nách quá")
            let cell = getCellAtIndexPath(index: (sender as AnyObject).tag)
            dataResult.comment_point -= 1
            cell.btnLike.tintColor = Common.lighGrayColor()
            cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
            cell.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 0
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
                }
            }
            break
        case -1:
            let cell = getCellAtIndexPath(index: (sender as AnyObject).tag)
            dataResult.comment_point += 2
            cell.btnLike.tintColor = Common.blueColor()
            cell.btnUnlike.tintColor = Common.lighGrayColor()
            cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
            cell.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
                }
            }
            break
        default:
            break
        }
    }
    
    func unLikeCommentAction(sender: Any) {
        let dataResult = self.messages[(sender as AnyObject).tag] as CommentResponse
        switch dataResult.comment_voted {
        case 0:
            print("nhức nách quá")
            let cell = getCellAtIndexPath(index: (sender as AnyObject).tag)
            dataResult.comment_point -= 1
            cell.btnUnlike.tintColor = Common.blueColor()
            cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
            cell.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = -1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
                }
            }
            break
        case 1:
            print("nhức nách quá")
            let cell = getCellAtIndexPath(index: (sender as AnyObject).tag)
            dataResult.comment_point -= 2
            cell.btnUnlike.tintColor = Common.blueColor()
            cell.btnLike.tintColor = Common.lighGrayColor()
            cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
            cell.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = -1
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: -1) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
                }
            }
            break
        case -1:
            print("nhức nách quá")
            let cell = getCellAtIndexPath(index: (sender as AnyObject).tag)
            dataResult.comment_point += 1
            cell.btnUnlike.tintColor = Common.lighGrayColor()
            cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
            cell.lblContent.text = dataResult.comment_content
            dataResult.comment_voted = 0
            
            CommentDAO.userVoteComment(user_id: "\(BaseDAO.getUserId())", comment_id: dataResult.comment_id, value: 0) { (success: Bool, result) in
                if success {
                    print("success")
                    dataResult.comment_point = Int(result!)
                    cell.lblPoint.text =  "\(Int((dataResult.comment_point)))" + NSLocalizedString("point", comment: "")
                }
            }

            break
        default:
            break
        }
    }
}

