//
//  SubMessageCommentCell.swift
//  gag
//
//  Created by Hieu Nguyen on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import Foundation
import UIKit

class SubMessageCommentCell : MessageCommentCell {
    
    // Initilized Sub Comment Cell
//    @IBOutlet weak var imgAvatar:UIImageView!
//    @IBOutlet weak var lblName:UILabel!
//    @IBOutlet weak var lblOP:UILabel!
//    @IBOutlet weak var lblPoint: UILabel!
//    @IBOutlet weak var lblTime: UILabel!
//    @IBOutlet weak var lblContent: UILabel!
//    @IBOutlet weak var btnReply: UIButton!
//    @IBOutlet weak var btnLike: UIButton!
//    @IBOutlet weak var btnUnlike: UIButton!
//    @IBOutlet weak var btnMore: UIButton!
//    @IBOutlet weak var lblShowReply: UILabel!
    @IBOutlet weak var heighLblMore: NSLayoutConstraint!
    @IBOutlet weak var btnViewMore: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //    override init (frame:CGRect)
    //    {
    //        super.init(frame:frame)
    //    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
