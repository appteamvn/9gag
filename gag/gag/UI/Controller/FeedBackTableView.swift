//
//  FeedBackTableView.swift
//  gag
//
//  Created by ThanhToa on 11/26/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Photos
class FeedBackTableView: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var userDefaults = UserDefaults()
    var problemType = ""
    var fbImagesArray = [String]()
    var isRating = false
    var saveItem = UIBarButtonItem()
    let isAuthorizePhoto = PHPhotoLibrary.authorizationStatus()
    var imageArray = [UIImage] ()
    @IBOutlet weak var feedbackTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("feed_back", comment: "")
        saveItem = UIBarButtonItem(title: NSLocalizedString("send", comment: ""), style: .plain, target: self, action: #selector(saveChange))
        self.navigationItem.rightBarButtonItem = saveItem
        feedbackTableView.delegate = self
        feedbackTableView.dataSource = self
        fbImagesArray.append(NSLocalizedString("add-picture", comment: ""))
        imageArray.append(UIImage(named: NSLocalizedString("add-picture", comment: ""))!)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isRating {
            return 4
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isRating {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell1Identifier", for: indexPath) as! FeedbackCell1
                if userDefaults.object(forKey: Common.USER_EMAIL) != nil {
                    let email = userDefaults.object(forKey: Common.USER_EMAIL) as! String
                    if !email.isEmpty {
                        cell.FB1UserEmail.text = email
                    } else {
                        cell.FB1UserEmail.placeholder = NSLocalizedString("your_email_address", comment: "")
                    }
      
                } else {
                    cell.FB1UserEmail.text = ""
                    cell.FB1UserEmail.placeholder = NSLocalizedString("your_email_address", comment: "")
                }
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell2Identifier", for: indexPath) as! FeedbackCell2
                cell.FB2FeedBackMessage.placeholder = NSLocalizedString("feedback_place_holder", comment: "")
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCellNilIdentifier", for: indexPath)
                cell.backgroundColor = UIColor.lightGray
                cell.textLabel?.text = NSLocalizedString("screen_shot", comment: "")
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell3Identifier", for: indexPath) as! FeedbackCell3
                cell.FB3CollectionView.backgroundColor = UIColor.clear
                cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
                return cell
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell0Identifier", for: indexPath) as! FeedbackCell0
                if problemType.isEmpty {
                    cell.textLabel?.text = NSLocalizedString("select_type_feedback", comment: "")
                } else {
                    cell.textLabel?.text = problemType
                }
                cell.accessoryType = .disclosureIndicator
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell1Identifier", for: indexPath) as! FeedbackCell1
                if userDefaults.object(forKey: Common.USER_EMAIL) != nil {
                    cell.FB1UserEmail.text = userDefaults.object(forKey: Common.USER_EMAIL) as? String
                } else {
                    cell.FB1UserEmail.text = ""
                    cell.FB1UserEmail.placeholder = NSLocalizedString("your_email_address", comment: "")
                }
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell2Identifier", for: indexPath) as! FeedbackCell2
                cell.FB2FeedBackMessage.placeholder = NSLocalizedString("feedback_place_holder", comment: "")
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCellNilIdentifier", for: indexPath)
                cell.backgroundColor = UIColor.lightGray
                cell.textLabel?.text = NSLocalizedString("screen_shot", comment: "")
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell3Identifier", for: indexPath) as! FeedbackCell3
                cell.FB3CollectionView.backgroundColor = UIColor.clear
                cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
                return cell
            default:
                break
            }
        }
                return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isRating {
            return NSLocalizedString("your_rating", comment: "")
        } else {
            return NSLocalizedString("feed_back_content", comment: "")
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isRating {
            switch indexPath.row {
            case 0:
                break
            case 1:
                break
            case 2:
                break
            case 3:
                break
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0:
                let feedbackTypeSelection = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackTypeSelection") as! FeedbackTypeSelection
                self.show(feedbackTypeSelection, sender: nil)
                break
            case 1:
                break
            case 2:
                break
            case 3:
                break
            case 4:
                break
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isRating {
            switch indexPath.row {
            case 0:
                return 35
            case 1:
                return 120
            case 2:
                return 30
            case 3:
                return 99
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0:
                return 35
            case 1:
                return 35
            case 2:
                return 120
            case 3:
                return 30
            case 4:
                return 99
            default:
                break
            }
            
        }
        return 30
    }
    
    func saveChange() {
        var cell1 = FeedbackCell2()
        if isRating {
            cell1 = getCellAtIndexPath(index: 1) as! FeedbackCell2
        } else {
            cell1 = getCellAtIndexPath(index: 2) as! FeedbackCell2
        }
        let alert = UIAlertView()
       if (cell1.FB2FeedBackMessage.text?.characters.count)! < 8 {
            alert.title = NSLocalizedString("error_nil_post_title", comment: "")
            alert.message = NSLocalizedString("error_nil_post_message", comment: "")
            alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
            alert.show()
       } else {
        alert.title = NSLocalizedString("app_name", comment: "")
        alert.message = NSLocalizedString("not_support_feature", comment: "")
        alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        alert.show()
        }
    }
    
    func getCellAtIndexPath(index : Int) -> UITableViewCell
    {
        let indexPath = IndexPath(item: index, section: 0)
        let cell =  self.feedbackTableView.cellForRow(at: indexPath)
        return cell!
    }
    
    func btnGalleryAction() {
        if(isAuthorizePhoto == PHAuthorizationStatus.authorized)
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imageChooser = UIImagePickerController()
                imageChooser.delegate = self
                imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imageChooser.allowsEditing = true
                if((self.presentationController) != nil) {
                    self.present(imageChooser, animated: true, completion: nil)
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization() {statusPhoto in
                switch statusPhoto {
                case .authorized:
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imageChooser = UIImagePickerController()
                        imageChooser.delegate = self
                        imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imageChooser.allowsEditing = true
                        if((self.presentationController) != nil) {
                            self.present(imageChooser, animated: true, completion: nil)
                        }
                    }
                    break
                case .denied, .restricted:
                    let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(appSettings as URL)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                    break
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var cell3 = FeedbackCell3()
        if isRating {
            cell3 = getCellAtIndexPath(index: 3) as! FeedbackCell3
        } else {
            cell3 = getCellAtIndexPath(index: 4) as! FeedbackCell3
        }
        
        let common = Common()
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let scaledImage = common.scaleImageWidth(image: pickedImage, targetSize: CGSize(width: 750, height: 1334))
            if imageArray.count < 4 {
                imageArray.append(scaledImage)
                cell3.FB3CollectionView.reloadData()
            }
            dismiss(animated: true) {
                if self.imageArray.count == 4{
                    Common.showAlert(title: NSLocalizedString("app_name", comment: ""),
                                     message: NSLocalizedString("accept_access_photo_message", comment: ""),
                                     titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                        print("Full image FB")
                    })
                }

            }
        }
    }

}



extension FeedBackTableView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fbCollectionCellIdentifier", for: indexPath) as! FB3CollectionViewCell
        if indexPath.row == 0 {
            cell.FB3DeletButton.isHidden = true
            cell.FB3ImageView.image = imageArray[0]
        } else {
            cell.FB3DeletButton.isHidden = false
            cell.FB3DeletButton.backgroundColor = UIColor.lightGray
            cell.FB3DeletButton.layer.cornerRadius = 10
            cell.FB3DeletButton.layer.masksToBounds = true
            cell.FB3ImageView.image = imageArray[indexPath.row]
            cell.FB3DeletButton.tag = indexPath.row
            cell.FB3DeletButton.addTarget(self, action: #selector(deleteFB3Image(sender:)), for: .touchUpInside)

        }
        cell.FB3ImageView.layer.cornerRadius = 3
        cell.FB3ImageView.layer.masksToBounds = true
        return cell
    }
    
    func deleteFB3Image(sender: UIButton) {
        var cell3 = FeedbackCell3()
        if isRating {
            cell3 = getCellAtIndexPath(index: 3) as! FeedbackCell3
        } else {
            cell3 = getCellAtIndexPath(index: 4) as! FeedbackCell3
        }
        cell3.FB3CollectionView.reloadData()
        imageArray.remove(at: sender.tag)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.btnGalleryAction()
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
}
