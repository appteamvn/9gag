//
//  Join9GagViewController.swift
//  gag
//
//  Created by ThanhToa on 12/2/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
class Join9GagViewController: UIViewController{
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var appIcon: UIImageView!
    
    var userDefaults = UserDefaults()
    var  mainTabbar: MainTabbarController!
    var isBackgroundBlack = false
    var uiview = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signUpButton.layer.cornerRadius = 3
        logInButton.layer.borderWidth = 1
        logInButton.layer.borderColor = UIColor.lightGray.cgColor
        logInButton.layer.cornerRadius = 3
        self.signUpButton.setTitle(NSLocalizedString("sign_up", comment: ""), for: .normal)
        self.logInButton.setTitle(NSLocalizedString("log_in", comment: ""), for: .normal)
        if !isBackgroundBlack {
            mainTabbar = self.tabBarController as! MainTabbarController
            lblTitle.text = NSLocalizedString("join_us_msn", comment: "")
            lblMessage.text = NSLocalizedString("join_fackju_message", comment: "")
        }
        self.checkCurrentView()
        if isBackgroundBlack {
            self.view.backgroundColor = UIColor.black
            lblTitle.text = NSLocalizedString("fackju_user_msg", comment: "")
            lblTitle.textColor = UIColor.white
            lblMessage.text = NSLocalizedString("join_us_msn", comment: "")
            lblMessage.textColor = UIColor.lightGray
            logInButton.setTitleColor(UIColor.black, for: UIControlState.normal)
            signUpButton.titleLabel?.textColor = UIColor.white
            notNowButton.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
            notNowButton.setTitle(NSLocalizedString("not_now", comment: ""), for: .normal)
            appIcon.layer.cornerRadius = 20
            appIcon.layer.masksToBounds = true
            appIcon.image = UIImage(named: "fackju_icon")
        } else {
            appIcon.isHidden = true
            appIcon = nil
            notNowButton = nil
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkCurrentView()
        if isBackgroundBlack {
            self.navigationController?.navigationBar.isHidden = true
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func checkLogin() {
        if BaseDAO.checkLogin() {
            let profileViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.show(profileViewController, sender: nil)
        }
    }
    
    func checkCurrentView() {
        uiview = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let settingBtn = UIBarButtonItem(title: NSLocalizedString("setting", comment: ""), style: .plain, target: self, action: #selector(setUpSettingBtn))
        uiview.tag = 111
        if self.tabBarController?.selectedIndex == 3 {
            if userDefaults.object(forKey: Common.USER_ID) == nil {
                let barButton = UIBarButtonItem(customView: uiview)
                self.navigationItem.leftBarButtonItem = barButton
                self.navigationItem.title = NSLocalizedString("join_app", comment: "")
                self.navigationItem.rightBarButtonItem = settingBtn
            }
        }
        if self.tabBarController?.selectedIndex == 4 {
            if uiview.tag == 111 {
                uiview.removeFromSuperview()
            }
            self.navigationItem.title = NSLocalizedString("join_app", comment: "")
            self.navigationItem.rightBarButtonItem = settingBtn
        }
    }
    
    //MARK: SETTING BUTTON
    func setUpSettingBtn() {
        let actionSheetSettings = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("get_pro_version", comment: ""), style: .default, handler: {(UIAlertAction)in
            self.showGettingProVersion1()
//            ProfileViewController.showGettingProVersion()
            print("press 9GAG")
        }))
        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default, handler: {(UIAlertAction)in
            let settingView = self.storyboard?.instantiateViewController(withIdentifier: "profileSettingsTableViewControllerIdentifier") as! ProfileSettingsTableViewController
            settingView.isNotLogin = true
            self.show(settingView, sender: nil)
        }))
        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_send", comment: ""), style: .default, handler: {(UIAlertAction)in
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            self.show(feedBackTableView, sender: nil)
        }))
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (action) in
        }
        actionSheetSettings.addAction(cancelAction)
        self.present(actionSheetSettings, animated: true, completion: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: LOGIN ACTION
    @IBAction func signUpAction(_ sender: Any) {
        self.uiview.removeFromSuperview()
        let logInViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        logInViewController.isSignUp = true
        self.show(logInViewController, sender: nil)
    }
    
    @IBAction func logInAction(_ sender: Any) {
        self.uiview.removeFromSuperview()
        let logInViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        let notLoginNotify = self.userDefaults.bool(forKey: Common.NOT_LOGIN_NOTIFY)
        if notLoginNotify {
            logInViewController.mainTabbar = self.mainTabbar
        }
        self.show(logInViewController, sender: nil)
    }

    @IBAction func notNowAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func showGettingProVersion1() {
        let message = NSLocalizedString("fackju_pro_feature", comment: "")
        let alertGetProversion = UIAlertController(title: NSLocalizedString("unlock_pro_version", comment: ""), message: message, preferredStyle: .alert)
        
        alertGetProversion.addAction(UIAlertAction(title: NSLocalizedString("unlock_pro_version_price", comment: ""), style: .default, handler: {(UIAlertAction) in
            print("press Unlock")
        }))
        alertGetProversion.addAction(UIAlertAction(title: NSLocalizedString("restore_purchase", comment: ""), style: .default, handler: {(UIAlertAction) in
            print("press Purchase")
        }))
        alertGetProversion.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction) in
            print("press Cancel")
        }))
        self.present(alertGetProversion, animated: true, completion: nil)
    }

}
