//
//  FirstViewController.swift
//  gag
//
//  Created by buisinam on 10/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import HidingNavigationBar
import Kingfisher

class VideosViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,GalleryItemsDataSource, GalleryDisplacedViewsDataSource{
    
    var videoImageArray: [VideoResponse]!
    
    open var pageIndex = 0
    
    // Properties
    let cornerRadius:CGFloat = 3.0
    let heighIndicator:CGFloat = 1.0
    
    // NavigationBar
    var seletionBar: UIView = UIView()
    var hidingNavBarManager: HidingNavigationBarManager?
    
    @IBOutlet weak var collectionView: UICollectionView!
    let nbsNav = UINib(nibName: "NBSNavBlur", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NBSNavBlur
    
    var galleryViewController: GalleryViewController!
    
    let refControl = UIRefreshControl()
    var userDefaults = UserDefaults()

    func refresh(sender:AnyObject)
    {
        self.refControl.beginRefreshing()
        
        UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.collectionView?.contentOffset = CGPoint(x: 0, y: 100)
            
        }, completion: { (complete) in
            if complete == true{
                if Common().checkNetworkConnect() {
                self.refreshCall()
                }
            }
        })
        
        //        }
        // your refreshing logic here...
        
    }
    override func viewDidLoad() {
        collectionView.delegate = self
        collectionView.dataSource = self
        self.getqueryListVideo()
        refControl.attributedTitle = NSAttributedString(string: "")
        refControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        self.collectionView?.addSubview(refControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hidingNavBarManager?.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpButtonBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func refreshCall() {
        self.getqueryListVideo()
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        hidingNavBarManager?.shouldScrollToTop()
        
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.videoImageArray != nil {
            return self.videoImageArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "VideoContentCellIdentifier"
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! VideoContentCollectionViewCell
        let postResponse = videoImageArray[indexPath.item]
        cell.videoContentImage.layer.cornerRadius = self.cornerRadius
        cell.videoContentTitle.text =  String(format: "%@", postResponse.post_title)
        guard let urlThumb: String = postResponse.youtube_thumbnail else{
            return cell
        }
//        urlThumb.add
        
        let resources = ImageResource(downloadURL: URL(string: UtilityObjc.encodeURL(urlThumb))!)
        cell.videoContentImage.kf.setImage(with: resources)
        return cell
    }
    
    let collectionHorizental = CollectionViewHorizontal()
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let indexPath = IndexPath(item: indexPath.item, section: 0)
//        let cell =  self.collectionView.cellForItem(at: indexPath) as! VideoContentCollectionViewCell
//        
//        _ = cell.videoContentImage
//        let displacedViewIndex = indexPath.item
        
        
//        galleryViewController = GalleryViewController(startIndex: displacedViewIndex, itemsDataSource: self as! GalleryItemsDataSource , displacedViewsDataSource: self, configuration: galleryConfiguration())
//        
//        
//        galleryViewController.headerView = nbsNav
//        galleryViewController.launchedCompletion = { self.reloadNavBarData(index: displacedViewIndex) }
//        galleryViewController.closedCompletion = { UIApplication.shared.statusBarStyle = .default}
//        galleryViewController.swipedToDismissCompletion = { UIApplication.shared.statusBarStyle = .default}
//        galleryViewController.landedPageAtIndexCompletion = { index in
//            self.reloadNavBarData(index: index)
//        }
//        
//        self.presentImageGallery(galleryViewController)
        
        let videoObj = videoImageArray[indexPath.item] 
        
        let videoVC: JWViewController = self.storyboard?.instantiateViewController(withIdentifier: "JWViewController") as! JWViewController
//        videoVC.modalPresentationStyle = .currentContext
        videoVC.modalTransitionStyle = .crossDissolve
        videoVC.videoObj = videoObj
        self.present(videoVC, animated: true, completion: nil)
    }
    
    func setUpButtonBar(){
        nbsNav.btnClose.addTarget(self, action: #selector(tapCloseFullScreen), for: .touchUpInside)
        
        var frame = nbsNav.frame
        frame.size.width = self.view.frame.width
        nbsNav.frame = frame
        
    }
    
    func tapCloseFullScreen()
    {
        UIApplication.shared.statusBarStyle = .default
        galleryViewController.closeGallery(true, completion: nil)
         galleryViewController = nil
    }
    
    func reloadNavBarData(index: Int){
        let data = self.videoImageArray[index]
        nbsNav.lblTitle.text = data.post_title
        nbsNav.lblVote.text =  data.youtube_duration
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width  = (collectionView.frame.size.width - 15) / 2
        let height = width * 0.9
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    func itemCount() -> Int {
        
        return self.videoImageArray.count
    }
    
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        let cell = getCellAtIndexPath(index: index)
        let  imgView = cell.videoContentImage as UIImageView
        return imgView
    }
    
    func getCellAtIndexPath(index: Int) ->VideoContentCollectionViewCell{
        let indexPath = IndexPath(item: index, section: 0)
        let cell =  self.collectionView.cellForItem(at: indexPath) as! VideoContentCollectionViewCell
        return cell
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let data = self.videoImageArray[index]
        print(data.description)

        return GalleryItem.video(fetchPreviewImageBlock: { $0(UIImage()) }, videoURL: URL(string: data.post_link)!)
        
    }
    
    func galleryConfiguration() -> GalleryConfiguration {
        
        return [
            GalleryConfigurationItem.showAds(false),
            GalleryConfigurationItem.pagingMode(.standard),
            GalleryConfigurationItem.presentationStyle(.displacement),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            
            GalleryConfigurationItem.maximumZoomScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.linear),
            
            GalleryConfigurationItem.statusBarHidden(false),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(false),
            GalleryConfigurationItem.displacementInsetMargin(50),
            GalleryConfigurationItem.closeButtonMode(.none),
            GalleryConfigurationItem.closeButtonMode(.none),
            GalleryConfigurationItem.thumbnailsButtonMode(.none),
            GalleryConfigurationItem.deleteButtonMode(.none)
        ]
    }
    
    func getqueryListVideo() {
        
        var display = "category"
        var type = ""
        var categorID = 0
        let categoryArray = VideoDao.VideoDaoSingleton.sharedInstance.categoryVideo
        
        if pageIndex == 0{
            display = "home"
        }
        else{
            categorID = categoryArray[pageIndex - 1].category_id
        }

        
        VideoDao.getListVideo(display: display, type: type,category_id: categorID,since: "", last: "") {(success: Bool, data) in
            if success {
                print("success")
                self.videoImageArray = data
                self.collectionView.reloadData()
                self.collectionView.contentOffset = CGPoint(x: 0, y: 100)
                //convert Youtube Link
//                for item in self.videoImageArray
//                {
//                    let realURL = NSURL(string: Common.getYouTubeLink(youtubeID: item.youtube_id))!
//                    Youtube.h264videosWithYoutubeURL(realURL as URL) { (videoInfo, error) -> Void in
//                        if let videoURLString = videoInfo?["url"] as? String{
//                            item.post_link = videoURLString
//                        }
//                    }
//                }
            }
            self.refControl.endRefreshing()
        }
    }
}

