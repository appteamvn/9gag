//
//  JWViewController.swift
//  gag
//
//  Created by buisinam on 4/11/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit
import BMPlayer
import NVActivityIndicatorView

class JWViewController: UIViewController {
    
    var videoObj: VideoResponse!
    
    var dataSources: [[String: Any]] = []
    
    var player: BMPlayer!
    
    var index: IndexPath!
    
    var changeButton = UIButton()
    
    var arrConfig : [BMPlayerResourceDefinition]!
    
    let nbsNav = UINib(nibName: "NBSNavBlur", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NBSNavBlur
    
    var isAnimating = false
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var exitPlayer = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.shouldRotate = true
        
        setupPlayerManager()
        preparePlayer()
        getLink()
        setUpButtonBar()
//        NotificationCenter.default.addObserver(self, selector: #selector(self.rotate), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        setUpButtonBar()
    }
    
    func setUpButtonBar(){
        nbsNav.btnClose.addTarget(self, action: #selector(tapCloseFullScreen), for: .touchUpInside)
        
        var width = self.view.frame.width
        var height = self.view.frame.height
//
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Landscape")
            if nbsNav != nil{
                nbsNav.removeFromSuperview()
            }
        }
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Portrait")
            width = min(self.view.frame.height,self.view.frame.width)
            height = max(self.view.frame.height,self.view.frame.width)
            var frame = nbsNav.frame
            frame.size.width = width
            nbsNav.frame = frame
            
            nbsNav.lblTitle.text = videoObj.post_title
            self.view.addSubview(nbsNav)
        }
        
        
        
        
    }
    
    @objc fileprivate func rotate() {
        
        /// If the app supports rotation on global level, we don't need to rotate here manually because the rotation
        /// of key Window will rotate all app's content with it via affine transform and from the perspective of the
        /// gallery it is just a simple relayout. Allowing access to remaining code only makes sense if the app is
        /// portrait only but we still want to support rotation inside the gallery.
        guard UIApplication.isPortraitOnly else { return }
        
        guard UIDevice.current.orientation.isFlat == false &&
            isAnimating == false else { return }
        
        isAnimating = true
        
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Landscape")
//            UIApplication.shared.isStatusBarHidden = true
            //            heightConstrainsTopbar.constant = 40
            //            headerView?.layoutIfNeeded()
        }
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Portrait")
//            UIApplication.shared.isStatusBarHidden = false
            //            heightConstrainsTopbar.constant = 64
            //            headerView?.layoutIfNeeded()
            
        }
        
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: { [weak self] () -> Void in
            
            self?.view.transform = windowRotationTransform()
            self?.view.bounds = rotationAdjustedBounds()
            self?.view.setNeedsLayout()
            self?.view.layoutIfNeeded()
            self?.setUpButtonBar()
            })
        { [weak self] finished  in
            
            self?.isAnimating = false
        }
    }
    
    func tapCloseFullScreen(){
        exitPlayer = true
        player.exitFullScreen()
        appDelegate.shouldRotate = false
        self.dismiss(animated: true) { 
//            UIApplication.shared.isStatusBarHidden = false
        }
    }
    
    func getLink(){
        let urlString = videoObj.google_drive
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error)
            } else {
                do {
                    if let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String:Any]]{
                        self.dataSources = result
                        print(self.dataSources)
                        self.setupPlayerResource()
                    }
                } catch let error as NSError {
                    print(error)
                }
            }
            
            }.resume()
    }
    
    /**
     prepare playerView
     */
    func preparePlayer() {
        var controller: BMPlayerControlView? = nil
        
        
        controller = BMPlayerCustomControlView()
        
        
        player = BMPlayer(customControllView: controller)
        view.addSubview(player)
        player.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.height.equalTo(view.snp.width).multipliedBy(9.0/16.0)
            make.centerY.equalTo(view.snp.centerY)
            make.centerX.equalTo(view.snp.centerX)
        }
        player.delegate = self
        player.backBlock = { [unowned self] (isFullScreen) in
            self.tapCloseFullScreen()
        }
        
        /// Listening to player state changes with Block
        //Listen to when the player is playing or stopped
        //        player.playStateDidChange = { (isPlaying: Bool) in
        //            print("| BMPlayer Block | playStateDidChange \(isPlaying)")
        //        }
        
        //Listen to when the play time changes
        //        player.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
        //            print("| BMPlayer Block | playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
        //        }
        
//        changeButton.setTitle("Change Video", for: .normal)
//        changeButton.addTarget(self, action: #selector(onChangeVideoButtonPressed), for: .touchUpInside)
//        changeButton.backgroundColor = UIColor.red.withAlphaComponent(0.7)
//        view.addSubview(changeButton)
//        
//        changeButton.snp.makeConstraints { (make) in
//            make.top.equalTo(player.snp.bottom).offset(30)
//            make.left.equalTo(view.snp.left).offset(10)
//        }
//        changeButton.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    func setupPlayerResource() {
        let asset = self.preparePlayerItem()
        player.setVideo(resource: asset)
    }
    
    func setupPlayerManager() {
        resetPlayerManager()
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
    }
    
    
    /**
     准备播放器资源model
     */
    func preparePlayerItem() -> BMPlayerResource {
        
        arrConfig = []
        for data: [String: Any] in dataSources{
            arrConfig.append(BMPlayerResourceDefinition(url: URL(string: data["file"] as! String)!,
                                                    definition: "\(data["label"] as! Int)"))
        }
        

        let asset = BMPlayerResource(name: videoObj.post_title,
                                     definitions: arrConfig,
                                     cover: nil)//cover image
        return asset
    }
    
    
    func resetPlayerManager() {
        BMPlayerConf.allowLog = false
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
        BMPlayerConf.topBarShowInCase = .horizantalOnly
        BMPlayerConf.loaderType  = NVActivityIndicatorType.semiCircleSpin

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: false)
        // If use the slide to back, remember to call this method
        player.pause(allowAutoPlay: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: false)
        // If use the slide to back, remember to call this method
        player.autoPlay()
        
    }
    
    deinit {
        // If use the slide to back, remember to call this method
        player.prepareToDealloc()
        print("VideoPlayViewController Deinit")
    }

}

// MARK:- BMPlayerDelegate example
extension JWViewController: BMPlayerDelegate {
    // Call back when playing state changed, use to detect is playing or not
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        print("| BMPlayerDelegate | playerIsPlaying | playing - \(playing)")
    }
    
    // Call back when playing state changed, use to detect specefic state like buffering, bufferfinished
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        print("| BMPlayerDelegate | playerStateDidChange | state - \(state)")
        if state == .bufferFinished && exitPlayer == false{
            player.enterFullScreen()
        }
    }
    
    // Call back when play time change
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
        //        print("| BMPlayerDelegate | playTimeDidChange | \(currentTime) of \(totalTime)")
    }
    
    // Call back when the video loaded duration changed
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        //        print("| BMPlayerDelegate | loadedTimeDidChange | \(loadedDuration) of \(totalDuration)")
    }
}
