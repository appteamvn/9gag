//
//  TimeLineCollectionViewCell.swift
//  gag
//
//  Created by ThanhToa on 11/2/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class TimeLineCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var profileActionStatus: UILabel!
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postPoints: UILabel!
    @IBOutlet weak var postComments: UILabel!
    @IBOutlet weak var postLikeButton: UIButton!
    @IBOutlet weak var postUnlikeButton: UIButton!
    @IBOutlet weak var postCommentButton: UIButton!
    @IBOutlet weak var postMoreButton: UIButton!
    @IBOutlet weak var postShareButton: UIButton!
    @IBOutlet weak var btnGif: UIButton!
    @IBOutlet weak var imgGif: GIFImageView!
    @IBOutlet weak var viewGif: VideoPlayer!
    @IBOutlet weak var longView: BorderView!
    @IBOutlet weak var viewSafe: UIView!
    @IBOutlet weak var viewFullPost: UILabel!
    
    var postLink: String!

}
