//
//  Message.swift
//  gag
//
//  Created by Hieu Nguyen on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import Foundation
import UIKit

let date = NSDate()
let calendar = NSCalendar.current

class Message:NSObject
{
    var imgAvatar:UIImageView! = nil
    var username:NSString = "" // use as messageID
    var textMessage:NSString = ""
    var isOP:Bool = false
    var points:Int = 0
    var timer:NSString = ""
    var numofcmt:Int = 0
    var isReplyMessage:Bool = false
    
}
