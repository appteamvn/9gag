//
//  ParentVideosViewController.swift
//  gag
//
//  Created by buisinam on 11/13/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ParentVideosViewController: BasePageViewController {

    var destinationVC: VideosPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("videos", comment: "")

        // Do any additional setup after loading the view.
        
        arrTitleSegment = (VideoDao.VideoDaoSingleton.sharedInstance.categoryVideo as NSArray).value(forKey: "category_name") as! [String]
        arrTitleSegment.insert(NSLocalizedString("home", comment: ""), at: 0)
        
        // Setup segmentview
        setUpdateSegmentView(isEqual: false, padding: 10.0)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func segmentSelection(selection: Int) {
        destinationVC.segmentSelection(selection: selection)
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let vc = segue.destination as? VideosPageViewController {
            destinationVC = vc
            destinationVC.parentVC = self
        }
    }
    
    override func setSelectedSegment(index: Int) {
        super.setSelectedSegment(index: index)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
