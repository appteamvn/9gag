//
//  ProfileNotificationTableView.swift
//  gag
//
//  Created by ThanhToa on 11/22/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileNotificationTableView: UITableViewController {
    let section = [NSLocalizedString("notification_session", comment: "")]
    let items = [[NSLocalizedString("upvote_your_comment", comment: ""),
                  NSLocalizedString("milestone_post_point", comment: ""),
                  NSLocalizedString("milestone_overall_point", comment: ""),
                  NSLocalizedString("milestone_cmt_point", comment: ""),
                  NSLocalizedString("mention_in_comment", comment: ""),
                  NSLocalizedString("your_friend_join", comment: ""),
                  NSLocalizedString("milestones_overall_cmt", comment: ""),
                  NSLocalizedString("upvote_your_post", comment: ""),
                  NSLocalizedString("report_cmt_remove", comment: ""),
                  NSLocalizedString("milestone_post_cmt", comment: ""),
                  NSLocalizedString("downvote_cmt_remove", comment: ""),
                  NSLocalizedString("milestone_cmt_reply", comment: ""),
                  NSLocalizedString("feature_your_post", comment: ""),
                  NSLocalizedString("reply_your_cmt", comment: "")]]
    let notifyControllers = NotificationsController.sharedInstance
    let defaults = UserDefaults.standard
    var refresh = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("notifications", comment: "")
        if defaults.bool(forKey: Common.NOT_LOGIN_NOTIFY) {
            self.tableView.alwaysBounceVertical = true
            refresh.tintColor = UIColor.white
            refresh.addTarget(self, action: #selector(loadData), for: .valueChanged)
            tableView.addSubview(refresh)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        if defaults.bool(forKey: Common.NOT_LOGIN_NOTIFY) {
             return 0
        } else {
            return self.section.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items [section].count
    }

    func loadData() {
        defaults.set(false, forKey: Common.NOT_LOGIN_NOTIFY)
        self.tableView.reloadData()
        refresh.endRefreshing()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifyCell", for: indexPath)
        let swt = UISwitch(frame: CGRect(x: 150, y: 300, width: 0, height: 0))
        cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
        cell.accessoryView = swt
        swt.addTarget(self, action: #selector(switchValueChanged(sender:)), for: UIControlEvents.valueChanged)
        switch indexPath.row {
        case 0:
            if self.notifyControllers.isUpVoteComent == true {
                swt.isOn = true
            }
            swt.tag = 100
            break
        case 1:
            if self.notifyControllers.isMilestonePostPoint == true {
                swt.isOn = true
            }
            swt.tag = 101
            break
        case 2:
            if self.notifyControllers.isMilestoneForOverallPoint == true {
               swt.isOn = true
            }
            swt.tag = 102
            break
        case 3:
            if self.notifyControllers.isMilestoneForCommentPoint == true {
                swt.isOn = true
            }
            swt.tag = 103
            break
        case 4:
            if self.notifyControllers.isMentitonYouAComment == true {
                swt.isOn = true
            }
            swt.tag = 104
            break
        case 5:
            if self.notifyControllers.isYourFriendJoin == true {
                swt.isOn = true
            }
            swt.tag = 105
            break
        case 6:
            if self.notifyControllers.isMiletonesForOverallComment == true {
               swt.isOn = true
            }
            swt.tag = 106
            break
        case 7:
            if self.notifyControllers.isUpvoteYourPost == true {
               swt.isOn = true
            }
            swt.tag = 107
            break
        case 8:
            if self.notifyControllers.isReportedCommentRemoved == true {
                swt.isOn = true
            }
            swt.tag = 108
            break
        case 9:
            if self.notifyControllers.isMilestoneForPostComment == true {
                swt.isOn = true
            }
            swt.tag = 109
            break
        case 10:
            if self.notifyControllers.isDownnVotedCommentRemoved == true {
                swt.isOn = true
            }
            swt.tag = 110
            break
        case 11:
            if self.notifyControllers.isMilestoneForCommentReply == true {
                swt.isOn = true
            }

            swt.tag = 111
            break
        case 12:
            if self.notifyControllers.isFeaturesPost == true {
                swt.isOn = true
            }
            swt.tag = 112
            break
        case 13:
            if self.notifyControllers.isReplyYourComment == true {
                swt.isOn = true
            }
            swt.tag = 113
            break
        default:
            break
        }
        
        return cell
    }
    
    func switchValueChanged(sender: Any){
        let swt = sender as! UISwitch
        switch swt.tag {
        case 100:
            self.notifyControllers.isUpVoteComent = swt.isOn
            if self.notifyControllers.isUpVoteComent == true {
                defaults.set(true, forKey: "DefaultUpVoteComent")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultUpVoteComent")
            }
            break
        case 101:
            self.notifyControllers.isMilestonePostPoint = swt.isOn
            if self.notifyControllers.isMilestonePostPoint == true {
                defaults.set(true, forKey: "DefaultMilestonePostPoint")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMilestonePostPoint")
            }
            break
        case 102:
            self.notifyControllers.isMilestoneForOverallPoint = swt.isOn
            if self.notifyControllers.isMilestoneForOverallPoint == true {
                defaults.set(true, forKey: "DefaultMilestoneForOverallPoint")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMilestoneForOverallPoint")
            }
            break
        case 103:
            self.notifyControllers.isMilestoneForCommentPoint = swt.isOn
            if self.notifyControllers.isMilestoneForCommentPoint == true {
                defaults.set(true, forKey: "DefaultMilestoneForCommentPoint")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMilestoneForCommentPoint")
            }
            break
        case 104:
            self.notifyControllers.isMentitonYouAComment = swt.isOn
            if self.notifyControllers.isMentitonYouAComment == true {
                defaults.set(true, forKey: "DefaultMentitonYouAComment")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMentitonYouAComment")
            }
            break
        case 105:
            self.notifyControllers.isYourFriendJoin = swt.isOn
            if self.notifyControllers.isYourFriendJoin == true {
                defaults.set(true, forKey: "DefaultYourFriendJoin")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultYourFriendJoin")
            }
            break
        case 106:
            self.notifyControllers.isMiletonesForOverallComment = swt.isOn
            if self.notifyControllers.isMiletonesForOverallComment == true {
                defaults.set(true, forKey: "DefaultMiletonesForOverallComment")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMiletonesForOverallComment")
            }
            break
        case 107:
            self.notifyControllers.isUpvoteYourPost = swt.isOn
            if self.notifyControllers.isUpvoteYourPost == true {
                defaults.set(true, forKey: "DefaultUpvoteYourPost")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultUpvoteYourPost")
            }
            break
        case 108:
            self.notifyControllers.isReportedCommentRemoved = swt.isOn
            if self.notifyControllers.isReportedCommentRemoved == true {
                defaults.set(true, forKey: "DefaultReportedCommentRemoved")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultReportedCommentRemoved")
            }
            break
        case 109:
            self.notifyControllers.isMilestoneForPostComment = swt.isOn
            if self.notifyControllers.isMilestoneForPostComment == true {
                defaults.set(true, forKey: "DefaultMilestoneForPostComment")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMilestoneForPostComment")
            }
            break
        case 110:
            self.notifyControllers.isDownnVotedCommentRemoved = swt.isOn
            if self.notifyControllers.isDownnVotedCommentRemoved == true {
                defaults.set(true, forKey: "DefaultDownnVotedCommentRemoved")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultDownnVotedCommentRemoved")
            }
            break
        case 111:
            self.notifyControllers.isMilestoneForCommentReply = swt.isOn
            if self.notifyControllers.isMilestoneForCommentReply == true {
                defaults.set(true, forKey: "DefaultMilestoneForCommentReply")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultMilestoneForCommentReply")
            }
            break
        case 112:
            self.notifyControllers.isFeaturesPost = swt.isOn
            if self.notifyControllers.isFeaturesPost == true {
                defaults.set(true, forKey: "DefaultFeaturesPost")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultFeaturesPost")
            }
            break
        case 113:
            self.notifyControllers.isReplyYourComment = swt.isOn
            if self.notifyControllers.isReplyYourComment == true {
                defaults.set(true, forKey: "DefaultReplyYourComment")
            }
            else
            {
                defaults.removeObject(forKey: "DefaultReplyYourComment")
            }
            break
        default:
            break
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section [section]
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = UIColor.lightGray
        headerView.textLabel?.font = UIFont.systemFont(ofSize: 15)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                break
            case 1:
                break
            case 2:
                break
            case 3:
                break
            case 4:
                break
            case 5:
                break
            case 6:
                break
            case 7:
                break
            case 8:
                break
            case 9:
                break
            case 10:
                break
            case 11:
                break
            case 12:
                break
            case 13:
                break
            case 14:
                self.dismiss(animated: true, completion: nil)
                break
            default:
                break
            }
            break
        default:
        break
        }
    }
}
