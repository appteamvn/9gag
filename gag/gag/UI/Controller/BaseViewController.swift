//
//  BaseViewController.swift
//  gag
//
//  Created by buisinam on 11/9/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var collView: UICollectionView!
    let refControl = UIRefreshControl()
    
    func refreshCall()
    {
        
    }
    func refresh(sender:AnyObject)
    {
        self.refControl.beginRefreshing()
        
        UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.collView?.contentOffset = CGPoint(x: 0, y: -200)
            
        }, completion: { (complete) in
            if complete == true{
                self.refreshCall()
            }
        })
        
        //        }
        // your refreshing logic here...
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        refControl.attributedTitle = NSAttributedString(string: "")
        refControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        self.collView?.addSubview(refControl)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
