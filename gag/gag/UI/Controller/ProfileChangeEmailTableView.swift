//
//  ProfileChangeEmailTableView.swift
//  gag
//
//  Created by ToaNT1 on Nov30.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileChangeEmailTableView: UITableViewController {
    let alert = UIAlertView()
    var userDefaults = UserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("change_email", comment: "")
        let saveItem = UIBarButtonItem(title: NSLocalizedString("save", comment: ""), style: .plain, target: self, action: #selector(saveChange))
        self.navigationItem.rightBarButtonItem = saveItem
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userDefaults = UserDefaults()
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileChangEmailCellIdentifier", for: indexPath) as! ProfileChangeEmailCell
        cell.changeEmailIcon.image = UIImage(named: "email")
        cell.changeEmailTextField.placeholder = NSLocalizedString("email", comment: "")
        if userDefaults.object(forKey: Common.USER_EMAIL) == nil {return cell}
        let email = userDefaults.object(forKey: Common.USER_EMAIL) as! String
        cell.changeEmailTextField.text = email
        return cell
    }
    // MARK: - saveChange
    func saveChange() {
            let cell = getCellAtIndexPath(section: 0, index: 0) as! ProfileChangeEmailCell //  ? equal with server
            let user_email = cell.changeEmailTextField.text
            if  !((user_email?.isEmpty)!) {
                if !Common().checkNetworkConnect() {
                    Common.showHubWithMessage(string: NSLocalizedString("error", comment: ""))
                } else {
                    self.userDefaults.setValue(user_email, forKey: Common.USER_EMAIL)
                    self.alert.title = NSLocalizedString("app_name", comment: "")
                    self.alert.message = NSLocalizedString("change_email_success", comment: "")
                    self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                    self.alert.show()
                }
            } else {
                self.alert.title = NSLocalizedString("app_name", comment: "")
                self.alert.message = NSLocalizedString("change_email_failed", comment: "")
                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                self.alert.show()
            }
    }
    
    func getCellAtIndexPath(section : Int,index : Int ) -> UITableViewCell
    {
        let indexPath = IndexPath(item: index, section: section
        )
        let cell = self.tableView.cellForRow(at: indexPath)
        return cell!
    }

}
