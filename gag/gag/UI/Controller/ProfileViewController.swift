//
//  FirstViewController.swift
//  gag
//
//  Created by buisinam on 10/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import LGSemiModalNavController
import MEVFloatingButton
import Cosmos
import Kingfisher
import AJRNight
import Photos
import AssetsLibrary

class ProfileViewController: UITableViewController, UIAlertViewDelegate, MEVFloatingButtonDelegate, ActionSheetAddButtonTimeLineDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,  AJRNightDelegate{
    @IBOutlet weak var profileAvatar: UIImageView!
    @IBOutlet weak var proviewViewBtn: UIButton!
    @IBOutlet weak var profileAccountName: UILabel!
    @IBOutlet weak var profileEditBtn: UIButton!
    
    // add button
    let addButton = MEVFloatingButton()
    // Actionsheet
    var actionSheetView:ActionSheetAddButtonTimeLine! = nil
    // Photo framework
    @IBOutlet var tableViewProfile: UITableView!
    var tapGestureProfileAvatar = UITapGestureRecognizer()
    var userDefaults = UserDefaults()
    let reachability = Reachability()
    let isAuthorizePhoto = PHPhotoLibrary.authorizationStatus()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor (red: 235, green: 235, blue: 241, alpha: 1)
        // add button
        self.setUpAddButton()
        
        tapGestureProfileAvatar = UITapGestureRecognizer(target:self, action:#selector(tappedOnProfileAvatar(sender:)))
        tapGestureProfileAvatar.numberOfTapsRequired  = 1
        profileAvatar.isUserInteractionEnabled = true
        profileAvatar.addGestureRecognizer(tapGestureProfileAvatar)
        
        let saveItem = UIBarButtonItem(title: NSLocalizedString("get_pro_version", comment: "version_no_ad"), style: .plain, target: self, action: #selector(showGettingProVersion))
        self.navigationItem.rightBarButtonItem = saveItem
        self.navigationItem.title = NSLocalizedString("profile", comment: "")
        self.view.nightProperties = ["backgroundColor" : UIColor.black]
        UILabel.appearance().nightProperties = ["textColor": UIColor.white]
        AJRNight.sharedClient().addViewController(self)
        
        proviewViewBtn.setTitle(NSLocalizedString("view_profile", comment: ""), for: .normal)
        self.navigationItem.title = NSLocalizedString("profile", comment: "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkLogin()
        profileAvatar.layer.masksToBounds = true
        profileAvatar.layer.cornerRadius = profileAvatar.bounds.size.width / 2
        if userDefaults.object(forKey: Common.USER_ID) == nil ||
            userDefaults.object(forKey: Common.USER_EMAIL) == nil ||
            userDefaults.object(forKey: Common.USER_NAME) == nil {return}
        let user_id = BaseDAO.getUserId()
        if (reachability?.isReachable)! {
            if user_id != 0 {
                self.setViewData()
            }
        }
        // night mode
        if self.userDefaults.bool(forKey: Common.DEFAULT_AUTO_NIGHT_MODE) {
            AJRNight.sharedClient().isNight = true
        }
        if self.userDefaults.object(forKey: Common.USER_AVATAR) == nil {
            self.profileAvatar.image = UIImage(named:"use_default_icon@1x")
        }
    }
    
    func checkLogin() {
        if userDefaults.object(forKey: Common.USER_ID) == nil {
            let join9GagViewController = self.storyboard?.instantiateViewController(withIdentifier: "Join9GagViewController") as! Join9GagViewController
            self.show(join9GagViewController, sender: nil)
        } else {
            
        }
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        let profileEditTableView = self.storyboard?.instantiateViewController(withIdentifier: "editProfilesViewControllerIdentifier")  as! EditProfilesViewController
        self.show(profileEditTableView, sender: true)
    }
    func setViewData() {
        let user_name = userDefaults.object(forKey: Common.USER_NAME) as! String
        profileAccountName.text = user_name
        if userDefaults.object(forKey: Common.USER_AVATAR) != nil {
            let urlString = userDefaults.object(forKey: Common.USER_AVATAR) as! String
            if !urlString.isEmpty && urlString != "" {
                let url = URL(string: urlString)
                
                profileAvatar.kf.setImage(with:ImageResource(downloadURL: url!,cacheKey: nil))
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let vc = segue.destination as? ParentHomeViewController {
            vc.isProfile = true
        }
    }
    
    //    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
    //        switch indexPath.row {
    //        case 0:
    //            break
    //        default:
    //            break
    //        }
    //        return cell
    //    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
            parentHomeViewController.isProfile = true
            parentHomeViewController.selectedIndex = 1
            self.show(parentHomeViewController, sender: nil)
            break
        case 2:
            let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
            parentHomeViewController.isProfile = true
            parentHomeViewController.selectedIndex = 2
            self.show(parentHomeViewController, sender: nil)
            break
        case 3:
            let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
            parentHomeViewController.isProfile = true
            parentHomeViewController.selectedIndex = 3
            self.show(parentHomeViewController, sender: nil)
            break
        case 5:
            let settingView = self.storyboard?.instantiateViewController(withIdentifier: "profileSettingsTableViewControllerIdentifier") as! ProfileSettingsTableViewController
            self.show(settingView, sender: nil)
            break
        case 6:
            let actionSheetSettings = UIAlertController(title: NSLocalizedString("feedback_send", comment: ""), message: nil, preferredStyle: .alert)
            actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_suggest", comment: ""), style: .default, handler: {(UIAlertAction)in
                let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                self.show(feedBackTableView, sender: nil)
                print("press suggest")
            }))
            actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_report", comment: ""), style: .default, handler: {(UIAlertAction)in
                let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                self.show(feedBackTableView, sender: nil)
                print("press report")
            }))
            actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("feedback_rate", comment: ""), style: .default, handler: {(UIAlertAction)in
                self.showRatingAlert()
                print("press rate")
            }))
            actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction)in
                print("press cancel")
            }))
            self.present(actionSheetSettings, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    //MARK: ADD BUTTON
    /* add button */
    func setUpAddButton() {
        addButton.displayMode = MEVFloatingButtonDisplayMode.always
        addButton.position = MEVFloatingButtonPosition.bottomRight
        addButton.image = UIImage(named: "add-button")
        addButton.outlineWidth = 0.0;
        addButton.imagePadding = 30.0
        addButton.horizontalOffset = 13.0;
        addButton.verticalOffset = -33.0;
        self.tableViewProfile .setFloatingButton(addButton)
        self.tableViewProfile.floatingButtonDelegate = self
    }
    
    func floatingButton(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        if !Common().checkNetworkConnect() {
            Common.showAlert(title: NSLocalizedString("error_network", comment: ""),
                             message: NSLocalizedString("error_network_message", comment: ""),
                             titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                print("cancel")
            })
        } else {
            if(isAuthorizePhoto == PHAuthorizationStatus.authorized) {
                actionSheetView = ActionSheetAddButtonTimeLine()
                let actionController = LGSemiModalNavViewController(rootViewController: actionSheetView)
                actionController.view.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: 224)
                actionController.navigationController?.setNavigationBarHidden(true, animated: true)
                actionController.animationSpeed = 0.35
                actionController.backgroundShadeColor = UIColor.black
                actionController.isTapDismissEnabled = true
                actionSheetView.delegateButton = self
                self.present(actionController, animated: true, completion: nil)
            } else {
                PHPhotoLibrary.requestAuthorization() {statusPhoto in
                    switch statusPhoto {
                    case .authorized:
                        self.actionSheetView = ActionSheetAddButtonTimeLine()
                        let actionController = LGSemiModalNavViewController(rootViewController: self.actionSheetView)
                        actionController.view.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: 224)
                        actionController.navigationController?.setNavigationBarHidden(true, animated: true)
                        actionController.animationSpeed = 0.35
                        actionController.backgroundShadeColor = UIColor.black
                        actionController.isTapDismissEnabled = true
                        self.actionSheetView.delegateButton = self
                        self.present(actionController, animated: true, completion: nil)
                        break
                    case .denied, .restricted:
                        let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                            if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                                UIApplication.shared.openURL(appSettings as URL)
                            }
                        }
                        alertController.addAction(settingsAction)
                        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                        alertController.addAction(cancelAction)
                        DispatchQueue.main.async {
                            self.present(alertController, animated: true, completion: nil)
                        }
                        break
                    case .notDetermined: break
                        // won't happen but still
                    }
                }
            }
        }
    }
    // add button action
    func btnCameraAction() {
        let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        if(cameraAuthorizationStatus == .authorized)
        {
            let cameraUI = UIImagePickerController()
            cameraUI.sourceType = UIImagePickerControllerSourceType.camera
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                if((self.presentationController) != nil) {
                    self.actionSheetView.dismiss(animated: true, completion: nil)
                    self.present(imagePicker, animated: true, completion: nil)
                }
            }
        } else {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                if granted {
                    let cameraUI = UIImagePickerController()
                    cameraUI.sourceType = UIImagePickerControllerSourceType.camera
                    
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                        imagePicker.allowsEditing = false
                        if((self.presentationController) != nil) {
                            self.actionSheetView.dismiss(animated: true, completion: nil)
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                    }
                } else {
                    if self.actionSheetView != nil {
                        self.actionSheetView.dismiss(animated: true, completion: nil)
                    }
                    let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(appSettings as URL)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    func btnGalleryAction() {
        if(isAuthorizePhoto == PHAuthorizationStatus.authorized)
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imageChooser = UIImagePickerController()
                imageChooser.delegate = self
                imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imageChooser.allowsEditing = true
                if((self.presentationController) != nil) {
                    self.actionSheetView.dismiss(animated: true, completion: nil)
                    self.present(imageChooser, animated: true, completion: nil)
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization() {statusPhoto in
                switch statusPhoto {
                case .authorized:
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imageChooser = UIImagePickerController()
                        imageChooser.delegate = self
                        imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imageChooser.allowsEditing = true
                        if((self.presentationController) != nil) {
                            self.actionSheetView.dismiss(animated: true, completion: nil)
                            self.present(imageChooser, animated: true, completion: nil)
                        }
                    }
                    break
                case .denied, .restricted:
                    Common.showAlert(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("accept_access_photo_message", comment: ""), titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                        print("not accept photo")
                    })
                    break
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
        
    }
    
    func btnCancelAction() {
        self.actionSheetView.dismiss(animated: true
            , completion: nil)
    }
    
    func didSelectedImageAction(imageSelected: UIImage) {
        let postViewController = self.storyboard?.instantiateViewController(withIdentifier: "postTableViewControllerIdentifier") as! PostTableViewController
        postViewController.postImageReceived = imageSelected
        self.actionSheetView.dismiss(animated: true) {
            let nav = UINavigationController(rootViewController: postViewController)
            self.navigationController?.tabBarController?.present(nav, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let common = Common()
        let postViewController = self.storyboard?.instantiateViewController(withIdentifier: "postTableViewControllerIdentifier") as! PostTableViewController
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let scaledImage = common.scaleImageWidth(image: pickedImage, targetSize: CGSize(width: 750, height: 1334))
            postViewController.postImageReceived = scaledImage
        }
        dismiss(animated: true) {
            let nav = UINavigationController(rootViewController: postViewController)
            self.navigationController?.tabBarController?.present(nav, animated: true, completion: nil)
        }
    }
    /* add button */
    func showRatingAlert() {
        let ratingAlertViewController = UIAlertController(title: NSLocalizedString("feedback_tell", comment: ""),
                                                          message: NSLocalizedString("feedback_rate_star", comment: ""),
                                                          preferredStyle: .alert)
        let ratingView = CosmosView()
        ratingView.frame = CGRect(x: 0, y: 0, width: ratingAlertViewController.view.frame.width - 40, height: 30)
        let xCoord = ratingAlertViewController.view.frame.width / 2 - ratingView.frame.width / 3 - 15
        let yCoord = CGFloat(85.0)
        ratingView.rating = 0.0
        ratingView.settings.starSize = 30
        ratingView.settings.emptyBorderColor = UIColor.black
        ratingView.settings.updateOnTouch = true
        ratingView.frame.origin.x = xCoord
        ratingView.frame.origin.y = yCoord
        ratingAlertViewController.view.addSubview(ratingView)
        let cancelAction = UIAlertAction(title: NSLocalizedString("feedback_not_now", comment: ""), style: .cancel) { (action) in
        }
        ratingAlertViewController.addAction(cancelAction)
        self.present(ratingAlertViewController, animated: true, completion: nil)
        ratingView.didFinishTouchingCosmos = {
            rating in
            ratingAlertViewController.dismiss(animated: true, completion: {Void in
                if ratingView.rating < 4.0 {
                    let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
                    feedBackTableView.isRating = true
                    self.show(feedBackTableView, sender: nil)
                } else {
                    self.processAfterRating()
                }
            })
        }
    }
    
    func tappedOnProfileAvatar(sender: UITapGestureRecognizer) {
        let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
        parentHomeViewController.isProfile = true
        self.show(parentHomeViewController, sender: nil)
    }
    
    func showGettingProVersion() {
        let common = Common()
        if !common.checkNetworkConnect() {
            Common.showAlert(title: NSLocalizedString("error", comment: ""),
                             message: NSLocalizedString("can_not_connect_itune", comment: ""),
                             titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                                print("cancel")
            })
            return
        }
        let message = NSLocalizedString("get_pro_version_msg", comment: "")
        let alertGetProversion = UIAlertController(title: NSLocalizedString("unlock_pro_version", comment: ""), message: message, preferredStyle: .alert)
        
        alertGetProversion.addAction(UIAlertAction(title: NSLocalizedString("unlock_pro_version_price", comment: ""), style: .default, handler: {(UIAlertAction) in
            print("press Unlock")
        }))
        alertGetProversion.addAction(UIAlertAction(title: NSLocalizedString("restore_purchase", comment: ""), style: .default, handler: {(UIAlertAction) in
            print("press Purchase")
        }))
        alertGetProversion.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction) in
            print("press Cancel")
        }))
        self.present(alertGetProversion, animated: true, completion: nil)
    }
    
    open func gotoProfiles(index: Int)
    {
        let parentHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
        parentHomeViewController.isProfile = true
        parentHomeViewController.selectedIndex = index
        self.show(parentHomeViewController, sender: nil)
    }
    
    func processAfterRating() {
        let alertController = UIAlertController(title: NSLocalizedString("enjoy_app", comment: ""),
                                                message: NSLocalizedString("rating_messase", comment: ""), preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: NSLocalizedString("rate_app", comment: ""), style: .default) { (alertAction) in
            let whatNewsURL = NSURL(string: NSLocalizedString("app_download_link", comment: ""))
            UIApplication.shared.openURL(whatNewsURL as! URL)
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("not_now", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

