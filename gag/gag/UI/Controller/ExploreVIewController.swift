//
//  FirstViewController.swift
//  gag
//
//  Created by buisinam on 10/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import Kingfisher

class ExploreVIewController:UITableViewController,UISearchResultsUpdating,UICollectionViewDelegate,UICollectionViewDataSource, UISearchBarDelegate,ExplorerCollectionHeaderDelegate,UISearchControllerDelegate, ExploreListCellClickDelegate{
    
    var dataArray = [String]()
    
    var filteredArray:[AnyObject] = []
    
    var shouldShowSearchResults = false
    
    var searchController: UISearchController!
    var isBeginSearch = false
    
    @IBOutlet weak var featuredLabel: UILabel!
    
    @IBOutlet weak var seaallButton: UIButton!
    
    @IBOutlet weak var collectionHeader: UICollectionView!
    
    lazy var collectionManager = ExplorerCollectionHeader()
    
    var arrayCategory : [CategoryResponse] = []
    var arrayFeatures : [PostResponse] = []
    var userDefaults = UserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Delegate CollectionViewControllers
        collectionHeader.delegate = self
        collectionHeader.dataSource  = self
        self.tableView.tableFooterView = UIView()
        featuredLabel.text = NSLocalizedString("featured", comment: "")
        seaallButton.setTitle(NSLocalizedString("see_all", comment: ""), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // SearchViewController
        configureSearchController()
        getFeaturesList()
        if self.arrayCategory.count == 0 {
            getCategoryList()
        }
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        if searchController != nil {
        searchController.searchBar.removeFromSuperview()
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCategoryList()
    {
        PostDAO.getCategoryPost { (success, data: [CategoryResponse]?) in
            if success{
                self.arrayCategory = data!
                let dataCate = CategoryResponse()
                dataCate.category_name = NSLocalizedString("gif", comment: "")
                dataCate.category_id = -1
                self.arrayCategory.append(dataCate)
                self.tableView.reloadData()
            }
        }
    }
    
    func getFeaturesList()
    {
        PostDAO.getFeaturePost { (success, data: [PostResponse]?) in
            if success{
                self.arrayFeatures = data!
                self.collectionHeader.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else {
            return Int(ceil(Double(arrayCategory.count) / 2.0))
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if shouldShowSearchResults {
            return 0
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExploreListTableViewCell", for: indexPath) as! ExploreListTableViewCell
            cell.tag = indexPath.row
        cell.delegate = self
        for i in (indexPath.row * 2)..<(indexPath.row * 2 + 2)
        {
            if i < arrayCategory.count{
                cell.arrayString.append(arrayCategory[i])
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 45))
        var frame  = headerView.bounds
        frame.origin.x = 10
        let title = UILabel(frame: frame)
        title.textAlignment = .left
        title.text = NSLocalizedString("section", comment: "")
        title.textColor = UIColor.lightGray
        headerView.addSubview(title)
        headerView.backgroundColor = UIColor("#F3F3F3")
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if arrayCategory.count != 0{
            return 45.0
        }
        else{
            return 0.0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: Custom functions
    
    func loadListOfCountries() {
        // Specify the path to the countries list file.
        let pathToFile = Bundle.main.path(forResource: "countries", ofType: "txt")
        
        if let path = pathToFile {
            // Load the file contents as a string.
            let countriesString = try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
            
            // Append the countries from the string to the dataArray array by breaking them using the line change character.
            dataArray = countriesString.components(separatedBy: "\n")
            
            // Reload the tableview.
            self.tableView.reloadData()
        }
    }
    
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        if searchController == nil{
            searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.placeholder = NSLocalizedString("search_here", comment: "")
            searchController.searchBar.delegate = self
            searchController.delegate = self
            searchController.hidesNavigationBarDuringPresentation = false
            searchController.definesPresentationContext = true
            searchController.searchBar.barTintColor = UIColor()
            searchController.searchBar.sizeToFit()
            searchController.searchBar.frame = CGRect(x: 5.0, y: 10.0, width: ((self.navigationController?.navigationBar.frame.size.width)! - 10.0), height: (self.navigationController?.navigationBar.frame.size.height)! - 10.0)
            if let index = indexOfSearchFieldInSubviews() {
                // Access the search field
                let searchField: UITextField = (searchController.searchBar.subviews[0] ).subviews[index] as! UITextField
                
                // Set the background color of the search field.
                searchField.backgroundColor = UIColor("#E4E4E4")
            }
        }
        self.navigationController?.navigationBar.addSubview( searchController.searchBar)
    }
    
    // MARK: UISearchBarDelegate functions
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        shouldShowSearchResults = true
        //        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
            if let subView = self.view.viewWithTag(101) {
                subView.removeFromSuperview()
            }
        
        shouldShowSearchResults = false
        self.tableView.reloadData()
        self.collectionHeader.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            self.tableView.reloadData()
        }
        let vcDetail = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
        vcDetail.searchData = searchBar.text!
        vcDetail.isSearch = true
        vcDetail.view.tag = 101
        self.view.addSubview(vcDetail.view)
        self.addChildViewController(vcDetail)
    }
    
    // MARK: UISearchResultsUpdating delegate function
    
    func updateSearchResults(for searchController: UISearchController) {
        if (searchController.searchBar.text?.characters.count)! > 0 && !shouldShowSearchResults
        {
            shouldShowSearchResults = true
            seaallButton.isHidden = true
            featuredLabel.isHidden = true
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            self.tableView.reloadData()
            self.collectionHeader.reloadData()
        } else if (searchController.searchBar.text?.characters.count)! == 0 {
            shouldShowSearchResults = false
            seaallButton.isHidden = false
            featuredLabel.isHidden = false
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
            self.tableView.reloadData()
            self.collectionHeader.reloadData()
        }
    }
    
    func indexOfSearchFieldInSubviews() -> Int! {
        // Uncomment the next line to see the search bar subviews.
        // println(subviews[0].subviews)
        
        var index: Int!
        let searchBarView = searchController.searchBar.subviews[0]
        
        for i in 0 ..< searchBarView.subviews.count {
            if searchBarView.subviews[i].isKind(of: UITextField.self) {
                index = i
                break
            }
        }
        
        return index
    }
    
    @IBAction func actionSeeAll(_ sender: AnyObject) {
        let featuredCollectionView = self.storyboard?.instantiateViewController(withIdentifier: "featuredCollectionViewIdentifier") as! FeaturedCollectionView
        featuredCollectionView.arrayFeatures = self.arrayFeatures
        self.show(featuredCollectionView, sender: nil)
    }
    
    //MARK: CollectionHeader Delegate
    func cellForCollectionView(collectionViewCell: UICollectionViewCell?, cellForItemAt indexPath: Int) {
        
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return 0
        }
        return arrayFeatures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let data = arrayFeatures[indexPath.item]
        let cellIdentifier = "FadeCell"
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! ExplorerHeaderViewCell
        cell.lblTitle.text = data.post_title
        cell.lblPoint.text =  "\(data.post_point)" + NSLocalizedString("point", comment: "") + data.post_comment + NSLocalizedString("comment", comment: "")
        if let url: URL = URL(string: data.url){
            cell.imgAvatar.kf.setImage(with: ImageResource(downloadURL: url,cacheKey: nil))
        }
        cell.contentView.layer.cornerRadius = 3
        cell.contentView.layer.masksToBounds = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell  = collectionView.cellForItem(at: indexPath) as! ExplorerHeaderViewCell
        
        let data = arrayFeatures[indexPath.item]
        let vc = CommentTableViewController()
        vc.post_id = data.post_id
        vc.imgAvatar = cell.imgAvatar.image
        vc.data = data
        vc.isDetail = true
        self.show(vc, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.size.width * 0.7, height: collectionView.frame.size.height * 0.9)
    }

    //MARK: - ExploreListCellClickDelegate
    func clickItemAtIndex(index: Int, cell: UITableViewCell) {
        var indexItem = 0
        if cell.tag == 0{
            indexItem = index
        }
        else{
            indexItem = cell.tag * 2 + index
        }
        
        let data  = self.arrayCategory[indexItem]
        let vcDetailExplore = self.storyboard?.instantiateViewController(withIdentifier: "parentHomeViewControllerIdentifier") as! ParentHomeViewController
        vcDetailExplore.isExplore = true
        vcDetailExplore.caterogy_id = data.category_id as Int
        vcDetailExplore.titleNavigation = data.category_name
        self.navigationController?.pushViewController(vcDetailExplore, animated: true)

    }
    
}
