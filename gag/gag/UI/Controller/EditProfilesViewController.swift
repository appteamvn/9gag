//
//  EditProfilesViewController.swift
//  gag
//
//  Created by Hieu Nguyen on 11/15/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import Photos
import Kingfisher

class EditProfilesViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, SCPopDatePickerDelegate{
    let section = [" "," "]
    var editProfileImagesArray = [["user", "name", "name","sex", "birthday", "info"],
                                  ["lock", "email"]]
    var editProfileTitleArray = [[NSLocalizedString("change_profile_picture", comment: ""), "",  "", "",
                                  "",  ""],
                                 [NSLocalizedString("change_password", comment: ""),
                                  NSLocalizedString("change_email", comment: "")]]
    // picker
    var chooseGenderPicker = UIPickerView()
    let pickerData = [NSLocalizedString("gender_other", comment: ""),
                      NSLocalizedString("gender_male", comment: ""),
                      NSLocalizedString("gender_female", comment: "")]
    var pickedGender: String = ""
    var userDefaults = UserDefaults()
    let imageChanged = UIImage()
    let alert = UIAlertView()
    let date = Date()
    let datePicker = SCPopDatePicker()
    let dateFormat = DateFormatter()
    let isAuthorizePhoto = PHPhotoLibrary.authorizationStatus()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("save", comment: ""), style: .plain, target: self, action: #selector(btn_SaveClicked(sender:)))
        self.navigationItem.title = NSLocalizedString("profile", comment: "")
        self.view.backgroundColor = UIColor(colorLiteralRed: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
    }

    func btn_SaveClicked(sender: UIBarButtonItem) {
        if !Common().checkNetworkConnect() {
            Common.showHubWithMessage(string: NSLocalizedString("couldnt_remove_picture", comment: ""))
        } else {
            let cell1 = self.getCellAtIndexPath(section: 0, index: 1) as! ProfileEditCell
            let cell2 = self.getCellAtIndexPath(section: 0, index: 1) as! ProfileEditCell
            let cell3 = self.getCellAtIndexPath(section: 0, index: 3) as! ProfileEditCell
            let cell4 = self.getCellAtIndexPath(section: 0, index: 4) as! ProfileEditCell
            if userDefaults.object(forKey: Common.USER_ID) != nil &&  userDefaults.object(forKey: Common.SECRET_KEY) != nil {
                let user_id = BaseDAO.getUserId()
                let secret_key = BaseDAO.getSecretKey()
                let full_name = cell1.editProfileTextField.text
                let user_name = cell2.editProfileTextField.text
                let user_gender = cell3.editProfileTextField.text
                var gender = 3
                let user_birth_day = cell4.editProfileTextField.text
                if (user_name?.isEmpty)! && (full_name?.isEmpty)! {
                    showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("error_nil_fullname", comment: ""))
                } else if (user_name?.isEmpty)! {
                    showAlertMessage(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("error_nil_user_name", comment: ""))
                } else {
                    let user_email = userDefaults.object(forKey: Common.USER_EMAIL) as! String
                    if user_gender == NSLocalizedString("gender_other", comment: "") {
                        gender = 0
                    } else if user_gender == NSLocalizedString("gender_male", comment: ""){
                        gender = 2
                    } else if user_gender == NSLocalizedString("gender_female", comment: ""){
                        gender = 1
                    }
                    UserDAO.updateProfile(user_id: String(user_id),
                                          secret_key: secret_key,
                                          gender: gender,
                                          birhday: user_birth_day!,
                                          country: "German",
                                          user_display_name: full_name!,
                                          nsfw: 0,
                                          user_email: user_email,
                                          completeHandle: {(success: Bool, _ data:[UserResponse]?) in
                                            if success {
                                                self.userDefaults.setValue(full_name, forKey: Common.USER_NAME)
                                                self.userDefaults.setValue(gender, forKey: Common.USER_GENDER)
                                                self.userDefaults.setValue(user_birth_day, forKey: Common.USER_DOB)
                                                self.alert.title = NSLocalizedString("app_name", comment: "")
                                                self.alert.message = NSLocalizedString("update_profile_success", comment: "")
                                                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                                                self.alert.show()
                                            } else {
                                                self.alert.title = NSLocalizedString("app_name", comment: "")
                                                self.alert.message = NSLocalizedString("update_profile_failed", comment: "")
                                                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                                                self.alert.show()
                                                cell1.editProfileTextField.text = self.userDefaults.object(forKey: Common.USER_NAME) as? String
                                                cell2.editProfileTextField.text = self.userDefaults.object(forKey: Common.USER_NAME) as? String
                                            }
                    })
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return  self.section.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.editProfileTitleArray [section].count
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = UIColor.lightGray
        headerView.textLabel?.font = UIFont.systemFont(ofSize: 14)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileEditCellIdentifier", for: indexPath) as! ProfileEditCell
        cell.editProfileImage.image = UIImage(named: self.editProfileImagesArray[indexPath.section][indexPath.row])
        cell.editProfileTextField.text =  self.editProfileTitleArray[indexPath.section][indexPath.row]
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                cell.editProfileTextField.isUserInteractionEnabled = false
                if userDefaults.object(forKey: Common.USER_AVATAR) != nil {
                    let urlString = userDefaults.object(forKey: Common.USER_AVATAR) as! String
                    let url = URL(string: urlString)
                    if url == nil {
                        return cell
                    }
                    cell.editProfileImage.kf.setImage(with: ImageResource(downloadURL: url!,cacheKey: url?.absoluteString))
                }
                return cell
            case 1:
                if userDefaults.object(forKey: Common.USER_NAME) != nil {
                    let accountName = userDefaults.object(forKey: Common.USER_NAME) as! String
                    cell.editProfileTextField.text = accountName
                }
                cell.editProfileTextField.placeholder = NSLocalizedString("full_name", comment: "")
                return cell
            case 2:
                if userDefaults.object(forKey: Common.USER_NAME) != nil {
                    let accountName = userDefaults.object(forKey: Common.USER_NAME) as! String
                    cell.editProfileTextField.text = accountName
                }
                cell.editProfileTextField.placeholder = NSLocalizedString("user_name", comment: "")
                return cell
            case 3:
                cell.editProfileTextField.isUserInteractionEnabled = false
                cell.editProfileTextField.placeholder = NSLocalizedString("gender", comment: "")
                if userDefaults.integer(forKey: Common.USER_GENDER) != 3 {
                    switch userDefaults.integer(forKey: Common.USER_GENDER) {
                    case 0:
                        cell.editProfileTextField.text  = NSLocalizedString("gender_other", comment: "")
                        break
                    case 1:
                        cell.editProfileTextField.text  = NSLocalizedString("gender_female", comment: "")
                        break
                    case 2:
                        cell.editProfileTextField.text  = NSLocalizedString("gender_male", comment: "")
                        break
                    default:
                        break
                    }
                }
                
                return cell
            case 4:
                if (pickedGender.characters.count > 0) {
                    cell.editProfileTextField.text = pickedGender
                }
                cell.editProfileTextField.placeholder = NSLocalizedString("birthday", comment: "")
                cell.editProfileTextField.isUserInteractionEnabled = false
                if userDefaults.object(forKey: Common.USER_DOB) != nil {
                    cell.editProfileTextField.text = userDefaults.object(forKey: Common.USER_DOB) as! String?
                }
                return cell
            case 5:
                cell.editProfileTextField.placeholder = NSLocalizedString("bio", comment: "")
                cell.editProfileTextField.text = NSLocalizedString("my_funny_collection", comment: "")
                return cell
            default:
                break
            }
        } else {
            switch indexPath.row {
                case 0:
                cell.editProfileTextField.isUserInteractionEnabled = false
                cell.accessoryType = .disclosureIndicator
                return cell
            case 1:
                cell.accessoryType = .disclosureIndicator
                cell.editProfileTextField.isUserInteractionEnabled = false
                return cell
            default:
                break
            }
        }
        return UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                return 55
            case 7:
                return 40
            default:
                break
            }
        }
        return 40
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if indexPath.row == 0 {
                let profileChangPasswordTableView = self.storyboard?.instantiateViewController(withIdentifier: "profileChangPasswordTableViewIdentifier")  as! ProfileChangPasswordTableView
                self.show(profileChangPasswordTableView, sender: nil)
            } else {
                let profileChangeEmailTableView = self.storyboard?.instantiateViewController(withIdentifier: "profileChangeEmailTableView")  as! ProfileChangeEmailTableView
                self.show(profileChangeEmailTableView, sender: nil)
            }
        } else {
            switch indexPath.row {
            case 0:
                self.showActionSheetChangeProfilePicture()
                break
            case 1:
                self.dismiss(animated: true, completion: nil)
                break
            case 2:
                self.dismiss(animated: true, completion: nil)
                break
            case 3:
                // picker gender
                self.showChooseGenderPicker()
                break
            case 4:
                self.showChooseDOBPicker()
                break
            case 5:
                self.dismiss(animated: true, completion: nil)
                break
            default:
                break
            }
        }
    }
    
    func showActionSheetChangeProfilePicture() {
        let actionSheetSettings = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionRemovePicture = UIAlertAction(title: "Remove Current Photo", style: .default, handler:  {(UIAlertAction) in
            if !Common().checkNetworkConnect() {
                Common.showHubWithMessage(string: NSLocalizedString("couldnt_remove_picture", comment: ""))
            } else {
                self.userDefaults.removeObject(forKey: Common.USER_AVATAR)
                self.tableView.reloadData()
            }
            print("press Remove Photo")
        })
        actionRemovePicture.setValue(UIColor.red, forKey: "titleTextColor")
        actionSheetSettings.addAction(actionRemovePicture)
        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("take_photo", comment: ""), style: .default, handler: {(UIAlertAction)in
            self.btnCameraAction()
            print("press Take photo")
        }))
        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("choose_from_camera_roll", comment: ""), style: .default, handler: {(UIAlertAction)in
            self.btnGalleryAction()
            print("press Camera Roll")
        }))
//        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("surprise_me", comment: ""), style: .default, handler: {(UIAlertAction)in
//            print("press Surprise")
//            self.surpriseMeAction()
//        }))
        actionSheetSettings.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: {(UIAlertAction)in
            print("press cancel")
        }))
        self.present(actionSheetSettings, animated: true, completion: nil)
    }
    
    func btnCameraAction() {
        let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        if(cameraAuthorizationStatus == .authorized) {
            let cameraUI = UIImagePickerController()
            cameraUI.sourceType = UIImagePickerControllerSourceType.camera
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                if((self.presentationController) != nil) {
                    //                self.actionSheetSettings.dismiss(animated: true, completion: nil)
                    self.present(imagePicker, animated: true, completion: nil)
                }
            }
        } else {
            //
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                if granted {
                    let cameraUI = UIImagePickerController()
                    cameraUI.sourceType = UIImagePickerControllerSourceType.camera
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                        imagePicker.allowsEditing = false
                        if((self.presentationController) != nil) {
                            //                self.actionSheetSettings.dismiss(animated: true, completion: nil)
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                    }
                } else {
                    let alertController = UIAlertController(title: "", message: NSLocalizedString("setting_photo_camera", comment: ""), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: NSLocalizedString("setting", comment: ""), style: .default) { (alertAction) in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(appSettings as URL)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
            //
        }
    }
    
    func btnGalleryAction() {
        if(isAuthorizePhoto == PHAuthorizationStatus.authorized) {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imageChooser = UIImagePickerController()
                imageChooser.delegate = self
                imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imageChooser.allowsEditing = true
                if((self.presentationController) != nil) {
                    self.present(imageChooser, animated: true, completion: nil)
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization() {statusPhoto in
                switch statusPhoto {
                case .authorized:
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imageChooser = UIImagePickerController()
                        imageChooser.delegate = self
                        imageChooser.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imageChooser.allowsEditing = true
                        if((self.presentationController) != nil) {
                            self.present(imageChooser, animated: true, completion: nil)
                        }
                    }
                    break
                case .denied, .restricted:
                    Common.showAlert(title: NSLocalizedString("app_name", comment: ""), message: NSLocalizedString("accept_access_photo_message", comment: ""), titleCancel: NSLocalizedString("ok", comment: ""), complete: {
                        print("not accept photo")
                    })
                    break
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
    }
    
    func surpriseMeAction() {
        self.alert.title = NSLocalizedString("app_name", comment: "")
        self.alert.message = NSLocalizedString("not_support_feature", comment: "")
        self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        self.alert.show()
    }
    // MARK: - GENDER + DOB
    // init picker
    func showChooseGenderPicker() {
        let alertGenderPicker = UIAlertController(title: nil, message: "\n\n\n\n", preferredStyle: .alert)
        alertGenderPicker.isModalInPopover = true
        let pickerFrame: CGRect = CGRect(x: 0, y: 0, width: 270, height: 150)
        chooseGenderPicker = UIPickerView(frame: pickerFrame)
        chooseGenderPicker.delegate = self
        chooseGenderPicker.dataSource = self
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertGenderPicker.addAction(ok)
        alertGenderPicker.view.addSubview(chooseGenderPicker)
        self.present(alertGenderPicker, animated: true, completion: nil)
    }
    
    
    // init datepicker
    func showChooseDOBPicker() {
        self.view.endEditing(true)
        self.datePicker.tapToDismiss = true
        self.datePicker.datePickerType = SCDatePickerType.date
        self.datePicker.datePickerStartDate = self.date
        self.datePicker.btnFontColour = .white
        self.datePicker.btnColour = .darkGray
        self.datePicker.showCornerRadius = true
        self.datePicker.showBlur = false
        self.datePicker.delegate = self
        self.datePicker.show(attachToView: self.view)
    }
    
    func scPopDatePickerDidSelectDate(_ date: Date) {
        self.dateFormat.dateFormat = "yyyy-MM-dd"
        let cell = self.getCellAtIndexPath(section: 0, index: 4) as! ProfileEditCell
       pickedGender = dateFormat.string(from: date)
        cell.editProfileTextField.text = pickedGender
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cell = self.getCellAtIndexPath(section: 0, index: 3) as! ProfileEditCell
//        self.pickedGender = pickerData[row]
        cell.editProfileTextField.text = pickerData[row]

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if !Common().checkNetworkConnect() {
            Common.showHubWithMessage(string: NSLocalizedString("couldnt_remove_picture", comment: ""))
        } else {
            let cell0 = getCellAtIndexPath(section: 0, index: 0) as! ProfileEditCell
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let scaledImage = scaleImageWidth(image: pickedImage, targetSize: CGSize(width: 750, height: 1334))
                if BaseDAO.getUserId() != 0 {
                    UserDAO.changeAvatar(user_id: String(BaseDAO.getUserId()),
                                         secret_key: BaseDAO.getSecretKey(),
                                         file: scaledImage,
                                         navigationBar: self.navigationController,
                                         completeHandle: {(success: Bool, img: String) in
                                            if success {
                                                self.alert.title = NSLocalizedString("app_name", comment: "")
                                                self.alert.message = NSLocalizedString("change_avatar_success", comment: "")
                                                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                                                self.alert.show()
                                                self.userDefaults.setValue(img, forKey: Common.USER_AVATAR)
                                                cell0.editProfileImage.image = scaledImage
                                            } else {
                                                self.alert.title = NSLocalizedString("app_name", comment: "")
                                                self.alert.message = NSLocalizedString("change_avatar_fail", comment: "")
                                                self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
                                                self.alert.show()
                                            }
                                            
                    })
                }
            }
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    func getCellAtIndexPath(section : Int,index : Int ) -> UITableViewCell
    {
        let indexPath = IndexPath(item: index, section: section)
        let cell = self.tableView.cellForRow(at: indexPath)
        return cell!
    }
    
    func scaleImageWidth(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func showAlertMessage(title: String, message: String) {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        alert.show()
    }
    
}
