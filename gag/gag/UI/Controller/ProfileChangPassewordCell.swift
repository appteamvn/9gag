//
//  ProfileChangPassewordCell.swift
//  gag
//
//  Created by ThanhToa on 11/22/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ProfileChangPassewordCell: UITableViewCell {
    @IBOutlet weak var passwordIcon: UIImageView!
    @IBOutlet weak var passwordInput: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
