//
//  ForgotPasswordViewController.swift
//  gag
//
//  Created by ThanhToa on 12/22/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var email: UITextField!
    let alert = UIAlertView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendBtn.layer.cornerRadius = 3
        self.sendBtn.layer.masksToBounds = true
        self.navigationItem.title = NSLocalizedString("forgot_password", comment: "")
        email.placeholder = NSLocalizedString("email", comment: "")
        self.sendBtn.setTitle(NSLocalizedString("sign_up", comment: ""), for: .normal)

    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        var emailInput = email.text
        self.alert.title = NSLocalizedString("app_name", comment: "")
        self.alert.message = NSLocalizedString("not_support_feature", comment: "")
        self.alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        self.alert.show()
    }
}
