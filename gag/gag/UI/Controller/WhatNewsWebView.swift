//
//  WhatNewsWebView.swift
//  gag
//
//  Created by ThanhToa on 11/23/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit

class WhatNewsWebView: UIViewController {
    @IBOutlet weak var whatNewWebview: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let whatNewsURL = NSURL(string: NSLocalizedString("app_what_new_link", comment: "")
)
        let request = NSURLRequest(url: whatNewsURL as! URL)
        whatNewWebview.loadRequest(request as URLRequest)
    }
}
