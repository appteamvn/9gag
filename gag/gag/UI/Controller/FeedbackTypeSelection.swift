//
//  FeedbackTypeSelection.swift
//  gag
//
//  Created by ThanhToa on 1/20/17.
//  Copyright © 2017 buisinam. All rights reserved.
//

import UIKit

class FeedbackTypeSelection: UITableViewController {
    var problemArray = [String]()
    var problemType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("problem_type", comment: "")
        // Problem type
        problemArray.append(NSLocalizedString("gif_problem", comment: ""))
        problemArray.append(NSLocalizedString("video_problem", comment: ""))
        problemArray.append(NSLocalizedString("picture_problem", comment: ""))
        problemArray.append(NSLocalizedString("tranlation_problem", comment: ""))
        problemArray.append(NSLocalizedString("app_crash_problem", comment: ""))
        problemArray.append(NSLocalizedString("inappropriate_content", comment: ""))
        problemArray.append(NSLocalizedString("account_problem", comment: ""))
        problemArray.append(NSLocalizedString("others_problem", comment: ""))
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return problemArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "problemTypeCellIdentifier", for: indexPath)
        cell.textLabel?.text = problemArray[indexPath.row]

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            problemType = NSLocalizedString("gif_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 1:
            problemType = NSLocalizedString("video_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 2:
            problemType = NSLocalizedString("picture_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 3:
            problemType = NSLocalizedString("tranlation_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 4:
            problemType = NSLocalizedString("app_crash_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 5:
            problemType = NSLocalizedString("inappropriate_content", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 6:
            problemType = NSLocalizedString("account_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        case 7:
            problemType = NSLocalizedString("others_problem", comment: "")
            let feedBackTableView = self.storyboard?.instantiateViewController(withIdentifier: "feedBackTableViewIdentifier") as! FeedBackTableView
            feedBackTableView.problemType = problemType
            self.show(feedBackTableView, sender: nil)
            break
        default:
            break
        }
        
    }
    
}

