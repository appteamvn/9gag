//
//  Common.swift
//  gag
//
//  Created by buisinam on 12/4/16.
//  Copyright © 2016 buisinam. All rights reserved.
// 

import UIKit
import ProgressHUD

class Common {
    let reachability = Reachability()
    var defaults = UserDefaults()

    static func lighGrayColor()-> UIColor{
//        return UIColor.init(hexString: "DEDCDE")
        return .lightGray
    }
    static func blueColor()-> UIColor{
        return UIColor.init(hexString: "0088ff")
    }
    
    static func getUserObject() -> User
    {
        let user = User()
        user.fromUserDefault()
        return user
    }
    
    static func getYouTubeLink(youtubeID: String)->String{
        return ("https://www.youtube.com/watch?v=" + youtubeID)
    }
    
    static func showLoading(view: UIView, string: String?)
    {
        
        DispatchQueue.main.async {
            if string != nil{
                ProgressHUD.show(string)
            }
            else{
                ProgressHUD.show()
            }
        }
    }
    
    static func hideLoading(view: UIView)
    {
        DispatchQueue.main.async {
            ProgressHUD.dismiss()
        }
    }
    
    static func showHubSuccess(string: String!)
    {
        DispatchQueue.main.async {
            ProgressHUD.showSuccess(string)
        }
    }
    
    static func showHubFail(string: String!)
    {
        DispatchQueue.main.async {
            ProgressHUD.show(string, interaction: false)
        }
    }
    
    static func showHubWithMessage(string: String!)
    {
        DispatchQueue.main.async {
            ProgressHUD.showError(string, interaction: true)
        }
    }
    
    // MARK: RESIZE IMAGE
    func scaleImageWidth(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func showAlert(title: String, message:String, titleCancel: String, complete: @escaping () -> Void)
    {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: titleCancel, style: .cancel)
        {
            (action:UIAlertAction!) in
            print("you have pressed the Cancel button");
            complete()
        }
        alertController.addAction(cancelAction)
        alertController.show()
    }
    // Cache calculator
    func cacheCalculator()  {
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        let folderSize = sizeOfFolder(cacheDirectory[0])
        defaults.set(folderSize, forKey: Common.CACHE_SIZE)
    }

    func sizeOfFolder(_ folderPath: String) -> String {
        var folderSize = 0
        do {
            let fileAttributes = try! FileManager.default.attributesOfItem(atPath: folderPath)
            folderSize = fileAttributes[FileAttributeKey.size] as! Int
            folderSize = folderSize*10240 // fack data
        }
        catch {
            print("error cache")
        }
        let folderSizeStr = ByteCountFormatter.string(fromByteCount: Int64(folderSize), countStyle: .file)
        return folderSizeStr
    }
    // MARK: NETWORK CONNECTION
    func checkNetworkConnect()-> Bool {
        if (reachability?.isReachable)! {
            return true
        }
        return false
    }
    

    
    // MARK: USER_DEFAULT KEYS
    static let USER_TYPE = "user_type"
    static let USER_ID = "user_id"
    static let SECRET_KEY = "secret_key"
    static let USER_NAME = "user_name"
    static let USER_EMAIL = "user_email"
    static let USER_PASSWORD = "user_password"
    static let USER_GENDER = "user_gender"
    static let USER_AVATAR = "user_avatar"
    static let USER_DOB = "user_dob"
    static let TOKEN_ID = "device_token_ios"
    // MARK: PROFILE_SETTING KEYS
    static let DEFAULT_SHOW_ADS = "default_show_ads"
    static let DEFAULT_AUTO_NIGHT_MODE = "default_auto_night_mode"
    static let NIGHT_MODE = "night_mode"
    static let SHOW_NEWS_POST = "show_news_post"
    static let AUTO_PLAY_GIF = "auto_play_gif"
    static let SENSITIVE_POST = "sensitive_post"
    static let NOT_LOGIN_NOTIFY = "not_login_notify"
    static let NOT_LOGIN_POST = "not_login_post"

    //Notification Name
    static let UPLOADING = "uploading"
    static let CACHE_SIZE = "cache_folder_size"

    // Network connection
    static let NO_NETWORK_CONNECTION = "no_network_connection"
    // Badge number
    static let BADGE_NUMBER = "badge_number"

}
