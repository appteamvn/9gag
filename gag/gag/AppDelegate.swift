//
//  AppDelegate.swift
//  gag
//
//  Created by buisinam on 10/27/16.
//  Copyright © 2016 buisinam. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import GoogleSignIn
import ProgressHUD
import Kingfisher
import Firebase
import FBAudienceNetwork
import RNNotificationView
import Alamofire
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var strDeviceToken : String = "XXX"

    let defaults = UserDefaults.standard
    
    let dataUploading:[AnyObject] = []
    let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
    let common = Common()
    
    var mainTabbar: MainTabbarController!
    var arrayViewController:NSMutableArray!
    var isAds : Bool = true

    internal var shouldRotate = false
    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return shouldRotate ? .allButUpsideDown : .portrait
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
    
        ImageCache.default.maxMemoryCost = UInt(10 * 1024 * 1024)
        
         FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        FBAdSettings.addTestDevice(FBAdSettings.testDeviceHash())
        
        UIApplication.shared.statusBarStyle =   UIStatusBarStyle.lightContent
        
        FIRApp.configure()

        GADMobileAds.configure(withApplicationID: "ca-app-pub-3861856777735024~9816465148")
        
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        
        VideoDao.getVideoCategory { (success: Bool, result) in
            
            if success {
                print("success")
            }
        }
        PostDAO.getCategoryPost { (success, data: [CategoryResponse]?) in
            if success{
                print("success")
            }
        }

        UIApplication.shared.statusBarStyle = .default
        let barAppearace = UIBarButtonItem.appearance()
        barAppearace.setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        barAppearace.tintColor = .black
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().barTintColor = .white
        registerForPushNotifications(application: application)
        
        self.loadStateProfileSetting();
        self.loadStateProfileSettingNotification()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidReceiveMemoryWarning,
                                               object: nil, queue: .main) { notification in
                                                print("Memory warning received")
                                                KingfisherManager.shared.cache.clearMemoryCache()
        }
        
        defaults.set(false, forKey: Common.NOT_LOGIN_NOTIFY)
        defaults.set(false, forKey: Common.NOT_LOGIN_POST)
        // calculate cache folder
        self.common.cacheCalculator()
        
        
        //Pending ads
        let todoEndpoint: String = "https://fackju-147503.firebaseio.com/.json"
        Alamofire.request(todoEndpoint)
            .responseJSON { response in
                // ...
                print(response)
                if let data = response.result.value as? NSDictionary{
                    self.isAds = data["isAds"] as! Bool
                }
        }
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        mainTabbar = storyboard.instantiateViewController(withIdentifier: "MainTabbarController") as! MainTabbarController
        
        self.window?.rootViewController = mainTabbar
        self.window?.makeKeyAndVisible()
        
        
        return true
    }
    
    func registerForPushNotifications(application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        self.strDeviceToken = token
        
//        let alertController = UIAlertController(title: "Title", message: self.strDeviceToken, preferredStyle: UIAlertControllerStyle.alert)
//        
//        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//        {
//            (result : UIAlertAction) -> Void in
//            print("You pressed OK")
//        }
//        alertController.addAction(okAction)
//        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
        let file = "file.txt" //this is the file. we will write to and read from it
        
        let text = self.strDeviceToken //just a text
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //writing
            do {
                try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
            
//            //reading
//            do {
//                let text2 = try String(contentsOf: path, encoding: String.Encoding.utf8)
//            }
//            catch {/* error handling here */}
        }
        
//        let acme = ["badge" : 1 ,
//                    "date_past" : "a moment ago",
//                    "message" : "abc",
//                    "post_id" : "1050",
//                    "title" : "title",
//                    "type" : "post"] as [String : Any]
//        
//        let aps = ["alert" : "Fackju",
//                   "badge" : 1,
//                   "sound" : "default"] as [String : Any]
//        let pushDict = ["acme" : acme, "aps": aps]
//        
//        delay(bySeconds: 30.0) { 
//            if let apsACME = pushDict["acme"] as? NSDictionary {
//                let post_id = apsACME["post_id"] as? Int
//                self.mainTabbar.tapOnBanner(post_id: post_id!,
//                                            type: (apsACME["type"] as? String)!)
//            }
//        }
        
    }
    
    public func delay(bySeconds seconds: Double, dispatchLevel: DispatchLevel = .main, closure: @escaping () -> Void) {
        let dispatchTime = DispatchTime.now() + seconds
        dispatchLevel.dispatchQueue.asyncAfter(deadline: dispatchTime, execute: closure)
    }
    
    public enum DispatchLevel {
        case main, userInteractive, userInitiated, utility, background
        var dispatchQueue: DispatchQueue {
            switch self {
            case .main:                 return DispatchQueue.main
            case .userInteractive:      return DispatchQueue.global(qos: .userInteractive)
            case .userInitiated:        return DispatchQueue.global(qos: .userInitiated)
            case .utility:              return DispatchQueue.global(qos: .utility)
            case .background:           return DispatchQueue.global(qos: .background)
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        let state: UIApplicationState = UIApplication.shared.applicationState
        if state == .active {
            if let aps = userInfo["aps"] as? NSDictionary {
                if let badge = aps["badge"] as? Int {
                    UIApplication.shared.applicationIconBadgeNumber = badge
                    mainTabbar.reloadBadge()
                }
                if let alertMess = aps["alert"] as? String {
                    // push notification banner - begin
                    RNNotificationView.show(withImage: UIImage(named: "fackju_icon"),
                                            title: NSLocalizedString("app_name", comment: ""),
                                            message: alertMess,
                                            duration: 3.0, onTap: {
                                                DispatchQueue.main.async {
                                                    if let apsACME = userInfo["acme"] as? NSDictionary {
                                                        let post_id = apsACME["post_id"] as? Int
                                                        self.mainTabbar.tapOnBanner(post_id: post_id!,
                                                                                    type: (apsACME["type"] as? String)!)
                                                    }
                                                }
                    })
                    // push notification banner - end
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
        
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
//        statusBar.backgroundColor = color
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, open: url,
                                                                           sourceApplication: sourceApplication,
                                                                           annotation: annotation)
        GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication, annotation: annotation)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func loadStateProfileSetting(){
        
        let settingControllers = SettingControllerSingleton.sharedInstance
        
        settingControllers.isAutoNightMode =  defaults.bool(forKey:  Common.DEFAULT_AUTO_NIGHT_MODE)
        settingControllers.isShowAds = defaults.bool(forKey: Common.DEFAULT_SHOW_ADS)
        settingControllers.isShowNewPost = defaults.bool(forKey: Common.SHOW_NEWS_POST)
        settingControllers.isAutoPlayGIFPost = defaults.bool(forKey: Common.AUTO_PLAY_GIF)
        settingControllers.isAutoExpandLongPost = defaults.bool(forKey: "DefaultAutoExpandLongPost")
        settingControllers.isShowSensitiveContent = defaults.bool(forKey: Common.SENSITIVE_POST)
        settingControllers.isNightMode = defaults.bool(forKey: Common.NIGHT_MODE)
        settingControllers.isPrivateUpvotes = defaults.bool(forKey: "DefaultPrivateUpvotes")
        defaults.set(true, forKey: Common.DEFAULT_SHOW_ADS)
//        settingControllers.isChangeStateExpand = false

    }

    func loadStateProfileSettingNotification(){
        
        let notifyControllers = NotificationsController.sharedInstance
        
        notifyControllers.isUpVoteComent =  defaults.bool(forKey:  "DefaultUpVoteComent")
        notifyControllers.isMilestonePostPoint = defaults.bool(forKey: "DefaultMilestonePostPoint")
        notifyControllers.isMilestoneForOverallPoint = defaults.bool(forKey: "DefaultMilestoneForOverallPoint")
        notifyControllers.isMilestoneForCommentPoint = defaults.bool(forKey: "DefaultMilestoneForCommentPoint")
        notifyControllers.isMentitonYouAComment = defaults.bool(forKey: "DefaultMentitonYouAComment")
        notifyControllers.isYourFriendJoin = defaults.bool(forKey: "DefaultYourFriendJoin")
        notifyControllers.isMiletonesForOverallComment = defaults.bool(forKey: "DefaultMiletonesForOverallComment")
        notifyControllers.isUpvoteYourPost = defaults.bool(forKey: "DefaultUpvoteYourPost")
        notifyControllers.isCommentYourPost = defaults.bool(forKey: "DefaultCommentYourPost")
        notifyControllers.isReportedCommentRemoved = defaults.bool(forKey: "DefaultReportedCommentRemoved")
        notifyControllers.isMilestoneForPostComment = defaults.bool(forKey: "DefaultMilestoneForPostComment")
        notifyControllers.isDownnVotedCommentRemoved = defaults.bool(forKey: "DefaultDownnVotedCommentRemoved")
        notifyControllers.isMilestoneForCommentReply = defaults.bool(forKey: "DefaultMilestoneForCommentReply")
        notifyControllers.isFeaturesPost = defaults.bool(forKey: "DefaultFeaturesPost")
        notifyControllers.isReplyYourComment = defaults.bool(forKey: "DefaultReplyYourComment")
        // network connection
        defaults.set(true, forKey: Common.NO_NETWORK_CONNECTION)
        
    }
    
}

