//
//  Post+CoreDataProperties.swift
//  
//
//  Created by tvo on 3/28/17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post");
    }

    @NSManaged public var is_long: Bool
    @NSManaged public var is_vote: Bool
    @NSManaged public var post_comment: String?
    @NSManaged public var post_date: String?
    @NSManaged public var post_id: String?
    @NSManaged public var post_link: String?
    @NSManaged public var post_title: String?
    @NSManaged public var post_type: Int16
    @NSManaged public var post_view: Int16
    @NSManaged public var safe: Bool
    @NSManaged public var url: String?

}
